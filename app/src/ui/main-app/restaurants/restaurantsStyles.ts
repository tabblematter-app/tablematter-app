import { StyleSheet } from 'react-native';
import { p1 } from '../../utils/themes/lightTheme';

export const styles = StyleSheet.create({
  reviewsTitle: {
    color: 'white',
    fontSize: 20,
    width: '100%',
    textAlign: 'center',
  },
  titletext: {
    color: '#000',
    fontSize: 20,
    fontWeight: 'bold',
    marginBottom: 10,
  },
  addrtext: {
    color: '#000',
    fontSize: 18,
  },
  restImage: {
    height: 200,
    width: '100%',
    flex: 1,
    marginBottom: 20
  },
  image: {
    width: "100%",
    height: undefined,
    flex: 1,
  },
  textDescription: {
    paddingTop: 7,
    color: '#555',
    fontSize: 16,
    paddingBottom: 2,
    textAlign: 'justify',
  },
  daysButton: {
    borderColor: '#F71818',
    borderWidth: 1,
    height: 26,
    flexGrow: 1,
    marginHorizontal: 5,
    marginVertical: 5,
  },
  daySelected: {
    backgroundColor: '#DA0202',
    borderColor: '#DA0202',
  },
  daysText: {
    textAlign: 'center',
    color: 'white',
    fontSize: 15,
  },
  price: {
    textAlign: "center",
  },
  category: {
    fontSize: 24,
    fontWeight: 'bold',
  },
  addButton: {
    right: '5%',
    borderRadius: 6,
    width: 40,
    height: 40,
    backgroundColor: p1,
    justifyContent: 'center',
    alignItems: 'center',
    padding: 0,
  },
  addButtonIcon: {
    fontSize: 25,
    color: "white"
  },
  remove: {
    textDecorationColor: p1,
    textDecorationLine: 'underline',
    color: p1,
  },
  borderTop: {
    borderTopWidth: 1,
    borderTopColor: p1,
  },
  icon: {
    color: p1,
    padding: 8,
  },
  listHeader: {
    backgroundColor: '#FFF'
  },
  //REVIEWS
  restaurantReviewBlob: {
    marginBottom: 20,
    height: 'auto',
    minHeight: 100,
    maxHeight: 200,
  },
  inner: {
    flexDirection: 'column',
    justifyContent: 'space-between',
    flex: 1,
    padding: 20,
  },
  restaurantReviewContent: {
    color: "black",
    marginBottom: 5,
  },
  restaurantReviewSubtext: {
    color: "black",
    fontSize: 12,
    marginBottom: 5,
  },
  restaurantReviewTextBold: {
    color: "black",
    fontWeight: "bold",
    marginBottom: 5,
  },
});
