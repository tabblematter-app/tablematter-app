import { UserState, TableMatterActionTypes, SET_USER_ACTION, LOG_OUT_ACTION } from "../types";

const defaultUser = {
    id: undefined,
    profilePictureUrl: undefined,
    firstName: undefined,
    lastName: undefined,
    email: undefined,
    phone: undefined,
};

export const user = (state: UserState = defaultUser, action: TableMatterActionTypes): UserState => {
    switch (action.type) {
        case SET_USER_ACTION:
            return {
                id: action.id,
                profilePictureUrl: action.profilePictureUrl,
                firstName: action.firstName,
                lastName: action.lastName,
                email: action.email,
                phone: action.phone,
            };

        case LOG_OUT_ACTION:
            return defaultUser;
    }

    return state;
};
