import reducer from "../../src/redux/reducers/index";
import { SET_USER_ACTION, TableMatterState, logOut } from "../../src/redux";
import { defaultState } from "./index.test";
// TODO add tests
describe("TableMatter - User Reducer", () => {
    it("should add a user", () => {
        expect(reducer(undefined, {
            type: SET_USER_ACTION,
            id: "1",
            profilePictureUrl: "a",
            firstName: "a",
            lastName: "a",
            email: "a",
            phone: "a",
        }))
            .toMatchObject({
                user: {
                    id: "1",
                    profilePictureUrl: "a",
                    firstName: "a",
                    lastName: "a",
                    email: "a",
                    phone: "a",
                },
            });
    });

    describe("LOG_OUT_ACTION", () => {
        it("should log out", () => {
            const initialState: TableMatterState = {
                ...defaultState,
                user: {
                    id: "1",
                    profilePictureUrl: "a",
                    firstName: "a",
                    lastName: "a",
                    email: "a",
                    phone: "a",
                }
            };
            const action = logOut();
            const actualState = reducer(initialState, action);
            expect(actualState).toEqual(defaultState);
        });
    });


});