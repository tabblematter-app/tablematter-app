import { CounterState, TableMatterActionTypes, SET_VALUE_ACTION, TableMatterState, SET_USER_RESERVATIONS_ACTION, SET_SEARCH_RESULTS_ACTION, SET_SELECTED_RESTAURANT_ACTION, SET_REVIEWS_ACTION, SET_SELECTED_RESERVATION_ACTION, ReviewData } from "../types";
import { combineReducers } from "redux";
import { user } from "./user";
import { Reservation, Restaurant } from "../types/serverTypes";
import { newReservation } from "./new-reservation";

const counter = (state: CounterState = { value: 0 }, action: TableMatterActionTypes): CounterState => {
  switch (action.type) {
    case SET_VALUE_ACTION:
      return {
        ...state,
        value: action.value
      };
    default:
  }
  return state;
};

const reservations = (state: Reservation[] = [], action: TableMatterActionTypes): Reservation[] => {
  switch (action.type) {
    case SET_USER_RESERVATIONS_ACTION:
      return action.reservations;
    default:
  }
  return state;
};

const searchResults = (state: Restaurant[] = [], action: TableMatterActionTypes): Restaurant[] => {
  switch (action.type) {
    case SET_SEARCH_RESULTS_ACTION:
      return action.searchResults;
    default:
  }
  return state;
};

const selectedRestaurant = (state: Restaurant | null = null, action: TableMatterActionTypes): Restaurant | null => {
  switch (action.type) {
    case SET_SELECTED_RESTAURANT_ACTION:
      return action.selectedRestaurant;
  }
  return state;
};

const selectedReservation = (state: Reservation | null = null, action: TableMatterActionTypes): Reservation | null => {
  switch (action.type) {
    case SET_SELECTED_RESERVATION_ACTION:
      return action.selectedReservation;
  }
  return state;
};


const reviews = (state: ReviewData[] = [], action: TableMatterActionTypes): ReviewData[] => {
  switch (action.type) {
    case SET_REVIEWS_ACTION:
      return action.reviews;
  }
  return state;
};

export default combineReducers<TableMatterState, TableMatterActionTypes>({ counter, reservations, searchResults, reviews, selectedRestaurant, user, newReservation, selectedReservation });