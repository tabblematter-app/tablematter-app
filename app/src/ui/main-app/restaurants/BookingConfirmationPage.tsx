import React from 'react';
import { Text, View, ScrollView } from 'react-native';
import { styles } from './SummaryStyles';
import { Header, Body, Left, Right, Icon, Button } from 'native-base';
import { Reservation, Restaurant } from '../../../redux/types/serverTypes';
import {
  TableMatterState,
  setSelectedRestaurant,
  NewReservationState,
  setTableNumbers,
  setPaid,
  UserState
} from '../../../redux';
import { connect } from 'react-redux';
import { useNavigation } from '@react-navigation/native';
import { defaultRestaurant } from './RestaurantPage';
import { Share } from 'react-native';
import { useRoute, RouteProp } from '@react-navigation/native';
import { DrawerParamList } from '../../utils/drawer';
import { commonStyles } from '../../common/commonStyles';
import { StackNavigationProp } from '@react-navigation/stack';
import { Grid, Row, Col } from 'react-native-easy-grid';
import moment from 'moment';
import { displayEndTime, displayParty } from '../../utils/helpers';
import { BackToHomeButton } from '../../common/BackToHome';
import { restToRestShort, userToCustShort } from '../../utils/type-conversions';
import { RootStackParamList } from '../../utils/rootStack';
import API from '../../utils/api';

//type orderPlacedScreenRoute = RouteProp<DrawerParamList, 'BookingConfirmationPage'>;

interface StateProps {
  restaurant: Restaurant;
  name: string;
  newReservation: NewReservationState;
  tableNumbers: number[];
  customer: UserState;
}
interface DispatchProps {
  setSelectedRestaurant: (selectedRestaurant: Restaurant | null) => void;
  setTableNumbers: (tableNumbers: number[]) => void;
  setPaid: (paid: boolean) => void;

}

type ProfileScreenNavigationProp = StackNavigationProp<DrawerParamList>;

interface OwnProps {
  navigation: ProfileScreenNavigationProp; // Alternatively any
}

type orderPlacedScreenRoute = RouteProp<RootStackParamList, 'OrderPlaced'>;
type Props = StateProps & OwnProps & DispatchProps;

const BookingConfirmationPage = (props: Props) => {
  const navigation = useNavigation();
  const [reservation] = React.useState<NewReservationState>(
    props.newReservation,
  );
  const route = useRoute() as orderPlacedScreenRoute;
  const orderId = route.params?.orderId || '';
  const dineIn = route.params?.dineIn || false;

  const shareReservation = async () => {
    try {
      const result = await Share.share({
        message: `Booking for ${reservation.date}`,
        url: `example.com/${orderId}`,
      });
      if (result.action === Share.sharedAction) {
        if (result.activityType) {
          // shared with activity type of result.activityType
        } else {
          // shared
        }
      } else if (result.action === Share.dismissedAction) {
        // dismissed
      }
    } catch (error) {
      console.warn(error.message);
    }
  };

  const handleMakeReservation = () => {
    props.setPaid(false);
    const reservation: Reservation = {
      restaurant: restToRestShort(props.restaurant),
      customer: userToCustShort(props.customer),
      orders: props.newReservation.orders,
      dineIn: props.newReservation.dineIn,
      duration: props.newReservation.duration,
      totalPrice: props.newReservation.totalPrice,
      finished: false,
      cancelled: false,
      paid: false,
      party: props.newReservation.party,
      tableNumbers: props.tableNumbers,
      timeBooked: props.newReservation.date,
    };

    API.post('/reservation', { reservation })
      .then((data) => {
        console.log(data.data.data._id);
        data.data
          ? navigation.navigate('OrderPlaced', {
            orderId: data.data.data._id,
            dineIn: true,
          })
          : navigation.navigate('ErrorPage');
      })
      .catch(() => navigation.navigate('ErrorPage'));
  };

  return (
    <View style={commonStyles.container}>
      <Header style={commonStyles.header}>
        <Left style={commonStyles.flex1}>
          <BackToHomeButton />
        </Left>
        <Body style={commonStyles.body}>
          <Text testID="welcome-title" style={commonStyles.title}>
            Booking Confirmation
          </Text>
        </Body>
        <Right style={commonStyles.flex1}>
          {dineIn && (
            <Icon
              name='share'
              style={{ color: "white" }}
              onPress={() => shareReservation()}
            />
          )}
        </Right>
      </Header>

      <ScrollView style={commonStyles.flex1}>
        <View style={styles.body}>
          <View style={{ alignItems: 'center', marginVertical: 15 }}>
            <Text style={styles.subtitle}>Your reservation for {props.name} at</Text>
            <Text style={styles.title}>{props.restaurant.name}</Text>
          </View>

          <View style={styles.checkmarkCircle}>
            <Icon name={"checkmark"} style={styles.checkmark} />
          </View>

          <Grid style={{ marginBottom: 20 }}>
            <Row style={styles.row}>
              <Col style={styles.leftCol}>
                <Row style={{ alignItems: 'center', justifyContent: 'center' }}>
                  <Icon name="calendar" style={styles.icon}></Icon>
                  <Text style={styles.detailTitle}>Date</Text>
                </Row>
                <Text style={styles.detailText}>
                  {moment(props.newReservation.date).format('MMMM Do YYYY')}
                </Text>
              </Col>
              <Col style={styles.rightCol}>
                <Row style={{ alignItems: 'center', justifyContent: 'center' }}>
                  <Icon name="time" style={styles.icon}></Icon>
                  <Text style={styles.detailTitle}>Time</Text>
                </Row>
                <Text>
                  {moment(props.newReservation.date).format('h:mm a') + ' - ' + displayEndTime(props.newReservation.date, props.newReservation.duration)}
                </Text>
              </Col>
            </Row>

            <Row style={styles.row}>
              <Col>
                <Row style={{ alignItems: 'center', justifyContent: 'center' }}>
                  <Icon name="people" style={styles.icon}></Icon>
                  <Text style={styles.detailTitle}>Party</Text>
                </Row>
                <Text style={styles.detailText}>
                  {props.newReservation.party &&
                    <Text>
                      {displayParty(props.newReservation.party) + ' at Table(s) ' + props.newReservation.tableNumbers.join(", ")}
                    </Text>
                  }
                </Text>
              </Col>
            </Row>
            <Row style={styles.row}>
              <Col>
                <Row style={{ alignItems: 'center', justifyContent: 'center' }}>
                  <Icon name="location" type="Entypo" style={styles.icon}></Icon>
                  <Text style={styles.detailTitle}>Address</Text>
                </Row>
                <Text style={styles.detailText}>
                  {props.restaurant.addressLine1}
                </Text>
              </Col>
            </Row>
          </Grid>
        </View>

        <View style={commonStyles.buttonRow}>
          <Col style={commonStyles.leftButton}>
            <Button
              rounded
              style={commonStyles.button}
              onPress={() => navigation.navigate('Menu', { reservation: true })}>
              <Text style={commonStyles.buttonText}>ORDER MEAL NOW</Text>
            </Button>
          </Col>
          <Col style={commonStyles.rightButton}>
            <Button
              rounded
              style={commonStyles.button}
              onPress={() => handleMakeReservation()}>
              <Text style={commonStyles.buttonText}>ORDER MEAL LATER</Text>
            </Button>
          </Col>
        </View>
      </ScrollView>
    </View>
  );
};

const mapStateToProps = (state: TableMatterState): StateProps => {
  return {
    newReservation: state.newReservation,
    restaurant: state.selectedRestaurant || defaultRestaurant,
    name: state.user.firstName!,
    tableNumbers: state.newReservation.tableNumbers || [],
    customer: state.user,
  };
};

const mapDispatchToProps: DispatchProps = {
  setSelectedRestaurant,
  setTableNumbers,
  setPaid
};

// IDK why error appears it doesnt affect it from working (Add this on components until it is fixed)
// @ts-ignore
export default connect<StateProps, DispatchProps>(mapStateToProps, mapDispatchToProps)(BookingConfirmationPage);
