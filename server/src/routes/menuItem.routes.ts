import * as express from "express";
import { Response } from "express";
import { findAll } from "../controllers/model.functions";
import menuItemModel from "../models/menuItemModel";

export default (app: express.Express) => {
    app.get("/menu-item", (_, res: Response) => findAll(res, menuItemModel));
};
