import { useNavigation } from '@react-navigation/native';
import { View } from 'native-base';
import React from 'react';
import { connect } from 'react-redux';
import { logOut } from '../../redux';

interface DispatchProps {
    logOut: () => void;
}

type Props = DispatchProps;

const LogOutPage = (props: Props) => {
  const navigation = useNavigation();

  React.useEffect(() => {
      props.logOut();
      navigation.navigate("Landing");
  });

  return (
    <View />
  );
};

const mapDispatchToProps: DispatchProps = {
    logOut,
};

// IDK why error appears it doesnt affect it from working (Add this on components until it is fixed)
// @ts-ignore 
export default connect<{}, DispatchProps>(undefined, mapDispatchToProps)(LogOutPage);
