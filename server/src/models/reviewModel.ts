import * as mongoose from "mongoose";
import { Review } from "../types/types";

export const reviewSchema = new mongoose.Schema({
    reservationId: String,
    restaurantId: String,
    customerId: {
        required: false,
        type: String
    }, // not necessary for anon reviews
    reviewerName: String, // by default, the customer first name or custom name
    rating: Number,
    content: String,
    modified: Boolean,
});

export default mongoose.model<Review & mongoose.Document>('Review', reviewSchema);