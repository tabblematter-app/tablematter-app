import { StyleSheet } from "react-native";

export const styles = StyleSheet.create({
    container: {
      width: '100%',
      height: '100%',
      justifyContent: "center",
      alignItems: "center"
    },
    webview: {
      width: '100%',
      height: '100%',
      position: 'absolute',
      top: 0,
      left: 0,
      right: 0,
      bottom: 0,
    },
    btn: {
      paddingVertical: 5,
      paddingHorizontal: 15,
      borderRadius: 10,
      backgroundColor: '#61E786',
      justifyContent: 'center',
      alignItems: 'center',
      alignContent: 'center',
    },
  });