import * as express from "express";
import { Response } from "express";
import { findRestaurants, findResturantsBookedByUser, findRestaurantById } from "../controllers/restaurant.controller";

export default (app: express.Express) => {
  app.get("/restaurant", (req, res: Response) =>
    findRestaurants(
      (req.query.searchString as string) || null,
      (req.query.restaurantName as string) || null,
      (req.query.locationString as string) || null,
      (req.query.menuItem as string) || null,
      (req.query.filters as string[]) || null,
      res)
  );

  app.get("/restaurantsbooked", (req, res: Response) => {
    findResturantsBookedByUser((req.query.userId as string) || "", res);
  });

  app.get("restaurantbyid", (req, res: Response) => {
    findRestaurantById(req.query.restaurantId as string, res);
  });
};
