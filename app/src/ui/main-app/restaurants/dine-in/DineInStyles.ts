import { StyleSheet } from 'react-native';
import { p1 } from '../../../utils/themes/lightTheme';

export const styles = StyleSheet.create({
  titletext: {
    color: '#000',
    fontSize: 20,
    fontWeight: 'bold',
    marginBottom: 10,
  },
  addrtext: {
    color: '#000',
    fontSize: 18,
  },
  partyText: {
    color: 'black',
    justifyContent: 'center',
    textAlign: 'center',
  },
  input: {
    alignItems: "center",
    justifyContent: "center"
  },
  picker: {
    borderWidth: 1,
    borderColor: p1,
    borderRadius: 30,
    paddingLeft: 10, // not sure why this has to be different
    paddingRight: 10,
    fontSize: 14,
    // width: Platform.OS === "ios" ? "95%" : "100%",
  },
  durationView: {
    flex: 1,
    flexDirection: 'row',
    justifyContent: "space-between",
    alignItems: "center"
  },
  icon: {
    fontSize: 20,
    color: p1,
  },
  restImage: {
    height: 200,
    width: '100%',
    flex: 1,
    marginBottom: 20
  },
  image: {
    width: "100%",
    height: undefined,
    flex: 1,
  },
});
