import React from 'react';
import { styles } from "./TutorialStyles";
import { Text, View } from "native-base";
import AppIntroSlider from 'react-native-app-intro-slider';
import { StackNavigationProp } from '@react-navigation/stack';
import { RootStackParamList } from '../utils/rootStack';
import { Image, ImageSourcePropType } from 'react-native';
import PropTypes from 'prop-types';

interface Item {
  key: string;
  title: string;
  image: ImageSourcePropType;
  text: string;
}

const slides: Item[] = [
  {
    key: "1",
    title: "Customise Your Search",
    image: require('../../../Assets/slide-1.png'),
    text: "Filter your search by restaurant name, location and even menu item ingredients",
  },
  {
    key: "2",
    title: "Dine In or Takeaway",
    image: require('../../../Assets/slide-2.png'),
    text: "Choose between making a reservation or ordering a meal to pickup or have delivered",
  },
  {
    key: "3",
    title: "Pre-order and Share",
    image: require('../../../Assets/slide-3.png'),
    text: "Pre-order meals for your booking and share your booking with your friends!",
  },
];

interface SlidesProp {
  item: Item;
  _renderItem: (item: object) => JSX.Element;
  _onDone: () => void;
};

type NavigationProp = StackNavigationProp<RootStackParamList>;

interface OwnProps {
  navigation: NavigationProp;
};

type Props = OwnProps & SlidesProp;



const TutorialPage = (props: Props) => {
  // eslint-disable-next-line react/prop-types
  const _renderItem = ({item}) => {
    return (
      <View style={styles.slide}>
        <Text style={styles.title}>{item.title}</Text>
        <View style={styles.imageContainer}>
          <Image source={item.image} style={styles.image} />
        </View>
        <Text style={styles.text}>{item.text}</Text>
      </View>
    );
  };
  // Required due to nested component
  _renderItem.propTypes = {
    item: PropTypes.shape({
      title: PropTypes.string,
      image: PropTypes.string,
      text: PropTypes.string,
    })
  };

  const _onDone = () => {
    props.navigation.navigate("Main");
  };

  return (
    <View style={styles.slideContainer}>
      <AppIntroSlider
        renderItem={_renderItem}
        data={slides}
        onDone={_onDone}
        showSkipButton={true}
        showPrevButton={true}
      />
    </View>
  );
};

export default TutorialPage;
