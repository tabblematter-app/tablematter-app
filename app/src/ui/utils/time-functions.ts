
import moment from 'moment';

export const getTimeFormated = (date: Date): string => {
  const isPm = date.getHours() > 12;

  const hours = date.getHours() % 12;
  const minutes = date.getMinutes();

  return `${hours}:${minutes} ${isPm ? "PM" : "AM"}`;
};

export const formatTime = (date: Date): string => {
  return `${moment(date).format('h:mma')}`;
};