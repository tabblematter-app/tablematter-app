import * as mongoose from "mongoose";
import { Table } from "../types/types";

export const tableSchema = new mongoose.Schema({
    number: Number,
    status: {
        type: String,
        enum: ['Open', 'Occupied', 'Reserved', 'Closed'],
        default: 'Closed',
    },
    capacity: Number,
    shape: {
        type: String,
        enum: ['Square', 'Circle', 'Rectangle', 'Semicircle', 'Other'],
        pos: [Number, Number],
    },
});

export default mongoose.model<Table & mongoose.Document>("Table", tableSchema);