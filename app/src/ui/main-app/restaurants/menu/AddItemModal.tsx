import React from 'react';
import { Text, Button, Card, CardItem, View, Icon } from "native-base";
import { MenuItem } from '../../../../redux/types/serverTypes';
import { addMenuItem } from '../../../../redux';
import { connect } from 'react-redux';
import Modal from "react-native-modal";
// import Icon from 'react-native-vector-icons/FontAwesome';
import { TextInput } from 'react-native-gesture-handler';
import { modalStyles } from "../addModalStyles";
import { commonStyles } from '../../../common/commonStyles';
import { TouchableOpacity } from 'react-native';

interface OwnProps {
  menuItem: MenuItem;
  isOpen: boolean;
  hide: () => void;
}

interface DispatchProps {
  addMenuItem: (menuItem: MenuItem) => void;
}

type Props = OwnProps & DispatchProps;

const AddItemModal = (props: Props) => {
  const [numberToOrder, setNumberToOrder] = React.useState(1);

  const handleSubmit = () => {
    for (let i = 0; i < numberToOrder; i++) {
      props.addMenuItem(props.menuItem);
    }
    props.hide();
  };

  return (
    <Modal isVisible={props.isOpen}>
      <Card>
        <CardItem header style={modalStyles.cardHeader}>
          <Text style={modalStyles.cardHeaderText}>Add Item</Text>
        </CardItem>

        <CardItem style={{ alignContent: "center", justifyContent: "center" }}>
          <View style={{ width: "80%" }}>
            <Text style={modalStyles.title}>{props.menuItem.name} - (${props.menuItem.price.toFixed(2)})</Text>
            <View style={[commonStyles.input]}>
              <View style={commonStyles.amountView}>
                <TouchableOpacity onPress={() => numberToOrder > 0 ? setNumberToOrder(numberToOrder - 1) : 0}>
                  <Icon name="remove" style={commonStyles.amountIcon}></Icon>
                </TouchableOpacity>
                <TextInput
                  keyboardType="numeric"
                  style={commonStyles.amountText}
                  onChangeText={(value) => setNumberToOrder(parseInt(value))}
                  value={numberToOrder.toString()}
                />
                <TouchableOpacity onPress={() => setNumberToOrder(numberToOrder + 1)}>
                  <Icon name="add" style={commonStyles.amountIcon}></Icon>
                </TouchableOpacity>
              </View>
            </View>

            {/* <View style={modalStyles.view}>
                <Text style={{ fontSize: 26, textAlignVertical: "top" }}>Qty: </Text>
                <Button style={modalStyles.numberButton} onPress={() => numberToOrder > 0 ? setNumberToOrder(numberToOrder - 1) : 0}>
                  <Icon name="minus" color="white" size={25} />
                </Button>
                <TextInput
                  keyboardType="numeric"
                  style={modalStyles.input}
                  onChangeText={(value) => setNumberToOrder(parseInt(value))}
                  value={numberToOrder.toString()}
                />
                <Button style={modalStyles.numberButton} onPress={() => setNumberToOrder(numberToOrder + 1)}>
                  <Icon name="plus" color="white" size={25} />
                </Button>
              </View> */}

            {/* <View style={modalStyles.view}>
                <Button style={[modalStyles.cancelButton, modalStyles.submitButton]} onPress={props.hide}>
                  <Text style={modalStyles.submitButtonText}>Cancel</Text>
                </Button>
                <Button style={modalStyles.submitButton} onPress={handleSubmit}>
                  <Text style={modalStyles.submitButtonText}>Add +</Text>
                </Button>
              </View> */}
          </View>
        </CardItem>

        <CardItem footer>
          <Button transparent style={commonStyles.cardButton} onPress={props.hide}>
            <Text style={commonStyles.cardButtonText}>Cancel</Text>
          </Button>
          <Button transparent style={commonStyles.cardButton} onPress={handleSubmit}>
            <Text style={commonStyles.cardButtonText}>Add</Text>
          </Button>
        </CardItem>
      </Card>
    </Modal>
  );
};

const mapDispatchToProps: DispatchProps = {
  addMenuItem,
};

// IDK why error appears it doesnt affect it from working (Add this on components until it is fixed)
// @ts-ignore 
export default connect<{}, DispatchProps>(undefined, mapDispatchToProps)(AddItemModal);
