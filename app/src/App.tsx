import React from 'react';
import {StyleSheet, View} from 'react-native';
import {Provider} from 'react-redux';
import {NavigationContainer} from '@react-navigation/native';
import {store} from './redux';
import {RootStack} from './ui/utils/rootStack';
import LandingPage from './ui/landing-page/LandingPage';
import {Main} from './ui/main-app/Main';
import SignUp from './ui/sign-up/SignUp';
import Login from './ui/login-page/login';
import {SignUpSuccess} from './ui/sign-up-success/SignUpSuccess';
import TutorialPage from './ui/tutorial/Tutorial';
import {ErrorPage} from './ui/common/ErrorPage';
import SearchResults from './ui/search/SearchResults';
import MenuPage from './ui/main-app/restaurants/menu/MenuPage';
import RestaurantPage from './ui/main-app/restaurants/RestaurantPage';
import SeatPickerView from './ui/seat-picker/SeatPickerView';
import EditProfile from './ui/profile-page/EditProfile';
import UserReviewPage from './ui/review-page/UserReviewPage';
import OrderSummary from './ui/reservation-finalisation/OrderSummary';
import DineInPage from './ui/main-app/restaurants/dine-in/DineInPage';
import OrderPlaced from './ui/reservation-finalisation/OrderPlaced';
import BookingDetailsPage from './ui/main-app/restaurants/BookingDetailsPage';
import ReservationSummaryPage from './ui/main-app/restaurants/ReservationSummary';
import {PayNowPage} from './ui/main-app/restaurants/finalise-order/PayNowPage';
import BookingConfirmationPage from './ui/main-app/restaurants/BookingConfirmationPage';
import SettingsPage from './ui/main-app/settings/SettingsPage';
import SettingsSubMenu from './ui/main-app/settings/SettingsSubMenu';

export const App = () => {
  return (
    <Provider store={store}>
      <View style={styles.container}>
        <NavigationContainer>
          <RootStack.Navigator initialRouteName="Landing" headerMode="none">
            <RootStack.Screen name="Landing" component={LandingPage} />
            <RootStack.Screen name="Main" component={Main} />
            <RootStack.Screen name="SignUpPage" component={SignUp} />
            <RootStack.Screen name="Login" component={Login} />
            <RootStack.Screen name="SignUpSuccess" component={SignUpSuccess} />
            <RootStack.Screen name="ErrorPage" component={ErrorPage} />
            <RootStack.Screen name="Tutorial" component={TutorialPage} />
            <RootStack.Screen name="SearchResults" component={SearchResults} />
            <RootStack.Screen name="Menu" component={MenuPage} />
            <RootStack.Screen
              name="RestaurantPage"
              component={RestaurantPage}
            />
            <RootStack.Screen name="SeatPicker" component={SeatPickerView} />
            <RootStack.Screen name="EditProfile" component={EditProfile} />
            <RootStack.Screen name="UserReview" component={UserReviewPage} />
            <RootStack.Screen name="OrderSummary" component={OrderSummary} />
            <RootStack.Screen name="ReservationSummaryPage" component={ReservationSummaryPage} />
            <RootStack.Screen name="DineInPage" component={DineInPage} />
            <RootStack.Screen name="OrderPlaced" component={OrderPlaced} />
            <RootStack.Screen
              name="BookingDetailsPage"
              component={BookingDetailsPage}
            />
            <RootStack.Screen name="PayNowPage" component={PayNowPage} />
            <RootStack.Screen
              name="BookingConfirmationPage"
              component={BookingConfirmationPage}
            />
            <RootStack.Screen name="SettingsPage" component={SettingsPage} />
            <RootStack.Screen name="SettingsSubMenu" component={SettingsSubMenu} />
          </RootStack.Navigator>
        </NavigationContainer>
      </View>
    </Provider>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
  },
});
