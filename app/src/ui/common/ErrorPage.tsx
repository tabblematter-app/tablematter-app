import React from 'react';
import { Platform } from "react-native";
import { Header, Body, Title, View, Left, Right, Text } from "native-base";
import { ScrollView } from 'react-native-gesture-handler';
import { BackButton } from '../common/BackButton';
import { commonStyles } from '../common/commonStyles';

export const ErrorPage = () => {
  return (
    <View>
        <Header style={commonStyles.header}>
            <Left style={commonStyles.flex1}>
                <BackButton />
            </Left>
            <Body style={commonStyles.body}>
                <Title style={commonStyles.title}>Whoops!</Title>
            </Body>
            <Right style={commonStyles.flex1} />
        </Header>
        <ScrollView style={{marginBottom: Platform.OS === "ios" ? 150 : 70}}>
            <Text style={{textAlign: "center", fontWeight: "bold", fontSize: 30, paddingBottom: 5}}>OH NO!</Text>
            <Text style={{ textAlign: "center", paddingBottom: 20 }}>It appears that something went wrong</Text>
        </ScrollView>
    </View>
  );
};
