/* eslint-disable @typescript-eslint/camelcase */
export const getPaypalDetails = (price: number, restaurantName: string, discount = 0): object => {
    return {
        "intent": "sale",
        "payer": {
          "payment_method": "paypal"
        },
        "transactions": [{
          "amount": {
            "currency": "AUD",
            "total": `${price - discount}`,
            "details": {
              "shipping": "0",
              "subtotal": `${price - discount}`,
              "shipping_discount": "0",
              "insurance": "0",
              "handling_fee": "0",
              "tax": "0"
            }
          },
          "description": `Payment to ${restaurantName}`,
          "payment_options": {
            "allowed_payment_method": "IMMEDIATE_PAY"
          }, "item_list": {
             "items": [{
                "name": "Total order",
                "description": "Total for order",
                "quantity": "1",
                "price": `${price - discount}`,
                "tax": "0",
                "sku": "1234",
                "currency": "AUD"
            }],
          }
        }],
        "redirect_urls": {
          "return_url": "https://example.com/",
          "cancel_url": "https://example.com/"
        }
    };
};