import reservationModel from "../models/reservationModel";
import helper from "../utils/responseHelper";
import { Response } from "express";
import constants from "../utils/constants";
import { Reservation } from "../types/types";

export function getUserReservations(id: string, res: Response) {
    reservationModel.find({ 'customer.id': id})
        .then((reservations: Reservation[]) => {
            if (!reservations) {
                return res.send(helper.failResponse(Error("NoReservations"), constants.fail.userReservations));
            }
            
            return res.send(helper.successResponse(reservations, constants.success.userReservations));
        })
        .catch((err) => {
            res.send(helper.failResponse(err, constants.fail.internalError));
        });
}

export function addReservation(reservation: Reservation, res: Response) {
    // TODO if issues adding reservations of a specific type just split with if around dineIn
    reservationModel.create({
        customer: reservation.customer,
        restaurant: reservation.restaurant,
        dineIn: reservation.dineIn,
        party: reservation.party,
        tableNumbers: reservation.tableNumbers,
        orders: reservation.orders,
        totalPrice: reservation.totalPrice,
        timeBooked: reservation.timeBooked,
        finished: reservation.finished,
        cancelled: reservation.cancelled,
        duration: reservation.duration,
        paid: reservation.paid,
    })
        .then((data) => {
            return res.send(helper.successResponse(data, constants.success.addReservation));
        })
        .catch((e) =>{
            console.log(e);
            return res.send(helper.failResponse(null, constants.fail.addReservation));
        });
}

export function updateReservation(reservationId: string, reservation: Reservation, res: Response) {
    reservationModel.findByIdAndUpdate({_id: reservationId}, {
        customer: reservation.customer,
        restaurant: reservation.restaurant,
        dineIn: reservation.dineIn,
        party: reservation.party,
        tableNumbers: reservation.orders,
        orders: reservation.orders,
        totalPrice: reservation.totalPrice,
        timeBooked: reservation.timeBooked,
        finished: reservation.finished,
        cancelled: reservation.cancelled,
        duration: reservation.duration,
        paid: reservation.paid,
    })
        .then(() => {
            return res.send(helper.successResponse(null, constants.success.updateReservation));
        })
        .catch(() =>{
            return res.send(helper.failResponse(null, constants.fail.updateReservation));
        });
}

export function deleteReservation(reservationId: string, res: Response) {
    reservationModel.findByIdAndDelete({_id: reservationId})
    .then(() => {
        return res.send(helper.successResponse(null, constants.success.deleteReservation));
    })
    .catch(() =>{
        return res.send(helper.failResponse(null, constants.fail.deleteReservation));
    });
}