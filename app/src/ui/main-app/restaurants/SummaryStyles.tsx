import { StyleSheet } from 'react-native';
import { p1, tmDarkRed } from '../../utils/themes/lightTheme';

export const styles = StyleSheet.create({
  body: {
    width: "80%",
    alignSelf: "center",
    paddingBottom: 40
  },
  subtitle: {
    color: tmDarkRed,
    marginBottom: 5
  },
  title: {
    fontSize: 18,
    fontWeight: 'bold',
    color: tmDarkRed,
    textAlign: 'center'
  },
  detailTitle: {
    fontSize: 16,
    fontWeight: 'bold',
    color: p1,
    marginVertical: 10,
  },
  detailText: {
    textAlign: 'center'
  },
  row: {
    marginVertical: 10,
    alignItems: 'center'
  },
  leftCol: {
    marginRight: 15,
  },
  rightCol: {
    marginLeft: 15,
  },
  totalRow: {
    width: "100%",
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center'
  },
  icon: {
    color: p1, // #555
    marginRight: 10,
  },
  mapContainer: {
    borderColor: 'red',
    borderWidth: 1,
    height: 200,
    width: "100%",
    marginTop: 15,
  },
  map: {
    flex: 1,
    height: '100%',
    width: '100%',
  },
  total: {
    fontWeight: "bold"
  },
  price: {
    paddingTop: 5,
    right: "5%",
    textAlign: "center",
  },
  // Checkmark for Booking Confirmation
  checkmarkCircle: {
    marginVertical: 20,
    borderWidth: 1,
    borderColor: p1,
    textAlign: 'center',
    alignItems: 'center',
    justifyContent: 'center',
    alignSelf: "center",
    width: 100,
    height: 100,
    backgroundColor: p1,
    borderRadius: 50,
  },
  checkmark: {
    fontSize: 50,
    color: "white"
  }
});