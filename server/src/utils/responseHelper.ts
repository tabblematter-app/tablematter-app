const helper = {
  failResponse: (error: any, message: string) => {
    //Use for all expected errors in the API
    return {
      status: false,
      data: (error ? error : null),
      message: (message ? message : "Failed Response")
    };
  },

  successResponse: (data: any, message: string) => {
    //Use for all successful responses in the API
    if (data) {
      return {
        status: true,
        data: (data ? data : null),
        message: (message ? message : "Sucessful Response")
      };
    } else {
      return {
        status: true,
        data: (data ? data : null),
        message: (message ? message : "Successful but empty response"),
      };
    }
  }
};

export default helper;