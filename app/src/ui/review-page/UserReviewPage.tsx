import React, { useEffect } from "react";
import { connect } from "react-redux";
import { View, Text, Image } from "react-native";
import { Header, Left, Body, Item, Input, Button, Right, CheckBox, ListItem } from "native-base";
import { DrawerNavigationProp } from "@react-navigation/drawer";
import { DrawerParamList } from "../utils/drawer";
import { styles } from "./UserReviewStyles";
import { ReviewData, setReviews, TableMatterState } from "../../redux";
import { BackButton } from "../common/BackButton";
import UserReviewStars from "../review-page/UserReviewStars";
import { commonStyles } from "../common/commonStyles";
import { useRoute, RouteProp } from "@react-navigation/native";
import API from "../utils/api";
import { ScrollView } from "react-native-gesture-handler";
import { p1 } from "../utils/themes/darkTheme";

type ProfileNavigationProp = DrawerNavigationProp<DrawerParamList>;
type reviewScreenRoute = RouteProp<DrawerParamList, "Review">;

interface OwnProps {
  navigation: ProfileNavigationProp;
}

interface StateProps {
  userId?: string;
  userFirstName?: string;
  userLastName?: string;
  userEmailAddress?: string;
  userPhoneNumber?: string;
}

interface DispatchProps {
  setReviews: (reviews: ReviewData[]) => void;
}

type Props = StateProps & DispatchProps & OwnProps;

const UserReviewPage = (props: Props) => {
  const route = useRoute() as reviewScreenRoute;

  const [reviewData, setReviewData] = React.useState<ReviewData>({
    reviewId: route.params.reviewId,
    reservationId: route.params.reservationId,
    customerId: route.params.customerId,
    restaurantId: route.params.restaurantId,
    reviewerName: route.params.reviewerName,
    rating: route.params.rating,
    content: route.params.content,
  });

  const [failed, setFailed] = React.useState("");

  useEffect(() => {
    if (route.params.content) {
      setReviewData((prevState) => ({
        ...prevState,
        reviewId: route.params.reviewId,
        reservationId: route.params.reservationId,
        customerId: route.params.customerId,
        restaurantId: route.params.restaurantId,
        reviewerName: route.params.reviewerName,
        rating: route.params.rating,
        content: route.params.content
      }));
    } else {
      setReviewData((prevState) => ({
        ...prevState,
        reviewId: route.params.reviewId,
        reservationId: route.params.reservationId,
        customerId: route.params.customerId,
        restaurantId: route.params.restaurantId,
        reviewerName: route.params.reviewerName,
        rating: route.params.rating,
      }));
    }
  }, [route]);

  //Get review from provided review id
  // useEffect(() => {
  //   if (reviewData.reviewId !== undefined && reviewData.reviewId.length > 0) {
  //     API.get(`/review?reviewId=${reviewData.reviewId}`)
  //       .then((result) => {
  //         if (result.status === 200) {
  //           const reviewData = result.data[0];
  //           console.log('review data call');
  //           console.log(reviewData);
  //           setReviewData((prevState) => ({
  //             ...prevState,
  //             reviewerName: reviewData.reviewerName,
  //             rating: reviewData.rating,
  //             content: reviewData.content,
  //           }));
  //         } else {
  //           console.log('An unexpected error occurred while trying to retrieve reviews for user');
  //         }
  //       })
  //       .catch((error) => {
  //         console.log('An error occurred while trying to retrieve reviews for user');
  //         console.log(error);
  //       });
  //   }

  // }, []);

  const submitReview = () => {
    //First check if there is a valid rating selected
    if (reviewData.rating === undefined || reviewData.rating === 0) {
      setFailed("Please provide a rating");
    } else {
      setFailed("");
      //Check the provided review id to see if this is a new or existing review
      if (reviewData.reviewId !== undefined && reviewData.reviewId.length > 0) {
        //Review already exists, need to update in the database
        API.patch('/review', {
          reviewId: reviewData.reviewId, reservationId: reviewData.reservationId, customerId: reviewData.customerId,
          restaurantId: reviewData.restaurantId, reviewerName: reviewData.reviewerName, rating: reviewData.rating, content: reviewData.content
        })
          .then((result) => {
            if (result.status) {
              updateReviews();
              props.navigation.goBack();
            } else {
              // Give response saying sign-up was unsuccessful
              console.log('Review failed: ' + result.status);
            }
          })
          .catch((error) => {
            // Give response saying review submission was unsuccessful
            setFailed(error.message);
          });
      } else {
        //Review does not exist, needs to be created in the database
        API.post('/review', {
          reservationId: reviewData.reservationId, customerId: reviewData.customerId,
          restaurantId: reviewData.restaurantId, reviewerName: reviewData.reviewerName, rating: reviewData.rating, content: reviewData.content
        })
          .then((result) => {
            if (result.data.status) {
              updateReviews();
              props.navigation.goBack();
            } else {
              console.log('Review failed: ' + result);
            }
          })
          .catch((error) => {
            // Give response saying review submission was unsuccessful
            setFailed(error.message);
          });
      }

    }
  };

  const updateReviews = () => {
    API.get(`/review?userId=${props.userId}`)
      .then((result) => {
        if (result.status === 200) {
          const reviewsListData = result.data;
          const reviewsList: ReviewData[] = [];

          reviewsListData.map((value) => {
            reviewsList.push({
              reviewId: value._id,
              reservationId: value.reservationId,
              customerId: value.customerId,
              restaurantId: value.restaurantId,
              reviewerName: value.reviewerName,
              rating: value.rating,
              content: value.content,
            });
          });

          props.setReviews(reviewsList);
        } else {
          console.log('An unexpected error occurred while trying to retrieve reviews for user');
        }
      })
      .catch((error) => {
        console.log('An error occurred while trying to retrieve reviews for user');
        console.log(error);
      });
  };

  const updateRating = (newRating: number) => {
    setReviewData((prevState) => ({
      ...prevState,
      rating: newRating
    }));
  };

  const updateContent = (newContent: string) => {
    setReviewData((prevState) => ({
      ...prevState,
      content: newContent
    }));
  };

  const updateReviewerName = (name?: string) => {
    //If the name has not been set then set it to the user's first name, otherwise turn it back to undefined so the user is anonymous
    if (!reviewData.reviewerName) {
      setReviewData((prevState) => ({
        ...prevState,
        reviewerName: name
      }));
    } else {
      setReviewData((prevState) => ({
        ...prevState,
        reviewerName: undefined
      }));
    }
  };

  return (
    <View style={commonStyles.container}>
      {/*Header section of the page*/}
      <Header style={commonStyles.header}>
        <Left style={commonStyles.flex1}>
          <BackButton />
        </Left>
        <Body style={commonStyles.body}>
          <Text style={commonStyles.title}>Review Restaurant</Text>
        </Body>
        <Right style={commonStyles.flex1} />
      </Header>

      <ScrollView>
        {/*review image*/}
        <View style={styles.reviewImageContainer}>
          <Image
            source={require('../../../Assets/PlaceholderGraphic.jpg')}
            style={styles.image}
          />
        </View>
        {/*review stars*/}
        <View style={commonStyles.content}>
          <Text style={commonStyles.label}>Rating</Text>
          <UserReviewStars rating={reviewData.rating} updateRatingFunction={updateRating} />
          {/*review text*/}
          <Text style={commonStyles.label}>Comments</Text>
          <Item rounded style={styles.reviewFeedbackBox}>
            <Input
              style={{ textAlignVertical: "top", fontSize: 16 }}
              value={reviewData.content}
              onChangeText={(value) => updateContent(value)}
              multiline={true}
              numberOfLines={9}
              maxLength={250}
              placeholder="Please leave your feedback here..." />
          </Item>
          <ListItem onPress={() => updateReviewerName(props.userFirstName)}>
            <CheckBox
              checked={!reviewData.reviewerName ? true : false}
              color={p1}
            />
            <Text style={styles.reviewerNameCheckboxText}>Keep me anonymous</Text>
          </ListItem>
          <Text style={[styles.placeholder, (!failed && styles.hidden), styles.alert]}>
            {failed}
          </Text>

          <View style={commonStyles.buttonView}>
            <Button style={commonStyles.button} rounded={true}>
              {/** for now this button simply returns to the previous page */}
              <Text onPress={() => submitReview()} style={commonStyles.buttonText}>
                SUBMIT REVIEW
              </Text>
            </Button>
          </View>
        </View>
      </ScrollView>
    </View>
  );
};

const mapStateToProps = (state: TableMatterState): StateProps => {
  return {
    userId: state.user.id,
    userFirstName: state.user.firstName,
    userLastName: state.user.lastName,
    userEmailAddress: state.user.email,
    userPhoneNumber: state.user.phone,
  };
};

const mapDispatchToProps: DispatchProps = {
  setReviews,
};

// @ts-ignore
export default connect<StateProps, DispatchProps>(mapStateToProps, mapDispatchToProps)(UserReviewPage);
