import React from 'react'; //Removed useState for now until process hookup
import { Text, View, ScrollView, } from 'react-native';

//Removed DatePickerIOS and Modal from react-native to resolve lint errors
import { styles } from './SummaryStyles';
import { Header, Body, Left, Right, List, ListItem, Icon } from 'native-base';
import { BackButton } from '../../common/BackButton';
import { Reservation } from '../../../redux/types/serverTypes';
import { TableMatterState, UserState, setSelectedReservation, setTableNumbers } from '../../../redux';
import { connect } from 'react-redux';
import { p1 } from '../../utils/themes/lightTheme';
import MenuItemDisplay from '../../main-app/restaurants/menu/MenuItem';
import MapView, { Marker, Callout } from 'react-native-maps';
import { commonStyles } from '../../common/commonStyles';
import moment from "moment";
import { Grid, Row, Col } from 'react-native-easy-grid';
import { displayParty, displayEndTime } from '../../utils/helpers';

interface StateProps {
  reservation: Reservation | null;
  name: string;
  customer: UserState;
}
interface DispatchProps {
  setTableNumbers: (tableNumbers: number[]) => void;
  setSelectedReservation: (selectedReservation: Reservation | null) => void;
}

type Props = DispatchProps & StateProps;

const ReservationSummaryPage = (props: Props) => {
  const handleExit = () => {
    setTableNumbers([]);
    setSelectedReservation(null);
  };

  if (props.reservation) {
    return (
      <View style={commonStyles.container}>
        <Header style={commonStyles.header}>
          <Left style={commonStyles.flex1}>
            <BackButton onPress={handleExit} />
          </Left>
          <Body style={commonStyles.body}>
            <Text testID="welcome-title" style={commonStyles.title}>
              Reservation Summary
          			</Text>
          </Body>
          <Right style={commonStyles.flex1}>
          </Right>
        </Header>

        <ScrollView style={commonStyles.flex1}>
          <View style={styles.body}>
            <View style={{ alignItems: 'center', marginVertical: 15 }}>
              <Text style={styles.subtitle}>Your reservation for</Text>
              <Text style={styles.title}>{props.reservation.restaurant.name}</Text>
            </View>

            <Grid style={{ marginBottom: 20 }}>
              <Row style={styles.row}>
                <Col style={styles.leftCol}>
                  <Row style={{ alignItems: 'center', justifyContent: 'center' }}>
                    <Icon name="calendar" style={styles.icon}></Icon>
                    <Text style={styles.detailTitle}>Date</Text>
                  </Row>
                  <Text style={styles.detailText}>
                    {moment(props.reservation.timeBooked).format('MMMM Do YYYY')}
                  </Text>
                </Col>
                <Col style={styles.rightCol}>
                  <Row style={{ alignItems: 'center', justifyContent: 'center' }}>
                    <Icon name="time" style={styles.icon}></Icon>
                    <Text style={styles.detailTitle}>Time</Text>
                  </Row>
                  <Text>
                    {moment(props.reservation.timeBooked).format('h:mm a') + ' - ' + displayEndTime(props.reservation.timeBooked, props.reservation.duration)}
                  </Text>
                </Col>
              </Row>

              <Row style={styles.row}>
                <Col>
                  <Row style={{ alignItems: 'center', justifyContent: 'center' }}>
                    <Icon name="people" style={styles.icon}></Icon>
                    <Text style={styles.detailTitle}>Party</Text>
                  </Row>
                  <Text style={styles.detailText}>
                    {props.reservation.party &&
                      <Text>
                        {displayParty(props.reservation.party) + ' at Table(s) ' + props.reservation.tableNumbers.join(", ")}
                      </Text>
                    }
                  </Text>
                </Col>
              </Row>
              <Row style={styles.row}>
                <Col>
                  <Row style={{ alignItems: 'center', justifyContent: 'center' }}>
                    <Icon name="location" type="Entypo" style={styles.icon}></Icon>
                    <Text style={styles.detailTitle}>Address</Text>
                  </Row>
                  <Text style={styles.detailText}>
                    {props.reservation.restaurant.addressLine1} {/* Restaurant's address */}
                  </Text>
                </Col>
              </Row>
            </Grid>


            {props.reservation.orders &&
              <List style={{ marginBottom: 20 }}>
                <Text style={[styles.title, { marginBottom: 10 }]}>Your Order</Text>
                {props.reservation.orders.map((item, key) => <MenuItemDisplay key={key} menuItem={item} orderSummary />)}
                <View style={{ borderBottomColor: p1, borderBottomWidth: 1, }} />
                <ListItem style={{ marginHorizontal: 0 }}>
                  <View style={styles.totalRow}>
                    <Text style={styles.total}>Total Cost:</Text>
                    <Text style={styles.total}>${props.reservation.totalPrice.toFixed(2)}</Text>
                  </View>
                  {/* <Body>
                    <Text style={styles.total}>Total Cost:</Text>
                  </Body>
                  <Right style={{ alignContent: "center" }}>
                    <Text style={[styles.price, styles.total]}>${props.reservation.totalPrice.toFixed(2)}</Text>
                  </Right> */}
                </ListItem>
              </List>
            }
            <Text style={styles.title}>Restaurant Location</Text>
            <View style={styles.mapContainer}>
              <MapView
                style={styles.map}
                region={{
                  latitude: 37.8025268,
                  longitude: -122.4351449,
                  latitudeDelta: 0.0011111,
                  longitudeDelta: 0.011111,
                }}>
                <Marker
                  coordinate={{ latitude: 37.8025259, longitude: -122.4351431 }}>
                  <Callout>
                    <Text style={{ color: 'red', fontWeight: 'bold' }}>
                      Chicken Frenzy
                				</Text>
                  </Callout>
                </Marker>
              </MapView>
            </View>
          </View>
        </ScrollView>
      </View>
    );
  } else {
    return (
      <View>
        <Text>There was an error.</Text>
        <Text>Reservation: {props.reservation}</Text>
      </View>
    );
  };
};

const mapStateToProps = (state: TableMatterState): StateProps => {
  return {
    reservation: state.selectedReservation,
    name: state.user.firstName! || 'unknown',
    customer: state.user!
  };
};

const mapDispatchToProps: DispatchProps = {
  setSelectedReservation,
  // setSelectedRestaurant,
  // setParty,
  setTableNumbers,
  // setDate,
  // setDuration,
  // setPaid
};

// IDK why error appears it doesnt affect it from working (Add this on components until it is fixed)
// @ts-ignore
export default connect<StateProps, DispatchProps>(mapStateToProps, mapDispatchToProps)(ReservationSummaryPage);