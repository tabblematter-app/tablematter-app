import * as chai from "chai";
const server=require("../index");
import chaiHttp = require('chai-http');
chai.use(chaiHttp);
const should = chai.should();
import { Reservation } from "../types/types";

const reservation: Reservation = {
    customer: {
        id: "gg",
        firstName: "gg",
        lastName: "g",
        phone: "jj",
    },
    restaurant: {
        id: "",
        name: "",
        addressLine1: "",
        addressLine2: "",
        suburb: "",
        state: "",
        phone: "",
    },
    tableNumbers: [],
    orders: [],
    totalPrice: 0,
    party: {
        size: 0,
        childCount: 0,
        adultCount: 0,
        elderCount: 0,
        disabledCount: 0,
    },
    timeBooked: "",
    cancelled: false,
    duration: 0,
    finished: false,
};


describe("reservations", function(){
    describe ("get reservations", function(){
        it("get all reservations", (done) => {
            chai.request(server)
                .get("/reservation")
                .send({})
                .end(({},res: any)=>{
                    res && res.should.have.status(200);
                    done();
                });
        });

        it("get user reservations", (done) => {
            chai.request(server)
                .get("/reservations?userId=5df06124548aa152b5ea6918")
                .send({})
                .end(({},res: any)=>{
                    res && res.should.have.status(200);
                    done();
                });
        });
    });

    describe("add reservation", function(){
        it("should add a new reservation", (done) => {
            chai.request(server)
            .post("/reservation")
            .send({
                reservation: reservation,
            })
            .end(({},res: any)=>{
                res && res.should.have.status(200);
                done();
            });
        });
    });

    describe("update reservation", function(){
        it("should update a reservation", (done) => {
            chai.request(server)
            .patch("/reservation")
            .send({
                reservationId: "noId",
                reservation: reservation,
            })
            .end(({},res: any)=>{
                res && res.should.have.status(200);
                done();
            });
        });
    });

    describe("delete reservation", function(){
        it("should delete a reservation", (done) => {
            chai.request(server)
            .delete("/reservation/noId")
            .send({})
            .end(({},res: any)=>{
                res && res.should.have.status(200);
                done();
            });
        });
    });
});