import * as express from "express";
import { Response } from "express";
import { findAll } from "../controllers/model.functions";
import reservationModel from "../models/reservationModel";
import { getUserReservations, addReservation, updateReservation, deleteReservation } from "../controllers/reservation.controller";

export default (app: express.Express) => {
    app.get("/reservation", (_, res: Response) => findAll(res, reservationModel));
    
    app.get("/reservations", (req, res: Response) => getUserReservations((req.query.userId as string) || "", res));

    app.post("/reservation", (req, res: Response) => addReservation(req.body.reservation, res));
    app.patch("/reservation", (req, res: Response) => updateReservation(req.body.reservationId, req.body.reservation, res));
    app.delete("/reservation/:id", (req, res: Response) => deleteReservation(req.params.id, res));
};
