import { CounterActions, CounterState } from "./counterTypes";
import { UserActions, UserState } from "./userTypes";
import { Reservation, Restaurant } from "./serverTypes";
import { ReservationActionTypes, NewReservationState } from "./reservationTypes";

export * from "./counterTypes";
export * from "./userTypes";
export * from "./reservationTypes";

export interface ReviewData {
  reviewId?: string;
  reservationId: string;
  customerId: string;
  restaurantId: string;
  reviewerName?: string;
  rating?: number;
  content?: string;
}

export interface TableMatterState {
  counter: CounterState;
  user: UserState;
  reservations: Reservation[];
  searchResults: Restaurant[];
  selectedRestaurant: Restaurant | null;
  selectedReservation: Reservation | null;
  newReservation: NewReservationState;
  reviews: ReviewData[];
}

export const SET_USER_RESERVATIONS_ACTION = "SET_USER_RESERVATIONS_ACTION";
export interface SetUserReservationsAction {
  type: typeof SET_USER_RESERVATIONS_ACTION;
  reservations: Reservation[];
}

export const SET_SEARCH_RESULTS_ACTION = "SET_SEARCH_RESULTS_ACTION";
export interface SetSearchResultsAction {
  type: typeof SET_SEARCH_RESULTS_ACTION;
  searchResults: Restaurant[];
}

export const SET_REVIEWS_ACTION = "SET_REVIEWS_ACTION";
export interface SetReviewsAction {
  type: typeof SET_REVIEWS_ACTION;
  reviews: ReviewData[];
}


export const SET_SELECTED_RESTAURANT_ACTION = "SET_SELECTED_RESTAURANT_ACTION";
export interface SetSelectedRestaurantAction {
  type: typeof SET_SELECTED_RESTAURANT_ACTION;
  selectedRestaurant: Restaurant | null;
}

export const SET_SELECTED_RESERVATION_ACTION = "SET_SELECTED_RESERVATION_ACTION";
export interface SetSelectedReservationAction {
  type: typeof SET_SELECTED_RESERVATION_ACTION;
  selectedReservation: Reservation | null;
}

export type TableMatterActionTypes =
  CounterActions |
  UserActions |
  SetUserReservationsAction |
  SetSearchResultsAction |
  SetSelectedRestaurantAction |
  SetSelectedReservationAction |
  ReservationActionTypes |
  SetReviewsAction;
