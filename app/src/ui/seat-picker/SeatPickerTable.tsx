import { Icon, Text } from "native-base";
import React, { useState } from "react";
import { View, TouchableOpacity } from "react-native";
import { connect } from "react-redux";
import { TableMatterState, setTableNumbers } from "../../redux";
import { styles } from "./seatPickerStyles";
import { Wall } from "./SeatPickerWall";

export enum TableType {
    square,
    round,
    rectangle,
    Blank
}

export interface Floor {
    floorId: string;
    layoutId: string;
    width: number;
    length: number;
    walls: Wall[];
    tables: Table[];
    stairs: Stair[];
}

export interface Stair {
    direction: string;
    x: number;
    y: number;
    width: number;
    length: number;
}

export interface Table {
    number: number;
    shape: TableType;
    capacity: number;
    x: number;
    y: number;
    width: number;
    length: number;
    available: boolean;
}

interface StateProps {
    tableNumbers: number[];
}

interface DispatchProps {
    setTableNumbers: (tableNumbers: number[]) => void;
}

type Props = Table & DispatchProps & StateProps;

const SeatPickerTable = (props: Props) => {
    const [selected, selectTable] = useState(false);

    const performSelection = (props: Props) => {
        if (props.available) {
            selectTable(!selected);
        };

        const currTables = props.tableNumbers;

        if(!selected) {
            currTables.push(props.number);
        } else {
            const index = currTables.indexOf(props.number);
            if(index > -1) {
                currTables.splice(index, 1);
            }
        }

        props.setTableNumbers(currTables);
    };

    const defaultClass = props.available ? styles.tableUnselected : styles.tableUnavailable;

    let table = (
        <View style={[styles.squareTable, selected ? styles.tableSelected : defaultClass]}>
            <Icon style={{ paddingLeft: '10%', paddingTop: '15%' }} type="MaterialIcons" name="event-seat">
                <Text>{props.capacity}</Text>
            </Icon>
        </View>
    );

    switch (props.shape) {
        case (TableType.square):
            break;
        case (TableType.round):
            table = <View style={[styles.circleTabel, selected ? styles.tableSelected : defaultClass]} >
                <Icon style={{ paddingLeft: '10%', paddingTop: '15%' }} type="MaterialIcons" name="event-seat">
                    <Text>{props.capacity}</Text>
                </Icon>
            </View>;
            break;
        case (TableType.rectangle):
            if (props.width > props.length) {
                table = <View style={[styles.rectangleWideTabel, selected ? styles.tableSelected : defaultClass]}>
                    <Icon style={{ paddingLeft: '40%', paddingTop: '8%' }} type="MaterialIcons" name="event-seat">
                        <Text>{props.capacity}</Text>
                    </Icon>
                </View>;
            }
            else if (props.length > props.width) {
                table = <View style={[styles.rectangleLongTabel, selected ? styles.tableSelected : defaultClass]}>
                    <Icon style={{ paddingLeft: '20%', paddingTop: '50%' }} type="MaterialIcons" name="event-seat">
                        <Text>{props.capacity}</Text>
                    </Icon>
                </View>;
            }
            break;
        case (TableType.Blank):
            //table = <View style={[styles.blankTable, selected ? styles.tableSelected : defaultClass]}/>;
            table = <View style={[styles.blankTable]} />;
            break;
    }

    return (
        <View>
            <TouchableOpacity onPress={() => { performSelection(props); }}>
                {table}
            </TouchableOpacity>
        </View>
    );
};

interface DispatchProps {
    setTableNumbers: (tableNumbers: number[]) => void;
}

const mapDispatchToProps: DispatchProps = {
    setTableNumbers
};

const mapStateToProps = (state: TableMatterState): StateProps => {
    return {
        tableNumbers: state.newReservation.tableNumbers
    };
};

export default connect<StateProps, DispatchProps>(mapStateToProps, mapDispatchToProps)(SeatPickerTable);
