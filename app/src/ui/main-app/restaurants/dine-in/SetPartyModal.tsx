import React, { useState } from 'react';
import { Card, CardItem, View, Button, Text, Body, Icon } from "native-base";
import { TouchableOpacity } from 'react-native';
import Modal from "react-native-modal";
import { modalStyles } from "../addModalStyles";
import { styles } from './DineInStyles';
import { commonStyles } from '../../../common/commonStyles';
import { Col } from 'react-native-easy-grid';
import { Party } from '../../../../redux/types/serverTypes';
import { connect } from 'react-redux';
import { TableMatterState, setParty } from '../../../../redux';

interface OwnProps {
  toggleModal: () => void;
}

interface StateProps {
  party: Party;
}

interface DispatchProps {
  setParty: (party: Party) => void;
}

type Props = OwnProps & DispatchProps & StateProps;

const SetPartyModal = (props: Props) => {
  const [partyAdultCounter, setPartyAdultCounter] = useState(props.party.adultCount ? props.party.adultCount : 1);
  const [partyElderCounter, setPartyElderCounter] = useState(props.party.elderCount);
  const [partyChildCounter, setPartyChildCounter] = useState(props.party.childCount);
  // const [partyDisabledCounter, setPartyDisabledCounter] = useState(0);

  const saveParty = (save: boolean) => {
    if (save) {
      props.setParty({
        size: partyChildCounter + partyAdultCounter + partyElderCounter,
        childCount: partyChildCounter,
        adultCount: partyAdultCounter,
        elderCount: partyElderCounter,
        disabledCount: 0
      });
    }
    props.toggleModal();
  };

  return (
    <Modal isVisible={true}>
      <Card>
        <CardItem header style={commonStyles.header}>
          <Text style={modalStyles.cardHeaderText}>Edit Party</Text>
        </CardItem>

        <CardItem>
          <Body>
            <View style={modalStyles.row}>
              <Col style={{ marginRight: 10 }}>
                <Text style={commonStyles.label}>Adults</Text>
                <View style={[commonStyles.input]}>
                  <View style={styles.durationView}>
                    <TouchableOpacity
                      onPress={() => {
                        partyAdultCounter > 1 && setPartyAdultCounter(partyAdultCounter - 1);
                      }}>
                      <Icon name="remove" style={styles.icon}></Icon>
                    </TouchableOpacity>
                    <Text style={styles.partyText}>{partyAdultCounter}</Text>
                    <TouchableOpacity
                      onPress={() => {
                        partyAdultCounter < 30 && setPartyAdultCounter(partyAdultCounter + 1);
                      }}>
                      <Icon name="add" style={styles.icon}></Icon>
                    </TouchableOpacity>
                  </View>
                </View>
              </Col>

              <Col style={{ marginLeft: 10 }}>
                <Text style={commonStyles.label}>Seniors</Text>
                <View style={[commonStyles.input]}>
                  <View style={styles.durationView}>
                    <TouchableOpacity
                      onPress={() => {
                        partyElderCounter > 0 && setPartyElderCounter(partyElderCounter - 1);
                      }}>
                      <Icon name="remove" style={styles.icon}></Icon>
                    </TouchableOpacity>
                    <Text style={styles.partyText}>{partyElderCounter}</Text>
                    <TouchableOpacity
                      onPress={() => {
                        partyElderCounter < 30 && setPartyElderCounter(partyElderCounter + 1);
                      }}>
                      <Icon name="add" style={styles.icon}></Icon>
                    </TouchableOpacity>
                  </View>
                </View>
              </Col>
            </View>

            <View style={modalStyles.row}>
              <Col style={{ marginRight: 10 }}>
                <Text style={commonStyles.label}>Children</Text>
                <View style={[commonStyles.input]}>
                  <View style={styles.durationView}>
                    <TouchableOpacity
                      onPress={() => {
                        partyChildCounter > 0 && setPartyChildCounter(partyChildCounter - 1);
                      }}>
                      <Icon name="remove" style={styles.icon}></Icon>
                    </TouchableOpacity>
                    <Text style={styles.partyText}>{partyChildCounter}</Text>
                    <TouchableOpacity
                      onPress={() => {
                        partyChildCounter < 30 && setPartyChildCounter(partyChildCounter + 1);
                      }}>
                      <Icon name="add" style={styles.icon}></Icon>
                    </TouchableOpacity>
                  </View>
                </View>
              </Col>

              <Col style={{ marginLeft: 10 }}>
                {/* <Text style={commonStyles.label}>Disabled</Text>
                <View style={[commonStyles.input]}>
                  <View style={styles.durationView}>
                    <TouchableOpacity
                      onPress={() => {
                        partyDisabledCounter > 1 && setPartyDisabledCounter(partyDisabledCounter - 1);
                      }}>
                      <Icon name="remove" style={styles.icon}></Icon>
                    </TouchableOpacity>
                    <Text style={styles.partyText}>{partyDisabledCounter}</Text>
                    <TouchableOpacity
                      onPress={() => {
                        partyDisabledCounter < 30 && setPartyDisabledCounter(partyDisabledCounter + 1);
                      }}>
                      <Icon name="add" style={styles.icon}></Icon>
                    </TouchableOpacity>
                  </View>
                </View> */}
              </Col>
            </View>
          </Body>
        </CardItem>

        <CardItem footer>
          <Button transparent style={commonStyles.cardButton} onPress={() => saveParty(false)}>
            <Text style={commonStyles.cardButtonText}>Cancel</Text>
          </Button>
          <Button transparent style={commonStyles.cardButton} onPress={() => saveParty(true)}>
            <Text style={commonStyles.cardButtonText}>Update</Text>
          </Button>
        </CardItem>
      </Card>
    </Modal>
  );
};

const mapStateToProps = (state: TableMatterState): StateProps => {
  return {
    party: state.newReservation.party,
  };
};

const mapDispatchToProps: DispatchProps = {
  setParty,
};

// IDK why error appears it doesnt affect it from working (Add this on components until it is fixed)
// @ts-ignore
export default connect<{}, DispatchProps>(mapStateToProps, mapDispatchToProps)(SetPartyModal);