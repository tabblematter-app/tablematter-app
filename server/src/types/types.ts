
export enum DietaryStatusEnum {
    GF = "GlutenFree",
    VEG = "Vegetarian",
    VEGAN = "Vegan",
}

export enum CategoryEnum {
    MAIN = "main",
    APPETIZER = "appetizer",
    DESSERT = "dessert",
    BEVERAGES = "beverages"
}

export enum AlergensEnum {
    MILK = "Milk", 
    EGG = "Egg",
    NUTS = "Nuts",
    SHELLFISH = "Shellfish",
}

export enum MealTypeEnum {
    APP = "appeteiser",
    MAIN = "main",
    DES = "desert",
    SPEC = "specialty",
}

export enum TableShapeEnum {
    SQUARE = "Square", 
    CIRCLE = "Circle",
    RECT = "Rectangle",
    SEMICIRC = "Semicircle",
    OTHER = "Other",
}

export enum TableStatusEnum {
    OPEN = "Open",
    OCCUPIED = "Occupied",
    RESERVED = "Reserved",
    CLOSED = "Closed",
}

export enum StaffPrivilegeEnum {
    ADMIN = "Admin",
    STANDARD = "Standard",
}

export const defaultOpenHours = [
    // [startHour, endHour]
    ["0800", "1830"],
    ["0800", "1830"],
    ["0800", "1830"],
    ["0800", "1830"],
    ["0800", "1830"],
    null,
    null,
];

export interface Image {
    link: string;
    alt: string;
}

export interface MenuItem {
    _id: string; // This will likely need to be debugged after we add front end
    name: string;
    desc: string;
    price: number;
    discount: number;
    category: CategoryEnum;
    images: Image[];
    tags: DietaryStatusEnum[];
    allergens: AlergensEnum[];
}

export interface Customer {
    profilePictureUrl: string;
    firstName: string;
	lastName: string;
	password: string;
	email: string;
	phone: string;
}

export interface Review {
    restaurantId: string;
    customerId?: string; // not necessary for anon reviews
    reviewerName: string;
    rating: number;
    content: string;
}

export interface Staff {
    firstName: string;
    lastName: string;
    password: string;
    email: string;
    phone: string;
}

export interface Table {
    number: number;
    status: TableStatusEnum;
    capacity: number;
    shape: TableShapeEnum;
}

export interface CustomerShort {
    id: string;
    firstName: string;
    lastName: string;
    phone: string;
}

export interface RestaurantShort {
    id: string;
    name: string;
    addressLine1: string;
    addressLine2: string;
    suburb: string;
    state: string;
    phone: string;
}

export interface Party {
    size: number;
    childCount: number;
    adultCount: number;
    elderCount: number;
    disabledCount: number;
}

export interface Reservation {
    customer: CustomerShort;
    restaurant: RestaurantShort;
    orders: MenuItem[];
    totalPrice: number;
    timeBooked: string; // figure out how to date in ts
    finished: boolean;
    cancelled: boolean;

    dineIn: boolean;

    tableNumbers: number[];
    party?: Party;
    duration: number;
    paid: boolean;
}

export interface StaffShort {
    staffId: string;
    privilege: StaffPrivilegeEnum;
}

export interface Restaurant {
    _id: string;
    name: string;
    desc: string;
    openHours: any; // TODO clarify type
    phone: string;
    email: string;
    website: string;
    addressLine1: string;
    addressLine2: string;
    suburb: string;
    state: string;
    images: Image[];
    menu: MenuItem[];
    tables: Table[];
    staff: StaffShort[];
    averageRating: number;
}