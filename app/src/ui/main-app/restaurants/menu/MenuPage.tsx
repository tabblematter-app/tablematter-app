import React from 'react';
import { Platform } from "react-native";
import { Header, Body, Title, View, Left, Right, Text, List, ListItem, Button } from "native-base";
import { ScrollView } from 'react-native-gesture-handler';
import { connect } from 'react-redux';
import { BackButton } from '../../../common/BackButton';
import { styles } from "../restaurantsStyles";
import { Restaurant, CategoryEnum } from '../../../../redux/types/serverTypes';
import { TableMatterState } from '../../../../redux';
import { defaultRestaurant } from '../RestaurantPage';
import MenuItemDisplay from './MenuItem';
import { useRoute, RouteProp, useNavigation } from '@react-navigation/native';
import { DrawerParamList } from '../../../utils/drawer';
import { commonStyles } from '../../../common/commonStyles';

type menuScreenRoute = RouteProp<DrawerParamList, "Menu">;

interface StateProps {
  restaurant: Restaurant;
}

type Props = StateProps;

const MenuPage = (props: Props) => {
  const route = useRoute() as menuScreenRoute;
  const isReservation = route.params?.reservation || false;
  const isDineIn = route.params?.dineIn || false;
  const navigateToConfirmation = route.params?.navigateToConfirmation || false;
  const navigation = useNavigation();

  return (
    <View style={commonStyles.container}>
      <Header style={commonStyles.header}>
        <Left style={commonStyles.flex1}>
          <BackButton />
        </Left>
        <Body style={commonStyles.body}>
          <Title testID="menu-title" style={commonStyles.title}>Menu</Title>
        </Body>
        <Right style={commonStyles.flex1} />
      </Header>
      {/*  style={{ marginBottom: Platform.OS === "ios" ? 150 : 70 }} */}
      <ScrollView style={{ marginBottom: Platform.OS === "ios" ? 150 : 20 }} >
        <List>
          <ListItem itemDivider itemHeader style={styles.listHeader}>
            <Text style={styles.category}>Appetizer</Text>
          </ListItem>
          {props.restaurant.menu.filter((item) => item.category === CategoryEnum.APPETIZER).map((item, key) => <MenuItemDisplay key={key} isReservation={isReservation} menuItem={item} />)}

          <ListItem itemDivider itemHeader style={styles.listHeader}>
            <Text style={styles.category}>Main</Text>
          </ListItem>
          {props.restaurant.menu.filter((item) => item.category === CategoryEnum.MAIN).map((item, key) => <MenuItemDisplay key={key} isReservation={isReservation} menuItem={item} />)}

          <ListItem itemDivider itemHeader style={styles.listHeader}>
            <Text style={styles.category}>Dessert</Text>
          </ListItem>
          {props.restaurant.menu.filter((item) => item.category === CategoryEnum.DESSERT).map((item, key) => <MenuItemDisplay key={key} isReservation={isReservation} menuItem={item} />)}
        </List>
      </ScrollView>
      {isReservation &&
        <View style={styles.borderTop}>
          <View style={commonStyles.buttonView}>
            {isDineIn ?
              <Button rounded style={commonStyles.button} onPress={() => navigateToConfirmation ? navigation.navigate("BookingDetailsPage", { hasMenu: true }) : navigation.navigate("DineInPage")}>
                <Text style={commonStyles.buttonText}>ADD ITEMS</Text>
              </Button>
              :
              <Button rounded style={commonStyles.button} onPress={() => navigation.navigate("OrderSummary")}>
                <Text style={commonStyles.buttonText}>ORDER NOW</Text>
              </Button>
            }
          </View>
        </View>
      }
    </View>
  );
};

const mapStateToProps = (state: TableMatterState): StateProps => {
  return {
    restaurant: state.selectedRestaurant || defaultRestaurant,
  };
};

// IDK why error appears it doesnt affect it from working (Add this on components until it is fixed)
// @ts-ignore 
export default connect<StateProps>(mapStateToProps)(MenuPage);
