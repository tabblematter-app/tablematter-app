import { StyleSheet } from "react-native";
import { p1, p3 } from "../utils/themes/lightTheme";

export const styles = StyleSheet.create({
  orderHistoryBlob: {
    marginBottom: 20,
    height: 175,
  },
  cardBody: {
    height: "100%",
  },
  inner: {
    flexDirection: 'column',
    justifyContent: 'space-between',
    flex: 1,
    padding: 20,
    height: "100%"
  },
  imageContainer: {
    width: "auto",
    height: "100%",
    flex: 1,
    padding: 10,
  },
  image: {
    width: "100%",
    height: undefined,
    flex: 1,
  },
  orderHistorySubtext: {
    color: "black",
    marginTop: 5
  },
  orderHistoryTextBold: {
    color: "black",
    fontWeight: "bold",
  },
  userInfoIcon: {
    color: p1,
  },
  ratingStarNotSelected: {
    color: p3,
  },
  reviewStarWrapper: {
    flexDirection: "row",
    marginTop: 10
  },
});