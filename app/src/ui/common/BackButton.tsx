import React from "react";
import Icon from "react-native-vector-icons/AntDesign";
import { useNavigation } from "@react-navigation/native";

{/*Generic back button that will return the user to the previous navigated screen.*/}
interface Props {
    onPress?: () => void;
}

export const BackButton = (props: Props) => {

    //self-created navigation object. Should work Universally.
    const navigation = useNavigation();

    return (
        <Icon name="arrowleft" size={30} color="white" onPress={() => {
            props.onPress && props.onPress();
            navigation.goBack();
        }}/>
    );
};
