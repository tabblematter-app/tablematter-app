import { Alert } from 'react-native';

export const showError =
    (alertTitle: string,
        alertMsg: string) => {
        Alert.alert(
            alertTitle,
            alertMsg,
            [
                {
                    text: "OK",
                    style: "default"
                }
            ]
        );
    };

export const promptBinaryResponse =
    (alertTitle: string,
        alertMsg: string,
        negativeButtonlabel: string,
        positiveButtonLabel: string,
        negativeCallback: any,
        positiveCallback: any
    ) => {
        Alert.alert(
            alertTitle,
            alertMsg,
            [
                {
                    text: negativeButtonlabel,
                    style: "cancel",
                    onPress: () => negativeCallback()
                },
                {
                    text: positiveButtonLabel,
                    style: "default",
                    onPress: () => positiveCallback()
                }
            ]
        );
    };