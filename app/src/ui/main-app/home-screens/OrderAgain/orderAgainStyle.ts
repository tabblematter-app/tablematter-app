import { StyleSheet } from "react-native";

export const styles = StyleSheet.create({
  orderAgainBlob: {
    height: 200
  },
  orderAgainText: {
    marginTop: 5,
  },
  reviewStarWrapper: {
    alignItems: "flex-start",
    flexWrap: "wrap",
    flexDirection: "row",
  },
  noBold: {
    fontWeight: "normal"
  },
  starStyle: {
    fontSize: 18,
    marginRight: 2
  },
});