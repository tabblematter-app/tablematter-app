import React, { useState } from "react";
import { connect } from "react-redux";
import { View, Text, Image, TextInput } from "react-native";
import { Header, Left, Body, Icon, Button, Right } from "native-base";
import { DrawerNavigationProp } from "@react-navigation/drawer";
import { DrawerParamList } from "../utils/drawer";
import { styles } from "./profilePageStyles";
import { commonStyles } from "../common/commonStyles";
import { TableMatterState } from "../../redux";
import { ScrollView, TouchableOpacity } from "react-native-gesture-handler";
import ImagePicker from "react-native-image-picker";
import Constants from "../utils/constants";
import { RNS3 } from "react-native-s3-upload";
import API from "../utils/api";
import { BackButton } from "../common/BackButton";
import { setUser } from '../../redux';

type ProfileScreenNavigationProp = DrawerNavigationProp<
  DrawerParamList
>;

interface OwnProps {
  navigation: ProfileScreenNavigationProp;
}

interface StateProps {
  userProfilePictureUrl?: string;
  userFirstName?: string;
  userLastName?: string;
  userEmailAddress?: string;
  userPhoneNumber?: string;
  userID?: string;
}

interface DispatchProps {
  setUser: (id: string, profilePictureUrl: string, firstName: string, lastName: string, email: string, phone: string) => void;
}

type Props = StateProps & OwnProps & DispatchProps;

// Modified code from the sign up page to hit the edit account endpoint
const EditProfile = (props: Props) => {
  const [profilePictureUrl, setProfilePictureUrl] = React.useState(props.userProfilePictureUrl ? props.userProfilePictureUrl : "");

  const [firstName, setFirstName] = useState(props.userFirstName);
  const [lastName, setLastName] = useState(props.userLastName);
  const [email] = useState(props.userEmailAddress);
  const [phone, setPhone] = useState(props.userPhoneNumber);

  const [id] = useState(props.userID);
  const [failed, setFailed] = useState("");

  const [firstNameError, setFirstNameError] = useState(false);
  const [lastNameError, setLastNameError] = useState(false);
  const [emailError, setEmailError] = useState(false);
  const [phoneError, setPhoneError] = useState(false);

  const handleUpdate = () => {
    firstName === "" ? setFirstNameError(true) : setFirstNameError(false);
    lastName === "" ? setLastNameError(true) : setLastNameError(false);
    email === "" ? setEmailError(true) : setEmailError(false);
    phone === "" ? setPhoneError(true) : setPhoneError(false);

    // For now we hard code the user id, as the state value is undefined and the success response does not return the id.

    if (!firstNameError && !lastNameError && !emailError && !phoneError) {
      console.log('Beginning profile update...');
      API.post('/editUserAccount', { id, firstName, lastName, email, phone })
        .then((result) => {
          if (result.data.status) {
            const user = result.data.data;

            // Put Details into Redux
            props.setUser(user._id, profilePictureUrl, user.firstName, user.lastName, user.email, user.phone);
            props.navigation.goBack();

          } else {
            // Give response saying update was unsuccessful
            setFailed("Failed to save profile update.");
          }
        })
        .catch((error) => {
          // Give response saying update was unsuccessful
          setFailed(error.message);
        });
    }
  };

  const handleImagePicker = () => {
    ImagePicker.showImagePicker(Constants.profilePicturePickerConfig, (file) => {
      if (file.didCancel) {

      } else if (file.error) {
        console.log("Image picker error");
      } else if (file.customButton) {
        setProfilePictureUrl("undefined");
        handleProfilePictureUpdate("undefined");
      } else {
        handleProfilePictureUpload(file);
      }
    });
  };

  const handleProfilePictureUpload = (file: any) => {
    file.name = props.userEmailAddress;
    RNS3.put(file, Constants.uploaderConfig).then((response: any) => {
      if (response.status == 201) {
        console.log("Successfully uploaded profile picture");
        const newURL = response.headers.Location + "?versionid=" + response.headers["x-amz-version-id"];
        console.log("Profile Pic: " + newURL);
        setProfilePictureUrl(newURL);
        handleProfilePictureUpdate(newURL);
      } else {
        console.log("Failed to upload profile picture");
      }
    }).catch(error => {
      console.log(error);
    });
  };

  const handleProfilePictureUpdate = (newURL) => {
    API.put("/user/picture/" + props.userEmailAddress + "/" + encodeURIComponent(newURL) + "/")
      .then((result) => {
        console.log(result.data.message);
        //Do success
      })
      .catch();
  };

  return (
    <View style={commonStyles.container}>
      <View>
        {/*Header section of the page*/}
        <Header style={commonStyles.header}>
          <Left style={commonStyles.flex1}>
            <BackButton />
          </Left>
          <Body style={commonStyles.body}>
            <Text style={commonStyles.title}>Edit Profile</Text>
          </Body>
          <Right style={commonStyles.flex1} />
          {/* {Platform.OS === "ios" && <Right />} */}
        </Header>
      </View>
      <ScrollView>
        {/*Profile picture and user name*/}
        <Button style={styles.profilePic} onPress={handleImagePicker}>
          {
            profilePictureUrl && profilePictureUrl !== "undefined" ?
              <Image style={styles.profilePic} source={{ uri: profilePictureUrl }} />
              :
              <Icon type="Ionicons" style={[styles.userInfoIcon, styles.profileIcon]} name="person" />
          }
        </Button>
        <View style={commonStyles.inputView}>
          <TouchableOpacity style={styles.altButton}>
            <Text onPress={handleImagePicker} style={styles.altButtonText}>Change Photo</Text>
          </TouchableOpacity>
          {/*User information*/}
          <Text style={commonStyles.label}>First Name</Text>
          <TextInput
            testID="name-input"
            style={commonStyles.input}
            placeholder="First Name"
            value={firstName}
            placeholderTextColor="red"
            onChangeText={(text: React.SetStateAction<string>) => setFirstName(text.toString())}
          />

          <Text style={commonStyles.label}>Last Name</Text>
          <TextInput
            testID="name-input"
            style={commonStyles.input}
            placeholder="First Name"
            value={lastName}
            placeholderTextColor="red"
            onChangeText={(text: React.SetStateAction<string>) => setLastName(text.toString())}
          />

          {/* <Text style={commonStyles.label}>Email</Text>
          <TextInput
            testID="email-input"
            style={commonStyles.input}
            placeholder="Email"
            value={email}
            placeholderTextColor="red"
            onChangeText={(text: React.SetStateAction<string>) => setEmail(text.toString())}
          /> */}
          <Text style={commonStyles.label}>Phone</Text>
          <TextInput
            testID="phone-input"
            style={commonStyles.input}
            placeholder="Phone"
            value={props.userPhoneNumber}
            placeholderTextColor="red"
            onChangeText={(text: React.SetStateAction<string>) => setPhone(text.toString())}
          />
          <Text style={[commonStyles.label, (!failed && styles.hidden), styles.alert]}>
            {failed}
          </Text>
          {/* <TouchableOpacity
          onPress={() => handleUpdate()}
          style={[styles.button, { marginBottom: Platform.OS === "ios" ? 200 : 120 }]}
        >
          <Text style={styles.text}>Save</Text>
        </TouchableOpacity> */}
        </View>
      </ScrollView>

      <View style={commonStyles.buttonView}>
        <Button rounded style={commonStyles.button} onPress={() => handleUpdate()}>
          <Text style={commonStyles.buttonText}>SUBMIT</Text>
        </Button>
      </View>
    </View>
  );
};

const mapStateToProps = (state: TableMatterState): StateProps => {
  return {
    userProfilePictureUrl: state.user.profilePictureUrl,
    userFirstName: state.user.firstName,
    userLastName: state.user.lastName,
    userEmailAddress: state.user.email,
    userPhoneNumber: state.user.phone,
    userID: state.user.id,
  };
};

const mapDispatchToProps: DispatchProps = {
  setUser,
};

// @ts-ignore 
export default connect<{}, DispatchProps>(mapStateToProps, mapDispatchToProps)(EditProfile);
