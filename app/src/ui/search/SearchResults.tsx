import React from 'react';
import { Platform, Text } from "react-native";
import { Header, Body, Title, View, Left, Right } from "native-base";
import { ScrollView } from 'react-native-gesture-handler';
import { BackButton } from '../common/BackButton';
import { Restaurant } from '../../redux/types/serverTypes';
import RestaurantBlob from '../main-app/restaurants/RestaurantBlob';
import { TableMatterState, setSelectedRestaurant } from '../../redux';
import { connect } from 'react-redux';
import { commonStyles } from '../common/commonStyles';


interface StateProps {
  searchResults: Restaurant[];
}

interface DispatchProps {
  setSelectedRestaurant: (selectedRestaurant: Restaurant | null) => void;
}

type Props = StateProps & DispatchProps;

const SearchResults = (props: Props) => {
  return (
    <View style={commonStyles.container}>
      <Header style={commonStyles.header}>
        <Left style={commonStyles.flex1}>
          <BackButton />
        </Left>
        <Body style={commonStyles.body}>
          <Title testID="search-results-title" style={commonStyles.title}>Search Results</Title>
        </Body>
        <Right style={commonStyles.flex1} />
      </Header>
      {/* Add Map with the search results drawing the correct information to the screen */}
      <View style={[commonStyles.content, { paddingTop: 20 }]}>
        {
          props.searchResults.length > 0 ?
            <ScrollView style={{ marginBottom: Platform.OS === "ios" ? 200 : 120 }}>
              {props.searchResults.map((restaurant, key) => <RestaurantBlob testID={`result-${key}`} key={restaurant.name} onPress={() => props.setSelectedRestaurant(restaurant)} restaurant={restaurant} rating={restaurant.averageRating} />)}
            </ScrollView>
            :
            <Text style={commonStyles.noResultsText}>
              Your search did not return any results.
            </Text>
          //TODO: Style this ^
        }
      </View>
    </View >
  );
};

const mapStateToProps = (state: TableMatterState): StateProps => {
  return {
    searchResults: state.searchResults,
  };
};

const mapDispatchToProps: DispatchProps = {
  setSelectedRestaurant,
};

// IDK why error appears it doesnt affect it from working (Add this on components until it is fixed)
// @ts-ignore 
export default connect<StateProps, DispatchProps>(mapStateToProps, mapDispatchToProps)(SearchResults);
