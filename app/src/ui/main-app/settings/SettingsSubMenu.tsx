import React from 'react';
import { Platform } from "react-native";
import { Header, Body, Title, View, Left, Right, Text } from "native-base";
import { ScrollView } from 'react-native-gesture-handler';
import { BackButton } from '../../common/BackButton';
import { commonStyles } from '../../common/commonStyles';

export enum SettingsType {
  Account,
  Payment,
  Delivery,
  Legal,
}

interface Props {
  settingsType: SettingsType;
  sectionName: string;
}

const SettingsSubMenu = ({ route }: any) => {
  
  // This identifies the settings section from passed parameters
  const { Props } = route.params;

  return (
    <View>
        <Header style={commonStyles.header}>
            <Left style={commonStyles.flex1}>
                <BackButton />
            </Left>
            <Body style={commonStyles.body}>
                <Title testID="menu-title" style={commonStyles.title}>{Props.sectionName}</Title>
            </Body>
            <Right style={commonStyles.flex1} />
        </Header>
        <ScrollView style={{marginBottom: Platform.OS === "ios" ? 150 : 70}}>
            {Props.settingsType == SettingsType.Legal ? 
            <View style={{ padding: 20 }}>
              <Text style={{ fontSize: 18, marginBottom: 10 }}>Placeholder Legal text:</Text>
              <Text>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Phasellus consectetur mauris a mauris volutpat finibus. Morbi laoreet elit eu eros convallis elementum. Fusce id mollis lacus. Aliquam non molestie ante. Donec blandit vel lacus sed tristique. Nullam varius arcu orci, sit amet fermentum enim gravida a. Proin consectetur odio massa, eu gravida risus sollicitudin et. Donec interdum tincidunt mauris, eu euismod lectus ullamcorper quis. Integer eget quam rhoncus nisl luctus pulvinar. Vestibulum vel tortor ut odio viverra molestie in iaculis nisl. Phasellus mauris turpis, aliquet et mollis a, dignissim ut diam. Curabitur finibus hendrerit neque ac ultrices. Etiam nisi augue, venenatis vel iaculis pulvinar, efficitur sit amet massa.</Text>
            </View> 
            : 
            <View />
            }
        </ScrollView>
    </View>
  );
};

export default SettingsSubMenu;
