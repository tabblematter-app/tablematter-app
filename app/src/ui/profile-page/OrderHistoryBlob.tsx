import React from "react";
import { View, Text, TouchableOpacity, Image } from "react-native";
import { Icon, Card, CardItem } from "native-base";
import { styles } from "./OrderHistoryStyles";
import { useNavigation } from "@react-navigation/native";
//import { TouchableOpacity } from "react-native-gesture-handler";

interface Props {
  orderHistoryID: string;
  restaurantName: string;
  price: number;
  dineIn: boolean;
  reviewId?: string;
  rating?: number;
  reviewText?: string;
  customerId: string;
  restaurantId: string;
  reviewerName?: string;
}

interface GetStarsProps {
  rating?: number;
}

interface StarProps {
  selected: boolean;
  half: boolean;
}

{/* To get star list */ }
const GetStars = (props: GetStarsProps) => {
  {/*loop through the stars value to correctly present the users rating*/ }
  const stars: Element[] = [];
  const rating = props.rating ? props.rating : 0;

  for (let x = 1; x <= 5; x++) {
    stars.push(
      <View key={stars.length}>
        <Star half={x - rating === 0.5 ? true : false} selected={x <= rating ? true : false} />
      </View>
    );
  }

  return (
    <View style={[styles.reviewStarWrapper]}>
      {stars}
    </View>
  );
};

{/*Star icon definition*/ }
const Star = (props: StarProps) => {
  return (
    <Icon name={props.half === true ? "star-half" : "star"} style={[styles.userInfoIcon, selectedState(props.selected || props.half)]} />
  );
};

{/*Conditional styling dictating the colour of the stars*/ }
const selectedState = (isSelected: boolean) => {
  if (!isSelected) {
    return styles.ratingStarNotSelected;
  } else {
    return '';
  }
};

const OrderHistoryBlob = (props: Props) => {
  //Self created navigation prop to avoid having to pass from parent
  const navigation = useNavigation();

  const reviewRestaurant = () => {
    navigation.navigate("UserReview", {
      reviewId: props.reviewId, reservationId: props.orderHistoryID, content: props.reviewText,
      customerId: props.customerId, restaurantId: props.restaurantId, reviewerName: props.reviewerName, rating: props.rating || 0
    });
  };

  return (
    <Card style={styles.orderHistoryBlob}>
      <CardItem cardBody style={styles.cardBody}>
        <View style={styles.inner}>
          <View>
            <Text style={styles.orderHistoryTextBold}>{props.restaurantName}</Text>
            {props.dineIn ? (
              <Text style={styles.orderHistorySubtext}>Dine-In</Text>
            ) : (
                <Text style={styles.orderHistorySubtext}>Takeaway</Text>
              )}
            {props.price && (
              <Text style={styles.orderHistorySubtext}>${props.price}</Text>
            )}
          </View>

          <TouchableOpacity
            onPress={reviewRestaurant}
          >
            <Text style={styles.orderHistoryTextBold}>Review</Text>
            <GetStars rating={props.rating} />
          </TouchableOpacity>
        </View>

        <View style={styles.imageContainer}>
          <Image
            source={require('../../../Assets/PlaceholderGraphic.jpg')}
            style={styles.image}
          />
        </View>
      </CardItem>
    </Card>
  );
};

export default OrderHistoryBlob;
