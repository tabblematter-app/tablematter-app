import { Document, Model } from "mongoose";
import { Customer } from "../types/types";
import { Response } from "express";
import helper from "../utils/responseHelper";
import constants from "../utils/constants";
import customerModel from "../models/customerModel";

export function logIn(email: string, password: string, res: Response, model: Model<Document & Customer>) {
    model.findOne({ email, password })
        .then((data) => {
            if (!data) {
                return res.send(helper.failResponse(Error("UserNotFound"), constants.fail.login));
            };

            const customerNoPass = {
                id: data._id,
                profilePictureUrl: data.profilePictureUrl,
                firstName: data.firstName,
                lastName: data.lastName,
                email,
                phone: data.phone,
            };

            return res.send(helper.successResponse(customerNoPass, constants.success.login));
        })
        .catch((err) => {
            return res.send(helper.failResponse(err, constants.fail.internalError));
        });
};

export function setProfilePicture(email: string, url: string, res: Response, model: Model<Document & Customer>) {
    model.findOne({ email })
        .then((data) => {
            if (!data) {
                return res.send(helper.successResponse(null, constants.fail.updatePicture));
            };

            model.updateOne({ email: email }, { profilePictureUrl: url })
                .then(() => {
                    return res.send(helper.successResponse(null, constants.success.updatePicture));
                });
        })
        .catch((err) => {
            return res.send(helper.failResponse(err, constants.fail.internalError));
        });
};

export function createUserAccount(firstName: string, lastName: string, email: string, password: string, phone: string, res: Response) {
    let userExists = false;
    
    //First check if the user with that email already exists
    customerModel.findOne({email})
        .then((data) => {
            if (data) {
                userExists = true;
            }
        })
        .catch((err) => {
            return res.send(helper.failResponse(err, constants.fail.internalError));
        });
    
    //If the user does not exist then try and create the user
    if (!userExists) {
        customerModel.create({firstName, lastName, email, password, phone})
            .then((data) => {
                return res.send(helper.successResponse(data, `Successfully created user account for: ${email}`));
            })
            .catch((err) => {
                return res.send(helper.failResponse(err, constants.fail.internalError));
            });
    } else {
        return res.send(helper.failResponse(null, constants.fail.userExists));
    }
}

// export function editUserAccount(id: string, firstName: string, lastName: string, email: string, password: string, phone: string, res: Response) {
//     //Find user using email and then update, returns the new document if successful
//     customerModel.findByIdAndUpdate({_id: id}, {firstName, lastName, email, password, phone}, {new: true}, )
//         .then((data) => {
//             console.log(data);
//             return res.send(helper.successResponse(data, `Successfully updated user account for: ${id}`));
//         })
//         .catch((err) => {
//             console.log(err);
//             return res.send(helper.failResponse(err, constants.fail.internalError));
//         });
// }

export function editUserAccount(id: string, firstName: string, lastName: string, email: string, phone: string, res: Response) {
    //Find user using email and then update, returns the new document if successful
    console.log(`Updating user account for: ${id}`);
    customerModel.findByIdAndUpdate({_id: id}, {firstName, lastName, email, phone}, )
        .then((data) => {
            console.log(data);
            const updatedData = {
                ...data,
                firstName,
                lastName,
                email,
                phone

            };
            return res.send(helper.successResponse(updatedData, `Successfully updated user account for: ${id}`));
        })
        .catch((err) => {
            console.log(err);
            return res.send(helper.failResponse(err, constants.fail.internalError));
        });
}
