import React, { useState } from "react";
import { Header, Title, Body, Text, View, Left, Right } from "native-base";
import { commonStyles } from "../common/commonStyles";
import { styles } from "./DineInTakeawayStyles";
import { BackButton } from '../common/BackButton';
import { TabView, TabBar } from 'react-native-tab-view';
import { Dimensions } from "react-native";
import { ScrollView } from "react-native-gesture-handler";
import { TableMatterState, setSelectedReservation } from "../../redux";
import { connect } from "react-redux";
import { Reservation } from "../../redux/types/serverTypes";
import DineInCard from "./DineInCard";
import TakeawayCard from "./TakeawayCard";
import PropTypes from "prop-types";

// Using TabView from https://github.com/satya164/react-native-tab-view

const DineInRoute = (props: Props) => {
  const upcomingBookings = props.reservations.find((reservation) => !reservation.finished && reservation.dineIn);
  const pastBookings = props.reservations.find((reservation) => reservation.finished && reservation.dineIn);
  return (
    <ScrollView style={commonStyles.flex1}>
      {upcomingBookings &&
        <View style={commonStyles.inputView}>
          <Text style={commonStyles.label}>Upcoming</Text>
        </View>
      }
      {upcomingBookings && props.reservations.filter((reservation) => !reservation.finished && reservation.dineIn).map((reservation, key) => <DineInCard key={key} reservation={reservation} />)}

      {pastBookings &&
        <View style={commonStyles.inputView}>
          <Text style={commonStyles.label}>Past</Text>
        </View>
      }
      {pastBookings && props.reservations.filter((reservation) => reservation.finished && reservation.dineIn).map((reservation, key) => <DineInCard key={key} reservation={reservation} />)}

      {!upcomingBookings && !pastBookings &&
        <View style={commonStyles.inputView}>
          <Text style={styles.noHistoryText}>You have no booking history.</Text>
        </View>
      }
    </ScrollView>
  );
};

const TakeawayRoute = (props: Props) => {
  const upcomingOrders = props.reservations.find((reservation) => !reservation.finished && reservation.orders.length > 0);
  const pastOrders = props.reservations.find((reservation) => reservation.finished && reservation.orders.length > 0);
  return (
    <ScrollView style={commonStyles.flex1}>
      {upcomingOrders &&
        <View style={commonStyles.inputView}>
          <Text style={commonStyles.label}>Upcoming</Text>
        </View>
      }
      {upcomingOrders && props.reservations.filter((reservation) => !reservation.finished && reservation.orders.length > 0).map((reservation, key) => <TakeawayCard key={key} reservation={reservation} />)}

      {pastOrders &&
        <View style={commonStyles.inputView}>
          <Text style={commonStyles.label}>Past</Text>
        </View>
      }
      {pastOrders && props.reservations.filter((reservation) => reservation.finished && reservation.orders.length > 0).map((reservation, key) => <TakeawayCard key={key} reservation={reservation} />)}

      {!upcomingOrders && !pastOrders &&
        <View style={commonStyles.inputView}>
          <Text style={styles.noHistoryText}>You have no order history.</Text>
        </View>
      }
    </ScrollView>
  );
};

const initialLayout = { width: Dimensions.get('window').width };

interface StateProps {
  reservations: Reservation[];
}

interface DispatchProps {
  setSelectedReservation: (selectedReservation: Reservation | null) => void;
  // setSelectedRestaurant: (selectedRestaurant: Restaurant | null) => void;
}

type Props = StateProps & DispatchProps;

const DineInTakeawayHistory = (props: Props) => {
  const [index, setIndex] = useState(0);
  const [routes] = React.useState([
    { key: 'dine-in', title: 'Dine In' },
    { key: 'takeaway', title: 'Takeaway' },
  ]);

  const renderScene = ({ route }) => {
    switch (route.key) {
      case 'dine-in':
        return <DineInRoute reservations={props.reservations} setSelectedReservation={setSelectedReservation} />;
      case 'takeaway':
        return <TakeawayRoute reservations={props.reservations} setSelectedReservation={setSelectedReservation} />;
      default:
        return null;
    }
  };
  renderScene.propTypes = {
    route: PropTypes.shape({
      key: PropTypes.string
    }),
    reservations: PropTypes.object
  };

  const renderTabBar = (props) => (
    <TabBar
      {...props}
      indicatorStyle={styles.tabIndicator}
      labelStyle={styles.tabLabel}
      style={styles.tab}
    />
  );

  return (
    <View style={commonStyles.container}>
      <Header style={commonStyles.header}>
        <Left style={commonStyles.flex1}>
          <BackButton />
        </Left>
        <Body style={commonStyles.body}>
          <Title style={commonStyles.title}>{index === 0 ? "Dine In" : "Takeaway"} History</Title>
        </Body>
        <Right style={commonStyles.flex1} />
      </Header>

      {/* Might want to set up lazy loading */}
      <TabView
        navigationState={{ index, routes }}
        renderScene={renderScene}
        onIndexChange={setIndex}
        initialLayout={initialLayout}
        renderTabBar={renderTabBar}
      />
    </View>
  );
};

const mapStateToProps = (state: TableMatterState): StateProps => {
  return {
    reservations: state.reservations
  };
};

const mapDispatchToProps: DispatchProps = {
  setSelectedReservation,
  // setSelectedRestaurant
};

// IDK why error appears it doesnt affect it from working (Add this on components until it is fixed)
// @ts-ignore 
export default connect<StateProps, DispatchProps>(mapStateToProps, mapDispatchToProps)(DineInTakeawayHistory);