import { Restaurant, RestaurantShort, CustomerShort } from "../../redux/types/serverTypes";
import { UserState } from "../../redux";

export const restToRestShort = (restaurant: Restaurant): RestaurantShort => {
    return {
        id: restaurant._id,
        name: restaurant.name,
        addressLine1: restaurant.addressLine1,
        addressLine2: restaurant.addressLine2,
        suburb: restaurant.suburb,
        state: restaurant.state,
        phone: restaurant.phone,
    };
};

export const userToCustShort = (customer: UserState): CustomerShort => {
    return {
        id: customer.id!,
        firstName: customer.firstName!,
        lastName: customer.lastName!,
        phone: customer.phone!,
    };
};