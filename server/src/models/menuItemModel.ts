import * as mongoose from "mongoose";
import { MenuItem } from "../types/types";

export const menuItemSchema = new mongoose.Schema({
    name: String,
    desc: String,
    price: Number,
    discount: {
        type: Number,
        default: 0,
    },
    images: [{link: String, alt: String}],
    category: {
        type: String,
    },
    tags: [{
        type: String,
    }],
    allergens: [{
        type: String,
    }],
});

export default mongoose.model<MenuItem & mongoose.Document>('MenuItem', menuItemSchema);