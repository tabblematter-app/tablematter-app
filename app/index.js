/**
 * @format
 */
import 'react-native-gesture-handler';
import {AppRegistry} from 'react-native';
import { App } from './src/App';
import {name as appName} from "./app.json";
import { setupTableMatter } from './src/redux';


// figure out how to make app registry to wait
setupTableMatter();
AppRegistry.registerComponent(appName, () => App);
console.disableYellowBox = true;
