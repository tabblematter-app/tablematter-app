import { Reservation, Restaurant } from "../types/serverTypes";
import { TableMatterActionTypes, SET_USER_RESERVATIONS_ACTION, SET_SEARCH_RESULTS_ACTION, SET_SELECTED_RESTAURANT_ACTION, SET_SELECTED_RESERVATION_ACTION, SET_REVIEWS_ACTION, ReviewData } from "../types";

export * from "./counterActions";
export * from "./userActions";
export * from "./newReservationActions";

export const setUserReservations = (reservations: Reservation[]): TableMatterActionTypes => {
  return {
    type: SET_USER_RESERVATIONS_ACTION,
    reservations,
  };
};

export const setSearchResults = (searchResults: Restaurant[]): TableMatterActionTypes => {
  return {
    type: SET_SEARCH_RESULTS_ACTION,
    searchResults,
  };
};

export const setSelectedRestaurant = (selectedRestaurant: Restaurant | null): TableMatterActionTypes => {
  return {
    type: SET_SELECTED_RESTAURANT_ACTION,
    selectedRestaurant,
  };
};

export const setSelectedReservation = (selectedReservation: Reservation | null): TableMatterActionTypes => {
  return {
    type: SET_SELECTED_RESERVATION_ACTION,
    selectedReservation,
  };
};

export const setReviews = (reviews: ReviewData[]): TableMatterActionTypes => {
  return {
    type: SET_REVIEWS_ACTION,
    reviews,
  };
};