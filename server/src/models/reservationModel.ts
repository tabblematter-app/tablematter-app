import * as mongoose from "mongoose";
import { Reservation } from "../types/types";
import { menuItemSchema } from "./menuItemModel";

export const reservationSchema = new mongoose.Schema({
    customer:{
        // Denormalise some fields from customer which need to be displayed to restaurant
        // These are unlikely to change and saves needing to Join with customer document
        id: String,
        firstName: String,
        lastName: String,
        phone: String,
    },
    restaurant:{
        // Denormalise some fields from restaurant which need to be displayed to customer
        // These are unlikely to change and saves needing to Join with restaurant document
        id: String,
        name: String,
        addressLine1: String,
        addressLine2: String,
        suburb: String,
        state: String,
        phone: String,
    },
    dineIn: Boolean,
    tableNumbers: [Number],
    orders: [menuItemSchema], // Embed as they are always retrieved with reservation (this will need to be considered when finalising implementation)
    totalPrice: Number,
    party: {
        type: {
            size: Number,
            childCount: Number,
            adultCount: Number,
            elderCount: Number,
            disabledCount: Number,
        },
        required: false,
    },
    timeBooked: Date,
    finished: {
        type: Boolean,
        default: false,
    },
    cancelled: {
        type: Boolean,
        default: false,
    },
    duration: {
        type: Number,
        default: 0,
    },
    paid: {
        type: Boolean,
        default: false,
    }
});

export default mongoose.model<Reservation & mongoose.Document>('Reservation', reservationSchema);