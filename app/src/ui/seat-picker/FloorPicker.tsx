import { Icon } from "native-base";
import React, { Dispatch, SetStateAction } from "react";
import { Text, View } from "react-native";
import { p1 } from "../utils/themes/darkTheme";
import { styles } from "./seatPickerStyles";
import { Floor } from "./SeatPickerTable";

export interface Wall {
    x1: number;
    y1: number;
    x2: number;
    y2: number;
}

type Props = {
    floors: Floor[];
    selectedFloor: number;
    setSelectedFloor: Dispatch<SetStateAction<number>>;
};

const FloorPicker = (props: Props) => {

    const show = (props.floors.length > 1);

    const lowerFloor = () => {
        if(props.selectedFloor > 0){
            props.setSelectedFloor(props.selectedFloor - 1);
        }
    };

    const increaseFloor = () => {
        if(props.selectedFloor < props.floors.length - 1){
            props.setSelectedFloor(props.selectedFloor + 1);
        }
    };

    return (
        <View style={[styles.floorPickerWrapper, show ? {} : { display: 'none', alignSelf: 'flex-end' }]}>
          <Text style={{ marginLeft: 320, color: p1 }} >Floor</Text>
          <Icon onPress={() => {increaseFloor();}} style={{ marginLeft: 320, color: p1 }} type="AntDesign" name="caretup" />
            <View style={{ width: 50, height: 50, backgroundColor: p1, marginLeft: 320 }}>
                <Text style={{ color: '#fff', alignSelf: 'center', marginTop: 12 }}>{props.selectedFloor + 1}</Text>
            </View>
          <Icon onPress={() => {lowerFloor();}} style={{ marginLeft: 320,  color: p1, marginBottom: '85%' }} type="AntDesign" name="caretdown" />
        </View>
    );
};

export default(FloorPicker);
