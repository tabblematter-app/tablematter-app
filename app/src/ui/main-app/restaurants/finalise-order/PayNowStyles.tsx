import { StyleSheet } from "react-native";
import { p1, bgColor } from "../../../utils/themes/lightTheme";

export const styles = StyleSheet.create({
  // Tab styles
  tabIndicator: {
    backgroundColor: p1
  },
  tab: {
    backgroundColor: bgColor,
  },
  tabLabel: {
    color: p1,
  }
});