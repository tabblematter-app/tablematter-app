import React from 'react';
import { View, Text, Button } from "native-base";
import { TableMatterState, setPaid, UserState, NewReservationState } from '../../../../redux';
import { connect } from 'react-redux';
import { getPaypalDetails } from '../../../utils/paypal-functions';
import { commonStyles } from '../../../common/commonStyles';
import { WebView } from 'react-native-webview';
import { styles } from "./paypal-styles";
import Axios, { AxiosRequestConfig } from 'axios';
import { ActivityIndicator } from 'react-native';
import qs from "qs";
import { defaultRestaurant } from '../RestaurantPage';
import { Reservation, Restaurant } from '../../../../redux/types/serverTypes';
import { restToRestShort, userToCustShort } from '../../../utils/type-conversions';
import API from '../../../utils/api';
import { useNavigation } from '@react-navigation/native';
import Constants from "../../../utils/constants";


const url = `https://api.sandbox.paypal.com/v1/oauth2/token`;

const data = {
  // eslint-disable-next-line @typescript-eslint/camelcase
  grant_type: 'client_credentials'

};

// DEMO CHANGE IN BEFORE RELEASE
const authConst = {
  username: Constants.payPalConfig.username,
  password: Constants.payPalConfig.password
};

interface OwnProps {
  dineIn: boolean;
}

interface StateProps {
  totalCost: number;
  restaurant: Restaurant;
  customer: UserState;
  newReservation: NewReservationState;
}

interface DispatchProps {
  setPaid: (paid: boolean) => void;
}


type Props = OwnProps & StateProps & DispatchProps;

const Paypal = (props: Props) => {
  const navigation = useNavigation();

  const [dataDetail, setDataDetail] = React.useState<object>();
  const [auth, setAuth] = React.useState(authConst);

  const [isWebViewLoading, SetIsWebViewLoading] = React.useState(false);
  const [paypalUrl, setPaypalUrl] = React.useState<string | null>('');
  const [accessToken, setAccessToken] = React.useState("");
  const [shouldShowWebViewLoading, setShouldShowWebviewLoading] = React.useState(true);


  React.useEffect(() => {
    setDataDetail(getPaypalDetails(props.totalCost, props.restaurant.name));
    setAuth(authConst);
  }, [props.totalCost]);

  const payNow = () => {
    const options: AxiosRequestConfig = {

      method: "post",
      headers: {
        'content-type': 'application/x-www-form-urlencoded',
        'Access-Control-Allow-Credentials': true
      },

      //Make sure you use the qs.stringify for data
      data: qs.stringify(data),
      auth: auth,
      url,
    };

    // Authorise with seller app information (clientId and secret key)
    Axios(options).then(response => {
      setAccessToken(response.data.access_token);


      //Resquest payal payment (It will load login page payment detail on the way)
      Axios.post(`https://api.sandbox.paypal.com/v1/payments/payment`, dataDetail,
        {
          headers: {
            'Content-Type': 'application/json',
            'Authorization': `Bearer ${response.data.access_token}`
          }
        }
      )
        .then(response => {

          // eslint-disable-next-line @typescript-eslint/no-unused-vars
          const { id, links } = response.data;
          const approvalUrl = links.find(data => data.rel == "approval_url").href;

          console.log("response", links);
          setPaypalUrl(approvalUrl);
        }).catch(err => {
          console.log(err);
        });
    }).catch(err => {
      console.log(err);
    });
  };

  const onWebviewLoadStart = () => {
    if (shouldShowWebViewLoading) {
      SetIsWebViewLoading(true);
    }
  };

  const onNavigationStateChange = (webViewState) => {
    //When the webViewState.title is empty this mean it's in process loading the first paypal page so there is no paypal's loading icon
    //We show our loading icon then. After that we don't want to show our icon we need to set setShouldShowWebviewLoading to limit it
    if (webViewState.title == "") {
      //When the webview get here Don't need our loading anymore because there is one from paypal
      setShouldShowWebviewLoading(false);
    }

    if (webViewState.url.includes('https://example.com/')) {

      setPaypalUrl(null);
      const urlArr = webViewState.url.split(/(=|&)/);

      const paymentId = urlArr[2];
      const payerId = urlArr[10];

      // eslint-disable-next-line @typescript-eslint/camelcase
      Axios.post(`https://api.sandbox.paypal.com/v1/payments/payment/${paymentId}/execute`, { payer_id: payerId },
        {
          headers: {
            'Content-Type': 'application/json',
            'Authorization': `Bearer ${accessToken}`
          }
        }
      )
        .then(() => {
          setShouldShowWebviewLoading(true);
          props.setPaid(true);
          const reservation: Reservation = {
            restaurant: restToRestShort(props.restaurant),
            customer: userToCustShort(props.customer),
            orders: props.newReservation.orders,
            dineIn: props.newReservation.dineIn,
            duration: props.newReservation.duration,
            totalPrice: props.newReservation.totalPrice,
            finished: false,
            cancelled: false,
            paid: true,
            tableNumbers: [],
            timeBooked: (new Date()).toString(),
          };

          API.post("/reservation", { reservation })
            .then((data) => {
              console.log(data.data.data._id);
              data.data ? navigation.navigate("OrderPlaced", { orderId: data.data.data._id, dineIn: props.dineIn }) : navigation.navigate("ErrorPage");
            })
            .catch(() => navigation.navigate("ErrorPage"));
        })
        .catch(err => {
          setShouldShowWebviewLoading(true);
          console.log({ ...err });
        });

    }
  };

  return (
    <>
      {paypalUrl ? (
        <View style={styles.webview}>
          <WebView
            style={{ height: "100%", width: "100%" }}
            source={{ uri: paypalUrl }}
            onNavigationStateChange={onNavigationStateChange}
            javaScriptEnabled={true}
            domStorageEnabled={true}
            startInLoadingState={false}
            onLoadStart={onWebviewLoadStart}
            onLoadEnd={() => SetIsWebViewLoading(false)}
          />
        </View>
      ) :
        <View style={commonStyles.buttonView}>
          <Button rounded activeOpacity={0.5} onPress={payNow} style={commonStyles.button}>
            <Text style={commonStyles.buttonText}>BUY NOW</Text>
          </Button>
        </View>
      }
      {isWebViewLoading ? (
        <View style={{ justifyContent: "center", alignItems: "center", backgroundColor: "#ffffff" }}>
          <ActivityIndicator size="small" color="#A02AE0" />
        </View>
      ) : null}
    </>
  );
};

const mapStateToProps = (state: TableMatterState): StateProps => {
  return {
    totalCost: state.newReservation.totalPrice,
    customer: state.user!,
    restaurant: state.selectedRestaurant || defaultRestaurant,
    newReservation: state.newReservation,
  };
};

const mapDispatchToProps: DispatchProps = {
  setPaid,
};

// IDK why error appears it doesnt affect it from working (Add this on components until it is fixed)
// @ts-ignore 
export default connect<StateProps, DispatchProps, OwnProps>(mapStateToProps, mapDispatchToProps)(Paypal);

