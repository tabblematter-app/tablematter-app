import React from "react";
import { View, Text, Image } from "react-native";
import { Card, CardItem } from 'native-base';
import { styles } from "../../profile-page/OrderHistoryStyles";
import { useNavigation } from "@react-navigation/native";
import { styles as orderAgainStyles } from "./restaurantBlobStyles";
import { Restaurant } from "../../../redux/types/serverTypes";
import OrderAgainReviewStars from "../../main-app/home-screens/OrderAgain/OrderAgainReviewStars";

interface OwnProps {
  restaurant: Restaurant;
  onPress?: () => void;
  testID?: string;
  rating?: number;
}

type Props = OwnProps;

const RestaurantBlob = (props: Props) => {
  //Self created navigation prop to avoid having to pass from parent
  const navigation = useNavigation();

  const handlePress = () => {
    props.onPress && props.onPress();
    navigation.navigate("RestaurantPage");

  };

  return (
    <Card style={styles.orderHistoryBlob} testID={props.testID} onTouchEnd={handlePress}>
      <CardItem cardBody style={styles.cardBody}>
        <View style={styles.inner}>
          <Text style={styles.orderHistoryTextBold}>{props.restaurant.name}</Text>
          <Text style={[styles.orderHistoryTextBold, orderAgainStyles.orderAgainText, orderAgainStyles.noBold]}>{props.restaurant.addressLine1}</Text>
          {/* <Text style={[styles.orderHistoryTextBold, orderAgainStyles.orderAgainText, orderAgainStyles.noBold]}>$$$</Text> */}
          {props.rating &&
            OrderAgainReviewStars(props.rating)
          }
          {/* Display review stars here */}
        </View>
        <View style={styles.imageContainer}>

          <Image
            source={require('../../../../Assets/PlaceholderGraphic.jpg')}
            style={styles.image}
          />
        </View>
      </CardItem>
    </Card>

  );

  // return (

  //   <View style={[styles.orderHistoryBlob, orderAgainStyles.orderAgainBlob]}>
  //     <TouchableOpacity testID={props.testID} style={styles.allScreen} onPress={() => {
  //       props.onPress && props.onPress();
  //       navigation.navigate("RestaurantPage");
  //     }}>
  //       <Text style={styles.orderHistoryTextBold}>{props.restaurant.name}</Text>
  //       <Text style={[styles.orderHistoryTextBold, orderAgainStyles.orderAgainText]}>{props.restaurant.addressLine1}</Text>
  //       <Text style={[styles.orderHistoryTextBold, orderAgainStyles.orderAgainText, orderAgainStyles.noBold]}>$$$</Text>
  //     </TouchableOpacity>
  //   </View>

  // );
};

export default RestaurantBlob;