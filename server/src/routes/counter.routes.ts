import * as express from "express";
import { Response } from "express";
import CounterModel from "../models/counterModel";
import { findAll } from "../controllers/model.functions";

export default (app: express.Express) => {
    app.get("/counter", (_, res: Response) => findAll(res, CounterModel));
};
