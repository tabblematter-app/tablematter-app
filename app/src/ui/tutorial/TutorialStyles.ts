import { StyleSheet } from "react-native";
import { bgColor, p1 } from "../utils/themes/lightTheme";

export const styles = StyleSheet.create({
  slideContainer: {
    flex: 1,
    backgroundColor: "#D93636"
  },
  slide: {
    flex: 1,
    alignItems: "center",
    justifyContent: "center",
    marginBottom: 50,
    backgroundColor: p1,
  },
  title: {
    color: bgColor,
    fontSize: 20,
  },
  text: {
    color: bgColor,
    marginHorizontal: 30,
    textAlign: "center",
    fontSize: 15,
  },
  dot: {
    backgroundColor: 'rgba(217, 54, 54, 0.2)'
  },
  activeDot: {
    backgroundColor: '#D93636'
  },
  actionText: {
    color: p1,
    marginBottom: 0,
    paddingBottom: 0,
  },
  imageContainer: {
    marginVertical: 30,
    width: 'auto',
    height: '60%',
    alignItems: "center",
  },
  image: {
    flex: 1,
    aspectRatio: 0.49,
    resizeMode: 'contain',
  }
});