import React from 'react';
import { Col, Row, Grid } from "react-native-easy-grid";
import { ImageBackground } from "react-native";
import { Body, Text, View, Card, CardItem, Button, Icon } from "native-base";
import { commonStyles } from "../common/commonStyles";
import { styles } from "./DineInTakeawayStyles";
import { Reservation } from "../../redux/types/serverTypes";
import moment from "moment";
import { useNavigation } from '@react-navigation/native';
import { connect } from 'react-redux';
import { setSelectedReservation } from "../../redux";
import { displayEndTime } from '../utils/helpers';

interface OwnProps {
  reservation: Reservation | null;
}

interface DispatchProps {
  setSelectedReservation: (selectedReservation: Reservation | null) => void;
  // setSelectedRestaurant: (selectedRestaurant: Restaurant | null) => void;
}

type Props = OwnProps & DispatchProps;

const DineInCard = (props: Props) => {
  const navigation = useNavigation();
  const viewReservation = () => {
    props.setSelectedReservation(props.reservation);
    // props.setSelectedRestaurant(null);
    // props.setSelectedRestaurant(props.reservation?.restaurant); // Cannot use RestaurantShort
    navigation.navigate('ReservationSummaryPage');

    // Use effect is invalid hook?
    // useEffect(() => {
    // API.get(`/restaurantbyid?restaurantId=${props.reservation?.restaurant.id}`)
    //   .then((result) => {
    //     console.log("Found the restaurant");
    //     props.setSelectedReservation(props.reservation);
    //     props.setSelectedRestaurant(result.data.data);
    //     navigation.navigate('DineInPage');
    //   })
    //   .catch((err) => {
    //     console.log(err.message);
    //   });
    // }, []);
  };



  if (props.reservation?.party) {
    return (
      <Card style={styles.container}>
        <CardItem cardBody>
          <View style={styles.cardImageContainer}>
            <ImageBackground
              source={require('../../../Assets/PlaceholderGraphic.jpg')}
              style={styles.cardImage}
            >
              <View style={styles.cardImageInner}>
                <Text style={{ color: '#FFF' }}>{props.reservation.restaurant.name}</Text>
                <Text style={{ color: '#FFF' }}>{props.reservation.restaurant.addressLine1}</Text>
              </View>
            </ImageBackground>
          </View>
        </CardItem>
        <CardItem>
          <Body>
            <Grid>
              <Row style={commonStyles.alignVertical}>
                <Icon name="calendar" style={styles.icon}></Icon>
                <Text>{moment(props.reservation.timeBooked).format('MMMM Do YYYY, h:mm a - ')}{displayEndTime(props.reservation.timeBooked, props.reservation.duration)}</Text>
              </Row>
            </Grid>
            <Grid>
              <Col style={styles.col}>
                <Icon name="people" style={styles.icon}></Icon>
                <Text>{props.reservation.party.size}</Text>
              </Col>
              <Col style={styles.col}>
                <Icon name="bookmark-multiple-outline" type="MaterialCommunityIcons" style={styles.icon}></Icon>
                <Text>{props.reservation.tableNumbers[0]}</Text>
              </Col>
            </Grid>
          </Body>
        </CardItem>
        {/* If order details */}
        {/* {props.reservation.orders &&
          <CardItem style={styles.borderTop}>
            <Body>
              <Text>Order (ID here) pre-booked</Text>
            </Body>
          </CardItem>
        } */}
        <CardItem cardBody style={[commonStyles.flex1, styles.borderTop]}>
          {/* Receipt if paid for */}
          {props.reservation.paid &&
            <Button transparent style={commonStyles.cardButton}>
              <Text style={commonStyles.cardButtonText}>RECEIPT</Text>
            </Button>
          }
          {/* If upcoming */}
          {!props.reservation.finished &&
            <Button transparent style={commonStyles.cardButton} onPress={viewReservation}>
              <Text style={commonStyles.cardButtonText}>VIEW</Text>
            </Button>
          }
        </CardItem>
      </Card>
    );
  } else {
    return (
      null
    );
  }
};

// const mapStateToProps = (state: TableMatterState): StateProps => {
//   return {
//     reservation: state.selectedReservation
//   };
// };

const mapDispatchToProps: DispatchProps = {
  setSelectedReservation,
  // setSelectedRestaurant
};

// IDK why error appears it doesnt affect it from working (Add this on components until it is fixed)
// @ts-ignore 
export default connect<{}, DispatchProps>(undefined, mapDispatchToProps)(DineInCard);