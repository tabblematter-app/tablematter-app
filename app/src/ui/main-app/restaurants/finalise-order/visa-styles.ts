import { StyleSheet } from "react-native";

export const styles = StyleSheet.create({
    input: {
        marginBottom: 30,
        marginTop: 10,
    },
    cardHolder: {
        marginBottom: 30,
    }
});