import * as mongoose from "mongoose";
import { Customer } from "../types/types";

export const customerSchema = new mongoose.Schema({
	profilePictureUrl: String,
	firstName: String,
	lastName: String,
	password: String,
	email: {
        unique: true,
        type: String,
    },
	phone: String,
	// Could possibly store reservations here
});

export default mongoose.model<Customer & mongoose.Document>('customer', customerSchema, "customer");
