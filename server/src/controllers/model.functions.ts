import { Document, Model } from "mongoose";
import { Response } from "express";

interface NamedModel {
    name: string;
}

// For when only 1 exists or you want all
export function findAll(res: Response, model: Model<Document>) {
    model.find().sort({ _id: 1 })
        .then((data) => res.send(data))
        .catch((err) => {
            res.statusMessage = err.message || "Some error occurred while retrieving missions.";
            res.end;
        }
        );
}

export function findOne(id: string, res: Response, model: Model<Document & NamedModel>) {
    model.findById(id)
        .then((data) => {
            if (!data) {
                res.statusMessage = "Model not found with id " + id;
                res.status(404).end();
            }
            res.send(data);
        })
        .catch((err) => {
            if (err.kind === "ObjectId") {
                res.statusMessage = "Model not found with id " + id;
                res.status(404).end();
            }
            res.statusMessage = "Error retrieving model with id " + id;
            res.end();
        });
}
