import { StyleSheet, Platform } from "react-native";
import { p1 } from "../utils/themes/lightTheme";

export const styles = StyleSheet.create({
  picker: {
    borderWidth: 1,
    borderColor: p1,
    borderRadius: 30,
    paddingLeft: 10, // not sure why this has to be different
    paddingRight: 10,
    fontSize: 14,
    width: Platform.OS === "ios" ? "95%" : "100%",
  },
});