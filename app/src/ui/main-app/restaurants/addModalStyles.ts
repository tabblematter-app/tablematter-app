import { StyleSheet } from "react-native";
import { p1 } from "../../utils/themes/lightTheme";

export const modalStyles = StyleSheet.create({
  view: {
    width: "100%",
    padding: 0,
    alignContent: "center",
    justifyContent: "center",
    flexDirection: "row",
    paddingVertical: 15,
  },
  cancelButton: {
    marginRight: 5,
  },
  submitButton: {
    width: "50%",
    color: "white",
    backgroundColor: p1,
  },
  submitButtonText: {
    padding: 0,
    width: "100%",
    fontSize: 24,
    textAlign: "center",
  },
  title: {
    fontSize: 18, // 28
    textAlign: "center",
    paddingVertical: 15,
    marginBottom: 10,
  },
  cardHeader: {
    backgroundColor: p1,
  },
  cardHeaderText: {
    color: "#FFF",
    fontWeight: "bold",
  },
  row: {
    flexDirection: 'row',
    width: "100%",
  },
});