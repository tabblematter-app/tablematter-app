import React from "react";
import { Icon, View } from "native-base";
import { styles } from "../../../profile-page/profilePageStyles";
import { styles as orderAgainStyles } from "./orderAgainStyle";


//const rating = 4;

interface StarProps {
  selected: boolean;
  half: boolean;
}

const OrderAgainReviewStars = (rating: number) => {
    {/*loop through the stars value to correctly present the users rating*/}
    const stars: Element[] = [];

  for (let x = 1; x <= 5; x++) {
    stars.push(
      <View key={stars.length}>
        <Star half={x - rating === 0.5 ? true : false} selected={x <= rating ? true : false} />
      </View>
    );
  }

  return (
    <View style={[orderAgainStyles.reviewStarWrapper]}>
      {stars}
    </View>
  );
};

{/*Star icon definition*/ }
const Star = (props: StarProps) => {
  return (
    <Icon name={props.half === true ? "star-half" : "star"} style={[styles.userInfoIcon, orderAgainStyles.starStyle, selectedState(props.selected || props.half)]} />
  );
};

{/*Conditional styling dictating the colour of the stars*/ }
const selectedState = (isSelected: boolean) => {
  if (!isSelected) {
    return styles.ratingStarNotSelected;
  } else {
    return '';
  }
};

export default OrderAgainReviewStars;
