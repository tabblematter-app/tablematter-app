import axios from 'axios';
import { host } from '../../config/axiosSetup';
import { showError } from "../utils/alert";

const API = axios.create({
    baseURL: `http://${host}:3000`,
    timeout: 5000
});

//TODO: Implement predefined error messages (here and in back end)

API.interceptors.request.use(
    //Intercept before sending request to server
    async (config) => {
        //Future implementation of user login tokens
        return config;
    },
    (error) => {
        showError("Error", error.message);
        return Promise.reject(error);
    }
);

API.interceptors.response.use(
    //Intercept once receiving response from server
    //
    //There are two types of errors supported:
    //Program Errors and Unexpected Errors
    //
    (response) => {
        //Program errors will go here
        //Specified by response.data.status === false
        //Methods receiving the response must always check response.data.status
        return response;
    },
    (error) => {
        //Unexpected errors including errors thrown by the API will go here
        //This should never happen in a bug-free app (except for server timeouts)

        console.log("ERROR RESPONSE: " + JSON.stringify(error.response));
        console.log("ERROR MESSAGE: " + error.message);

        //Process error response here and create message

        if (error.code === "ECONNABORTED") {
            error.message = "Server connection timeout";
        }
        else if (error.status === 404) {
            error.message = "The server could not find the specified route";
        }

        showError("Error", error.message);
        return Promise.reject(error);
    }
);

export default API;
