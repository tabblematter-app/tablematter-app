import {Store} from 'redux';
import API from '../../ui/utils/api';
import {setCounter} from '../actions';

export const setupCounter = (store: Store) => {
  API.get(`/counter/`)
    .then((data) => {
      store.dispatch(setCounter(data?.data?.[0]?.value || 0)); //fixed undefined error. If not defined, defines as 0
    })
    .catch();
};
