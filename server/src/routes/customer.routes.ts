import * as express from "express";
import { Response } from "express";
import { findAll } from "../controllers/model.functions";
import customerModel from "../models/customerModel";
import { logIn, setProfilePicture, createUserAccount, editUserAccount } from "../controllers/user.controller";

export default (app: express.Express) => {
    app.get("/user", (_, res: Response) => findAll(res, customerModel));
    app.get("/login/:email/:password", (req, res: Response) => logIn(req.params.email, req.params.password, res, customerModel));

    app.put("/user/picture/:email/:url", (req, res: Response) => setProfilePicture(req.params.email, req.params.url, res, customerModel));

    app.post("/createUserAccount", (req, res: Response) => createUserAccount(req.body.firstName, req.body.lastName, req.body.email, req.body.password, req.body.phone, res));

    // app.post("/editUserAccount", (req, res: Response) => editUserAccount((req.body.id as string) || "", req.body.firstName, req.body.lastName, req.body.email, req.body.password, req.body.phone, res));

    app.post("/editUserAccount", (req, res: Response) => editUserAccount((req.body.id as string) || "", req.body.firstName, req.body.lastName, req.body.email, req.body.phone, res));
};
