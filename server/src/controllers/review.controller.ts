import { Response } from "express";
import reviewModel from "../models/reviewModel";
import helper from "../utils/responseHelper";
import constants from "../utils/constants";
import restaurantModel from "../models/restaurantModel";
import { Review } from "../types/types";

const updateRestaurantRating = (restaurantId: string) => {
    reviewModel.find({ 'restaurantId': restaurantId })
        .then((reviews: Review[]) => {
            if (reviews && reviews.length > 0) {
                let totalRating = 0;
                reviews.map((review) => {
                    totalRating += review.rating;
                });

                const averageRating = totalRating/reviews.length;
                console.log(averageRating);

                if (averageRating) {
                    restaurantModel.findByIdAndUpdate({ _id: restaurantId }, { averageRating: averageRating })
                    .then(() => {
                        return true;
                    })
                    .catch(() => {
                        return false;
                    });
                } else {
                    return false;
                }
            }
        })
        .catch(() => {
            return false;
        });
};

export function addReview(reservationId: string | null, customerId: string | null, restaurantId: string, reviewerName: string, rating: number, content: string, res: Response) {
    const modified = false;
    reviewModel.create({reservationId, restaurantId, customerId, reviewerName, rating, content, modified})
    .then((review) => {
        //Update the average rating of the restaurant
        updateRestaurantRating(review.restaurantId);
        return res.send(helper.successResponse(review, constants.success.addReview));
    }).catch(() => {
        return res.send(helper.failResponse(null, constants.fail.addReview));
    });
};

export function updateReview(reviewId: string, reservationId: string | null, customerId: string | null, restaurantId: string, reviewerName: string, rating: number, content: string, res: Response) {
    const modified = true;
    reviewModel.findByIdAndUpdate({ _id:reviewId }, {reservationId, customerId, restaurantId, reviewerName, rating, content, modified})
        .then((review) => {
            if (review) {
                //Update the average rating of the restaurant
                updateRestaurantRating(review.restaurantId);
            }
            return res.send(helper.successResponse(null, constants.success.updateReview));
        })
        .catch(() => {
            return res.send(helper.failResponse(null, constants.fail.updateReview));
        });
};

export function deleteReview(reviewId: string, res: Response) {
    reviewModel.findByIdAndDelete({_id: reviewId})
        .then((review) => {
            console.log(review);
            if (review) {
                //Update the average rating of the restaurant
                updateRestaurantRating(review.restaurantId);
            }
            return res.send(helper.successResponse(null, constants.success.deleteReview));
        })
        .catch(() => {
            return res.send(helper.failResponse(null, constants.fail.deleteReview));
        });
};

export const getReviewById = (reviewId: string, res: Response) => {
    reviewModel.findById({ _id: reviewId })
        .then((review) => {
            return res.send(helper.successResponse(review, constants.success.getReviewById));
        })
        .catch(() => {
            return res.send(helper.failResponse(null, constants.fail.getReviewById));
        });
};

export const getUserReviews = (userId: string, restaurantId: string | null, res: Response)=> {
    if (restaurantId) {
        reviewModel.findOne({ customerId: userId, restaurantId})
        .then((review) => {
            return res.send(helper.successResponse(review, constants.success.getUserReviews));
        })
        .catch(() => {
            return res.send(helper.failResponse(null, constants.fail.getUserReviews));

        });
    } else {
        console.log('User Id: ' + userId);
        reviewModel.find({ customerId: userId})
        .then((reviews) => {
            return res.send(helper.successResponse(reviews, constants.success.getUserReviews));
        })
        .catch(() => {
            return res.send(helper.failResponse(null, constants.fail.getUserReviews));

        });
    }
    
};

export const getReviewsForRestaurant = (restaurantId: string, res: Response) => {
    if (restaurantId && restaurantId.length > 0) {
        reviewModel.find({ 'restaurantId': restaurantId})
            .then((reviews) => {
                return res.send(helper.successResponse(reviews, constants.success.getReviewsForRestaurant));
            })
            .catch(() => {
                return res.send(helper.failResponse(null, constants.fail.getReviewsForRestaurant));

            });
    } else {
        return res.send(helper.failResponse(null, constants.fail.badRequestError));
    }
};