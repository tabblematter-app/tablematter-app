import React from "react";
import { View, Text, Image } from "react-native";
import { Card, CardItem } from 'native-base';
import { styles } from "../../../profile-page/OrderHistoryStyles";
import { styles as orderAgainStyles } from "./orderAgainStyle";
import { useNavigation } from "@react-navigation/native";
import OrderAgainReviewStars from "./OrderAgainReviewStars";
import { setSelectedRestaurant } from '../../../../redux';
import { Restaurant } from '../../../../redux/types/serverTypes';
import { connect } from "react-redux";

interface OwnProps {
  restaurant: Restaurant;
}

interface Props {
  id?: string;
  name: string;
  onPress?: () => void;
  address: string;
  rating?: number;
}

interface DispatchProps {
  setSelectedRestaurant: (selectedRestaurant: Restaurant | null) => void;
}


const OrderAgainBlob = (props: Props) => {
  //Self created navigation prop to avoid having to pass from parent
  const navigation = useNavigation();
  const handlePress = () => {
    props.onPress && props.onPress();
    navigation.navigate("RestaurantPage");
  };

  return (
    <Card style={styles.orderHistoryBlob} testID={props.id} onTouchEnd={handlePress}>
      <CardItem cardBody style={styles.cardBody}>
        <View style={styles.inner}>
          <Text style={styles.orderHistoryTextBold}>{props.name}</Text>
          <Text style={[styles.orderHistoryTextBold, orderAgainStyles.orderAgainText, orderAgainStyles.noBold]}>{props.address}</Text>
          {/* <Text style={[styles.orderHistoryTextBold, orderAgainStyles.orderAgainText, orderAgainStyles.noBold]}>$$$</Text> */}
          {props.rating && (
            OrderAgainReviewStars(props.rating)
          )}
        </View>
        <View style={styles.imageContainer}>

          <Image
            source={require('../../../../../Assets/PlaceholderGraphic.jpg')}
            style={styles.image}
          />
        </View>
      </CardItem>
    </Card>
  );
};

const mapDispatchToProps: DispatchProps = {
  setSelectedRestaurant,
};

// IDK why error appears it doesnt affect it from working (Add this on components until it is fixed)
// @ts-ignore 
export default connect<{}, DispatchProps>(undefined, mapDispatchToProps)(OrderAgainBlob);