import React, { useState } from "react";
// import { connect } from "react-redux";
import { View, TouchableOpacity } from "react-native";
// import { TableMatterState } from "../../redux";
import { commonStyles } from "../common/commonStyles";
import { Header, Left, Text, Body, Right } from "native-base";
import { BackButton } from "../common/BackButton";
import { styles } from "./seatPickerStyles";
import SeatPickerWindow from "./SeatPickerWindow";
import { useNavigation } from '@react-navigation/native';
import { TableMatterState, setTableNumbers, setSelectedRestaurant } from '../../redux';
import { connect } from 'react-redux';
import { defaultRestaurant } from '../main-app/restaurants/RestaurantPage';
import { Floor, TableType } from "./SeatPickerTable";
import FloorPicker from "./FloorPicker";
import { Restaurant } from "../../redux/types/serverTypes";
//import { floor } from "react-native-reanimated";
//import {StackNavigationProp} from '@react-navigation/stack';
//import {DrawerParamList} from '../utils/drawer';

interface StateProps {
  tableNumbers: number[];
  restaurant: Restaurant;
}

// interface OwnProps {

// }

interface DispatchProps {
	setTableNumbers: (tableNumbers: number[]) => void;
  setSelectedRestaurant: (selectedRestaurant: Restaurant | null) => void;
}

type Props = DispatchProps & StateProps;

const SeatPickerView = () => {
  const navigation = useNavigation();

  const [selectForMe, setSelectForMe] = useState(false);
  const [selectedFloor, setSelectedFloor] = useState(0);

  const handleSubmit = () => {
    navigation.navigate('BookingDetailsPage', {hasMenu: false});
  };

  const validateFloorsData = (floors: Floor[]): Floor[] => {
    const validatedFloors: Floor[] = floors;

    //Iterate backwards through the floors so we can remove any bad ones
    for (let f = floors.length - 1; f >= 0; f--) {
      //Validate base data for the floor
      if (validString(floors[f].floorId) && validString(floors[f].layoutId)
        && validNumber(floors[f].length) && validNumber(floors[f].width)) {
          
          //This means the floor can be rendered
          //Now check the walls within the floor, remove any bad ones (loop backwards)
          for (let w = floors[f].walls.length - 1; w >= 0; w--) {
            if (!(validNumber(floors[f].walls[w].x1) && validNumber(floors[f].walls[w].x2)
              && validNumber(floors[f].walls[w].y1) && validNumber(floors[f].walls[w].y2))) {
                //Remove this wall
                floors[f].walls.splice(w, 1);
            }
          }

          //Now check the tables, works same as walls, iterate backwards and remove bad ones
          for (let t = floors[f].tables.length - 1; t >= 0; t--) {
            if (!(validNumber(floors[f].tables[t].capacity) && validNumber(floors[f].tables[t].length)
              && validNumber(floors[f].tables[t].number) && validNumber(floors[f].tables[t].width)
              && validNumber(floors[f].tables[t].x) && validNumber(floors[f].tables[t].y))) {
                //Remove this table
                floors[f].tables.splice(t, 1);
            }
          }

          //Now check the stairs (if applicable), works same as walls and tables, iterate backwards and remove bad ones
          if (floors[f].stairs.length > 0) {
            for (let s = floors[f].stairs.length - 1; s >= 0; s--) {
              if (!(validString(floors[f].stairs[s].direction) && validNumber(floors[f].stairs[s].length)
                && validNumber(floors[f].stairs[s].width) && validNumber(floors[f].stairs[s].x) && validNumber(floors[f].stairs[s].y))) {
                  //Remove this staircase
                  floors[f].stairs.splice(s, 1);
              }
            }
          }

          //Check the length of the walls and table arrays, if any of them are 0, then this floor is bad and should be removed
          //(Stairs can be empty)
          if (floors[f].tables.length === 0 && floors[f].walls.length === 0) {
            floors.splice(f, 1);
          }
      }
    }

    return validatedFloors;
  };

  const validString = (string?: string): boolean => {
    if (string && string.trim().length > 0) {
      return true;
    }

    return false;
  };

  //Checks if number is not null and greater than 0
  const validNumber = (number?: number): boolean => {
    if (number !== null && number !== undefined && number >= 0) {
      return true;
    }

    return false;
  };

  const startingFloors: Floor[] = [
    {
        floorId: '32f0ocn203jfwejag32rfak0tap',
        layoutId: '0293fjff320jfsvdonvapsfjoa',
        width: 7,
        length: 6,
        walls: [
            {
                x1: 0,
                y1: 0,
                x2: 4,
                y2: 0
            },
            {
                x1: 0,
                y1: 0,
                x2: 0,
                y2: 6
            },
            {
                x1: 4,
                y1: 0,
                x2: 4,
                y2: 3,
            },
            {
              x1: 0,
              y1: 3,
              x2: 4,
              y2: 3,
          },
          {
            x1: 4,
            y1: 3,
            x2: 7,
            y2: 3,
          },
          {
            x1: 7,
            y1: 3,
            x2: 7,
            y2: 6
        },
        {
            x1: 0,
            y1: 6,
            x2: 7,
            y2: 6
        }
        ],
        tables: [
              {
                number: 1,
                shape: TableType.round,
                capacity: 3,
                x: 1,
                y: 0,
                width: 1,
                length: 1,
                available: true
              },
              {
                number: 2,
                shape: TableType.round,
                capacity: 3,
                x: 0,
                y: 1,
                width: 1,
                length: 1,
                available: true
              },
              {
                number: 3,
                shape: TableType.round,
                capacity: 3,
                x: 2,
                y: 1,
                width: 1,
                length: 1,
                available: true
              },
              {
                number: 4,
                shape: TableType.rectangle,
                capacity: 6,
                x: 4,
                y: 4,
                width: 1,
                length: 2,
                available: true
              },
              {
                number: 5,
                shape: TableType.rectangle,
                capacity: 6,
                x: 6,
                y: 4,
                width: 1,
                length: 2,
                available: true
            }
        ],
        stairs: [
            {
                direction: "up",
                x: 1,
                y: 4,
                width: 1,
                length: 1
              }  
        ]
    },
    {
      floorId: '32f0ocn203jfwejag32rfak0tap',
      layoutId: '0293fjff320jfsvdonvapsfjoa',
      width: 7,
      length: 6,
      walls: [
          {
              x1: 0,
              y1: 0,
              x2: 6,
              y2: 0
          },
          {
              x1: 0,
              y1: 0,
              x2: 0,
              y2: 6
          },
      ],
      tables: [
            {
              number: 6,
              shape: TableType.square,
              capacity: 3,
              x: 1,
              y: 0,
              width: 1,
              length: 1,
              available: true
            },
            {
              number: 7,
              shape: TableType.square,
              capacity: 3,
              x: 4,
              y: 0,
              width: 1,
              length: 1,
              available: true
            },
            {
              number: 8,
              shape: TableType.square,
              capacity: 3,
              x: 3,
              y: 2,
              width: 1,
              length: 1,
              available: true
            },
            {
              number: 9,
              shape: TableType.round,
              capacity: 6,
              x: 6,
              y: 4,
              width: 1,
              length: 2,
              available: true
            },
            {
              number: 10,
              shape: TableType.rectangle,
              capacity: 6,
              x: 7,
              y: 4,
              width: 1,
              length: 2,
              available: true
          }
      ],
      stairs: [
          {
              direction: "up",
              x: 1,
              y: 4,
              width: 1,
              length: 1
            }  
      ]
    },
  ];

  const floors = validateFloorsData(startingFloors);
  // 0 values causing bugs with rendering
  floors.forEach((item: Floor) => {
    item.tables.forEach((table) => {
      table.y += 1;
      table.x += 1;
    });
  });

  return (

    <View style={commonStyles.flex1}>
      <View style={{ zIndex: 10 }}>
        {/*Header section of the page*/}
        <Header style={commonStyles.header}>
          <Left style={commonStyles.flex1}>
            <BackButton />
          </Left>
          <Body style={commonStyles.body}>
            <Text style={commonStyles.title}>Choose a Table</Text>
          </Body>
          <Right style={commonStyles.flex1} />
        </Header>
      </View>
      <View style={{ zIndex: 1 }}>
        <View>
          {/* Here we create a view made up of shapes using x / y coordinates to re-create the design specified by the admin application */}
          <SeatPickerWindow floor={selectedFloor} selectForMe={selectForMe} floors={floors} />
        </View>
        <View style={[styles.buttonWrapper]}>
          <FloorPicker selectedFloor={selectedFloor} setSelectedFloor={setSelectedFloor} floors={floors} />
        </View>
        <View style={styles.buttonWrapper}>
          <TouchableOpacity style={styles.button}
		  	onPress={() => setSelectForMe(true)}>
            <Text style={styles.buttonText}>SELECT FOR ME</Text>
          </TouchableOpacity>
          <TouchableOpacity style={styles.button}
            onPress={() => handleSubmit()}>
            <Text style={styles.buttonText}>CONFIRM</Text>
          </TouchableOpacity>
        </View>
      </View>
    </View>
  );
};

interface DispatchProps {
  setTableNumbers: (tableNumbers: number[]) => void;
}

const mapDispatchToProps: DispatchProps = {
  setTableNumbers,
  setSelectedRestaurant
};

const mapStateToProps = (state: TableMatterState): StateProps => 
{
  return{
    restaurant: state.selectedRestaurant || defaultRestaurant,
    tableNumbers: state.newReservation.tableNumbers
  };
};

// @ts-ignore 
// export default connect<StateProps, DispatchProps>(mapStateToProps)(SeatPickerView);
export default connect<StateProps, DispatchProps>(mapStateToProps, mapDispatchToProps)(SeatPickerView);
