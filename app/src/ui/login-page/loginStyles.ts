import { StyleSheet } from "react-native";

export const styles = StyleSheet.create({
  password: {
    marginBottom: 300,
    letterSpacing: 0
  },
  hidden: {
    display: "none",
  },
  alert: {
    marginBottom: 15,
  }
});