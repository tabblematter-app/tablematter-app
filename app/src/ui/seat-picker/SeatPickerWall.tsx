import React from "react";
import { View } from "react-native";
import { styles } from "./seatPickerStyles";

export interface Wall {
    x1: number;
    y1: number;
    x2: number;
    y2: number;
}

type Props = {
    dimensions: Wall;
    showHorizontal: boolean;
    showVertical: boolean;
    walls: Wall[];
};

const SeatPickerWall = (props: Props) => {

    let width = 2;
    let height = 2;

    const isHorizontal = (props.dimensions.x1 !== props.dimensions.x2) || props.showHorizontal;
    //const previousWall = props.walls[props.walls.indexOf(props.dimensions)];



    if(isHorizontal)
    {
        width = (props.dimensions.x2 - props.dimensions.x1) * 92;
    } 
    else 
    {
        height = (props.dimensions.y2 - props.dimensions.y1) * 90;
    }

    const isBlankSpace = (props.dimensions.x1 == 0 && props.dimensions.x2 == 0 && props.dimensions.y1 == 0 && props.dimensions.y2 == 0);
    
    if(isBlankSpace) {
        // Don't make blank spaces push past the end of previous line
        if(props.showHorizontal)
        {
            width = 69;
        } 
        else {
            width = 69;
        }

        if(props.showVertical)
        {
            height = 90;
        }
        else {
            height = 90;
        }
    }

    return (
        <View>
            { isHorizontal ? <View style={[isBlankSpace ? styles.blankSpace : styles.wallHorizontal, { width: width, height: height }, props.showHorizontal ? {} : { display: 'none' } ]} /> 
            :
            <View style={[isBlankSpace ? styles.blankSpace : styles.wallVertical, { width: width, height: height }, props.showVertical ? {} : { display: 'none' } ]} /> }
        </View>
    );
};

export default(SeatPickerWall);
