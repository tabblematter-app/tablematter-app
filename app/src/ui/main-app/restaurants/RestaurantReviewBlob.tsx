import React from "react";
import { View, Text } from "react-native";
import { Card, CardItem } from 'native-base';
import { styles } from "./restaurantsStyles";
import OrderAgainReviewStars from "../../main-app/home-screens/OrderAgain/OrderAgainReviewStars";
import Icon from "react-native-vector-icons/MaterialCommunityIcons";

interface OwnProps {
  key: string;
  reviewerName?: string;
  rating: number;
  content: string;
  modified: boolean;
}

type Props = OwnProps;

const RestaurantReviewBlob = (props: Props) => {

  return (
    <Card style={styles.restaurantReviewBlob} testID={props.id}>
      <CardItem cardBody>
        <View style={styles.inner}>
          {(props.reviewerName && props.reviewerName.length > 0) ? (
            <View>
              <Text style={styles.restaurantReviewTextBold}>{props.reviewerName}</Text>
              {props.modified && (
                <Text style={styles.restaurantReviewSubtext}><Icon name={"pencil"} color="#01a699" /> Edited</Text>
              )}
            </View>
          ) : (
              <View>
                <Text style={styles.restaurantReviewTextBold}>Anonymous</Text>
                {props.modified && (
                  <Text style={styles.restaurantReviewSubtext}><Icon name={"pencil"} color="#01a699" /> Edited</Text>
                )}
              </View>
            )}
          <Text style={styles.restaurantReviewContent}>{props.content}</Text>
          {/* Display review stars here */}
          {props.rating &&
            OrderAgainReviewStars(props.rating)
          }
        </View>
      </CardItem>
    </Card>
  );
};

export default RestaurantReviewBlob;