import {createStackNavigator} from '@react-navigation/stack';

export type RootStackParamList = {
  Landing: undefined;
  Main: undefined;
  SignUpPage: undefined;
  Login: undefined;
  SignUpSuccess: undefined;
  Tutorial: undefined;
  ErrorPage: undefined;
  OrderPlaced: {
    orderId: string;
    dineIn: boolean;
  };
  BookingDetailsPage: {
    hasMenu: boolean;
  };
  BookingConfirmationPage: {
    hasMenu: boolean;
  };
  PayNowPage: {
    dineIn: boolean;
  };
  Review: {
    reviewId: string;
    reservationId: string;
    customerId: string;
    restaurantId: string;
    reviewerName: string;
    reviewStars: number;
  };
  SeatPicker: undefined;
  OrderSummary: undefined;
  ReservationSummaryPage: undefined;
  DineInPage: undefined;
  UserReview: undefined;
  Menu: {
    dineIn: boolean;
    reservation: boolean;
    navigateToConfirmation: boolean;
  };
  RestaurantPage: undefined;
  EditProfile: undefined;
  SearchResults: undefined;

  SettingsPage: undefined;
  SettingsSubMenu: undefined;
};

export const RootStack = createStackNavigator<RootStackParamList>();
