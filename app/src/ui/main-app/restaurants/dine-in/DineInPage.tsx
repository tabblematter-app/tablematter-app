import React, { useState } from 'react';
import {
  Text,
  TouchableOpacity,
  View,
  Image,
  ScrollView,
  Platform,
  DatePickerIOS,
  TextInput,
} from 'react-native';
//Removed DatePickerIOS and Modal from react-native to resolve test errors
import { styles } from './DineInStyles';
import { commonStyles } from '../../../common/commonStyles';
import { Header, Body, Left, Right, Button, Icon } from 'native-base';
import { BackButton } from '../../../common/BackButton';
import { Restaurant, Party } from '../../../../redux/types/serverTypes';
import {
  TableMatterState,
  setParty,
  setSelectedRestaurant,
  setDate,
  setDuration,
  setTableNumbers,
} from '../../../../redux';
import { connect } from 'react-redux';
// import Icon from 'react-native-vector-icons/MaterialCommunityIcons';
import { useNavigation } from '@react-navigation/native';
import DateTimePicker from '@react-native-community/datetimepicker';
import { defaultRestaurant } from '../RestaurantPage';
import { SetTimeModal } from './SetTimeModal';
import SetPartyModal from './SetPartyModal';
import { formatTime } from '../../../utils/time-functions';
import { Grid, Row, Col } from 'react-native-easy-grid';
import { displayParty } from '../../../utils/helpers';

interface StateProps {
  restaurant: Restaurant;
  name: string;
  party: Party;
  date: string;
  duration: number;
}

interface DispatchProps {
  setSelectedRestaurant: (selectedRestaurant: Restaurant | null) => void;
  setParty: (party: Party) => void;
  setDate: (date: string) => void;
  setDuration: (duration: number) => void;
  setTableNumbers: (tableNumbers: number[]) => void;
}

type Props = DispatchProps & StateProps;

const DineInPage = (props: Props) => {
  const navigation = useNavigation();
  const [date, setDate] = useState(props.date); // new Date()
  const [mode, setMode] = useState<
    'time' | 'date' | 'datetime' | 'countdown' | undefined
  >('date');
  const [show, setShow] = useState(false);
  const [editParty, setEditParty] = useState(false);
  const [durationMinutes, setDurationMinutes] = useState(30);
  const [newDate, setNewDate] = useState(new Date(date));

  const onChange = (event, selectedDate) => {
    console.log(event);
    const currentDate = selectedDate || date;
    setShow(Platform.OS === 'ios');
    setDate(currentDate);
  };

  const showMode = (currentMode) => {
    setShow(true);
    setMode(currentMode);
  };

  const showDatepicker = () => {
    showMode('date');
  };

  const showTimepicker = () => {
    showMode('time');
  };

  const onIosChange = (event) => {
    setNewDate(event);
    console.log(event);
  };

  const saveIos = (save: boolean) => {
    save ? setDate(newDate.toISOString()) : setNewDate(new Date(date)); // newDate, date
    setShow(false);
  };

  const togglePartyModal = () => {
    setEditParty(false);
  };

  const incrementDuration = () => {
    if (durationMinutes < 180) {
      setDurationMinutes(durationMinutes + 30);
    }
  };

  const decrementDuration = () => {
    if (durationMinutes > 30) {
      setDurationMinutes(durationMinutes - 30);
    }
  };

  const handleSubmit = () => {
    props.setDate(newDate.toISOString());
    props.setDuration(durationMinutes);
    props.setTableNumbers([]);

    navigation.navigate('SeatPicker');
  };

  return (
    <View style={commonStyles.container}>
      <Header style={commonStyles.header}>
        <Left style={commonStyles.flex1}>
          <BackButton
            onPress={() => {
              setParty({
                size: 1,
                childCount: 0,
                adultCount: 1,
                elderCount: 0,
                disabledCount: 0,
              });
              setDate(new Date().toISOString());
              setDuration(30);
              setTableNumbers([]);
            }}
          />
        </Left>
        <Body style={commonStyles.body}>
          <Text testID="welcome-title" style={commonStyles.title}>
            Dine In
          </Text>
        </Body>
        <Right style={commonStyles.flex1}>
          <Icon
            name={'silverware-fork-knife'}
            type="MaterialCommunityIcons"
            style={{ color: '#FFF' }}
            onPress={() =>
              navigation.navigate('Menu', {
                reservation: true,
                dineIn: true,
                navigateToConfirmation: false,
              })
            }
          />
        </Right>
      </Header>

      <ScrollView style={commonStyles.flex1}>
        <View style={styles.restImage}>
          <Image
            style={styles.image}
            source={require('../../../../../Assets/PlaceholderGraphic.jpg')}
          />
        </View>
        <View
          style={{
            width: '90%',
            maxHeight: 100,
            alignSelf: 'center',
          }}>
          <View style={{ marginBottom: 40 }}>
            <Text style={styles.titletext}>{props.restaurant.name}</Text>
            <View
              style={{
                flex: 1,
                flexDirection: 'row',
                justifyContent: 'space-between',
                alignItems: 'flex-start',
              }}>
              <Text style={styles.addrtext}>
                {props.restaurant.addressLine1}
              </Text>
              {/* <Text style={styles.cashmoney}>$$$</Text> */}
            </View>
          </View>
        </View>

        <View style={{ width: '90%', alignSelf: 'center' }}>
          <Grid>
            <Row>
              <Col style={{ marginRight: 15 }}>
                <Text style={commonStyles.label}>Date</Text>
                <TouchableOpacity style={[commonStyles.input, styles.input]} onPress={showDatepicker}>
                  <Text>
                    {/* .toDateString() */}
                    {date ? new Date(date).toDateString() : "Date"}
                  </Text>
                </TouchableOpacity>
              </Col>
              <Col style={{ marginLeft: 15 }}>
                <Text style={commonStyles.label}>Time</Text>
                <TouchableOpacity style={[commonStyles.input, styles.input]} onPress={showTimepicker}>
                  <Text>
                    {/* date */}
                    {date ? formatTime(new Date(date)) : "Time"}
                  </Text>
                </TouchableOpacity>
              </Col>
            </Row>
            <Row>
              <Col style={{ marginRight: 15 }}>
                <Text style={commonStyles.label}>Bookee</Text>
                <TextInput
                  placeholder="Enter bookee name"
                  style={[commonStyles.input, { textAlign: 'center' }]}
                // onChangeText={(event) => setLocation(event)} Should probably allow them to change who it is under
                >
                  {props.name}
                </TextInput>
              </Col>
              <Col style={{ marginLeft: 15 }}>
                <Text style={commonStyles.label}>Duration</Text>
                <View style={[commonStyles.input]}>
                  <View style={styles.durationView}>
                    <TouchableOpacity onPress={() => decrementDuration()}>
                      <Icon name="remove" style={styles.icon}></Icon>
                    </TouchableOpacity>
                    <Text style={styles.partyText}>
                      {durationMinutes} minutes
                    </Text>
                    <TouchableOpacity onPress={() => incrementDuration()}>
                      <Icon name="add" style={styles.icon}></Icon>
                    </TouchableOpacity>
                  </View>
                </View>
              </Col>
            </Row>
            <Row>
              <Col>
                <Text style={commonStyles.label}>Party</Text>
                <TouchableOpacity
                  style={[commonStyles.input, styles.input]}
                  onPress={() => setEditParty(true)}>
                  <Text>{displayParty(props.party)}</Text>
                </TouchableOpacity>
              </Col>
            </Row>
          </Grid>
          {/* Date Picker */}
          {show && Platform.OS === 'ios' && (
            <SetTimeModal handleSave={saveIos}>
              {/* date */}
              <DatePickerIOS
                style={{ width: '100%' }}
                testID="dateTimePicker"
                onDateChange={onIosChange}
                date={newDate}
                minimumDate={new Date()}
              />
            </SetTimeModal>
          )}
          {show && Platform.OS === 'android' && (
            // date, date
            <DateTimePicker
              style={{ width: '100%' }}
              testID="dateTimePicker"
              value={newDate}
              mode={mode}
              minimumDate={new Date()}
              display="default"
              onChange={onChange}
            />
          )}
          {/* Party Modal */}
          {editParty && (
            <SetPartyModal
              toggleModal={togglePartyModal}
              party={props.party}></SetPartyModal>
          )}
        </View>

        <View style={commonStyles.buttonView}>
          <Button
            rounded
            style={commonStyles.button}
            onPress={() => handleSubmit()}>
            <Text style={commonStyles.buttonText}>FIND A TABLE</Text>
          </Button>
        </View>
      </ScrollView>
    </View>
  );
};

const mapStateToProps = (state: TableMatterState): StateProps => {
  return {
    restaurant: state.selectedRestaurant || defaultRestaurant,
    name: state.user.firstName! || 'unknown',
    party: state.newReservation.party,
    date: new Date().toISOString(),
    duration: 30,
  };
};
interface DispatchProps {
  setSelectedRestaurant: (selectedRestaurant: Restaurant | null) => void;
}
const mapDispatchToProps: DispatchProps = {
  setSelectedRestaurant,
  setParty,
  setDate,
  setDuration,
  setTableNumbers
};

// IDK why error appears it doesnt affect it from working (Add this on components until it is fixed)
// @ts-ignore
export default connect<StateProps, DispatchProps>(mapStateToProps, mapDispatchToProps)(DineInPage);
