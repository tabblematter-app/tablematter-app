import * as http from "http";
import * as express from "express";
import * as bodyParser from "body-parser";
import * as mongoose from "mongoose";
import * as WebSocket from "ws";
import counterRoutes from "./routes/counter.routes";
import { WebSocketClient } from "./websocket";
import { httpPort } from "./config/http.config";
import customerRoutes from "./routes/customer.routes";
import menuItemRoutes from "./routes/menuItem.routes";
import reservationRoutes from "./routes/reservation.routes";
import restaurantRoutes from "./routes/restaurant.routes";
import reviewRoutes from "./routes/review.routes";
import tableRoutes from "./routes/table.routes";
import { dbUrl } from "./config/mongo.config";
import { validateAll } from "./validate/validate";
import customerModel from "./models/customerModel";
import menuItemModel from "./models/menuItemModel";
import counterModel from "./models/counterModel";
import reservationModel from "./models/reservationModel";
import restaurantModel from "./models/restaurantModel";

const app = express();
app.use(bodyParser.json());

mongoose
    .connect(dbUrl, { useNewUrlParser: true, useUnifiedTopology: true })
    .then(() =>  {
        console.info("Successfully connected to the database");
        validateAll(customerModel, "Customers");
        validateAll(menuItemModel, "Menu Items");
        validateAll(counterModel, "Counter");
        validateAll(reservationModel, "Reservations");
        validateAll(restaurantModel, "Restaurants");

    })
    .catch((err) => {
        console.error("Could not connect to the database", err);
        process.exit(1);
    });

counterRoutes(app);
customerRoutes(app);
menuItemRoutes(app);
reservationRoutes(app);
restaurantRoutes(app);
reviewRoutes(app);
tableRoutes(app);

const server = http.createServer({}, app);

const webSocketServer = new WebSocket.Server({ server });

webSocketServer.on("connection", (webSocket: WebSocketClient) => {
    console.info("New web socket connection");
    console.log(webSocket);
    // Add webSocket Where needed
});

server.listen(httpPort, () => {
    console.info(`Server is listening on port ${httpPort}`);
});

mongoose.set("debug", true);

module.exports = server;
