import * as chai from "chai";
const server=require("../index");
import chaiHttp = require('chai-http');
import { isMainThread } from "worker_threads";
chai.use(chaiHttp);
const should = chai.should();


describe("menu Item", function(){
    describe ("get menuItem", function(){
        it("get all menu Items", (done) => {
            chai.request(server)
                .get("/menu-item")
                .send({})
                .end(({},res: any)=>{
                    res && res.should.have.status(200);
                    done();
                });
        });
    });
});

describe("restaurant list", function () {
    describe ("get restaurants booked by user", function () {
        it("returns empty object with no user id", (done) => {
            chai.request(server)
                .get("/restaurantsbooked")
                .send({})
                .end(({}, res: any) => {
                    res && res.data === null;
                    done();
                });
        });

        it("returns empty object with bad user id", (done) => {
            chai.request(server)
                .get("/restaurantsbooked?userId=123")
                .send({})
                .end(({}, res: any) => {
                    res && res.data === null;
                    done();
                });
        });

        it("returns restaurant list", (done) => {
            chai.request(server)
                .get("/restaurantsbooked?userId=5df06124548aa152b5ea6918")
                .send({})
                .end(({}, res: any) => {
                    res && res.should.have.status(200);
                    done();
                });
        });
    });
});