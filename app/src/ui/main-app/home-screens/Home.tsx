import React from "react";
import { connect } from "react-redux";
import { View, Text, Platform, TextInput } from "react-native";
import { Header, Left, Body, Right, Icon } from "native-base";
import { NavButton } from "./NavButton";
import { DrawerNavigationProp } from "@react-navigation/drawer";
import { DrawerParamList } from "../../utils/drawer";
import { styles } from "./homeStyles";
import API from "../../utils/api";
import { TableMatterState, setCounter, UserState, setUserReservations, setSearchResults, setSelectedRestaurant } from "../../../redux";
import { Reservation, Restaurant } from "../../../redux/types/serverTypes";
// import { styles as searchInputStyles } from "../../search/searchStyles";
import { commonStyles } from "../../common/commonStyles";
import OrderAgainBlob from "./OrderAgain/OrderAgainBlob";
import { ScrollView } from "react-native-gesture-handler";


type ProfileScreenNavigationProp = DrawerNavigationProp<
  DrawerParamList
>;

interface OwnProps {
  navigation: ProfileScreenNavigationProp; // Alternatively any
}

interface StateProps {
  value: number;
  userFirstName?: string;
  user: UserState;
}

interface DispatchProps {
  setCounter: (value: number) => void;
  setUserReservations: (reservations: Reservation[]) => void;
  setSearchResults: (results: Restaurant[]) => void;
  setSelectedRestaurant: (selectedRestaurant: Restaurant | null) => void;
}

type Props = StateProps & DispatchProps & OwnProps;

const Home = (props: Props) => {
  const [search, setSearch] = React.useState("");
  const [restaurantList, setRestaurants] = React.useState<Restaurant[]>([]);

  const handleSearch = () => {
    API.get(`/restaurant?searchString=${search}`)
      .then((result) => {
        props.setSearchResults(result.data.data);
        props.navigation.navigate("SearchResults");
      })
      .catch((err) => {
        console.log(err.message);
      });
  };

  // // TODO Remove this once a restaurant search is added (this is a demo so people know how to use it)
  // React.useEffect(() => {
  //     API.get(`/restaurant?restaurantName=chicken&locationString=m&menuItem=fried`)
  //         .then((result) => {
  //             console.log(result.data.data);
  //         })
  //         .catch((err) => {
  //             console.log(err.message);
  //         });
  // }, []);

  // TODO Might want to move this either into login or somewhere else more appropriate
  // currently here to help  with people setting up dummy profile data
  React.useEffect(() => {
    API.get(`/reservations?userId=${props.user.id}`)
      .then((result) => {
        if (result.data.status) {
          props.setUserReservations(result.data.data);
        } else {
          //Handle program error (e.g. no reservations)
        }
      })
      .catch();
  }, []);

  React.useEffect(() => {
    API.get(`/restaurantsbooked?userId=${props.user.id}`)
      .then((result) => {
        const restaurantList: Restaurant[] = result.data.data;
        setRestaurants(restaurantList);
      })
      .catch((err) => {
        console.log(err.message);
      });
  }, []);

  const GetOrderAgainList = (setSelectedRestaurant: (selectedRestaurant: Restaurant | null) => void) => {
    const restaurantElements: Element[] = [];

    if (restaurantList.length > 0) {
      restaurantList.map((value, index) => {
        restaurantElements.push(
          <View key={index}>
            <OrderAgainBlob onPress={() => setSelectedRestaurant(value)} id={value._id} name={value.name} address={value.addressLine1 ? value.addressLine1 : value.addressLine2} rating={value.averageRating}></OrderAgainBlob>
          </View>
        );
      });
    }

    if (restaurantElements.length > 0) {
      return (
        <View style={commonStyles.content}>
          <Text style={commonStyles.label}>Order Again</Text>
          <View>
            {restaurantElements}
          </View>
        </View>
      );
    } else {
      return (
        <View style={commonStyles.content}>
          <Text style={commonStyles.noResultsText}>
            You have no order history. Search for a restaurant to make one now!
          </Text>
        </View>
      );
    }
  };
  return (
    <View style={commonStyles.container}>
      <Header style={commonStyles.header}>
        <Left style={commonStyles.flex1}>
          <NavButton navigation={props.navigation} />
        </Left>
        <Body style={commonStyles.body}>
          <Text testID="welcome-title" style={commonStyles.title}>Welcome {props.userFirstName}</Text>
        </Body>
        <Right style={commonStyles.flex1} />
        {/* {Platform.OS === "ios" && <Right />} */}
      </Header>

      <View style={styles.searchBarWrapper}>
        <TextInput
          placeholder="Search for a restaurant"
          style={[commonStyles.input, styles.searchBar]}
          onSubmitEditing={handleSearch}
          onChangeText={(event) => setSearch(event)}
        >
          {search}
        </TextInput>
        <View style={styles.filterIcon}>
          {/* Not sure why Ionicons (defualt) filter won't work */}
          <Icon onPress={() => props.navigation.navigate("Search")} type="MaterialCommunityIcons" name="filter-variant" style={styles.searchSettingsButton} />
        </View>
      </View>
      <ScrollView style={{ paddingBottom: Platform.OS === "ios" ? 200 : 120 }}>
        {GetOrderAgainList(props.setSelectedRestaurant)}
      </ScrollView>
    </View>
  );
};

const mapStateToProps = (state: TableMatterState): StateProps => {
  return {
    value: state.counter.value,
    userFirstName: state.user.firstName,
    user: state.user,
  };
};

const mapDispatchToProps: DispatchProps = {
  setCounter,
  setUserReservations,
  setSearchResults,
  setSelectedRestaurant
};

// IDK why error appears it doesnt affect it from working (Add this on components until it is fixed)
// @ts-ignore 
export default connect<StateProps, DispatchProps>(mapStateToProps, mapDispatchToProps)(Home);
