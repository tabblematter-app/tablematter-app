import { TableMatterActionTypes, NewReservationState, SET_RESERVATION_RESTAURANT_ACTION, SET_DURATION_ACTION, SET_DATE_ACTION, ADD_MENU_ITEM_ACTION, REMOVE_MENU_ITEM_ACTION, SET_PARTY_ACTION, SET_TABLE_NUMBERS_ACTION, SET_DINE_IN_ACTION, SET_PAID_ACTION, RESET_RESERVATION_ACTION } from "../types";

const defaultNewReservation: NewReservationState = {
  restaurant: {
    id: "",
    name: "",
    addressLine1: "",
    addressLine2: "",
    suburb: "",
    state: "",
    phone: "",
  },
  tableNumbers: [],
  orders: [],
  totalPrice: 0,
  dineIn: true,
  party: {
    size: 0,
    elderCount: 0,
    adultCount: 0,
    childCount: 0,
    disabledCount: 0,
  },
  duration: 0,
  paid: false,
  date: ""
};

export const newReservation = (state: NewReservationState = defaultNewReservation, action: TableMatterActionTypes): NewReservationState => {
  switch (action.type) {
    case RESET_RESERVATION_ACTION:
      return {
        ...defaultNewReservation,
        orders: [],
      };

    case SET_RESERVATION_RESTAURANT_ACTION:
      return {
        ...state,
        restaurant: action.restaurant,
      };

    case SET_TABLE_NUMBERS_ACTION:
      return {
        ...state,
        tableNumbers: action.tableNumbers,
      };

    case ADD_MENU_ITEM_ACTION:
      state.orders.push(action.menuItem);
      return {
        ...state,
        orders: state.orders,
        totalPrice: state.orders.reduce((total, item) => total + item.price, 0),
      };

    case SET_DINE_IN_ACTION:
      return {
        ...state,
        dineIn: action.dineIn,
      };

    case SET_PAID_ACTION:
      return {
        ...state,
        paid: action.paid
      };
    // TODO: Add nicer way of only removing one
    case REMOVE_MENU_ITEM_ACTION:
      let numberRemoved = 0;
      const newOrders = state.orders.filter((item) => {
        item === action.menuItem && numberRemoved++;
        return item !== action.menuItem || numberRemoved >= (action.numberToRemove + 1);
      });
      return {
        ...state,
        orders: newOrders,
        totalPrice: newOrders.reduce((total, item) => total + item.price, 0),
      };

    case SET_PARTY_ACTION:
      return {
        ...state,
        party: action.party,
      };

    case SET_DURATION_ACTION:
      return {
        ...state,
        duration: action.duration
      };
    case SET_DATE_ACTION:
      return {
        ...state,
        date: action.date
      };
  }

  return state;
};
