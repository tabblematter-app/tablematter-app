import * as chai from "chai";
const server=require("../index");
import chaiHttp = require('chai-http');
import { isMainThread } from "worker_threads";
chai.use(chaiHttp);
const should = chai.should();


describe("table", function(){
    describe ("get tables", function(){
        it("get all tables", (done) => {
            chai.request(server)
                .get("/table")
                .send({})
                .end(({},res: any)=>{
                    res && res.should.have.status(200);
                    done();
                });
        });
    });
});