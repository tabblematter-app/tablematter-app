
import React from 'react';
import Home from './home-screens/Home';
import SearchPage from '../search/search';
import { View, Icon, Text } from 'native-base';
import { Drawer } from '../utils/drawer';
import { StyleSheet } from 'react-native';
import ProfilePage from '../profile-page/ProfilePage';
import { p1 } from '../utils/themes/lightTheme';
import DineInTakeawayHistory from '../dine-in-takeaway-history/DineInTakeawayHistory';
import LogOutPage from './LogOutPage';
import SettingsPage from './settings/SettingsPage';

export const Main = () => {
  return (
    <View style={styles.container}>
      <Drawer.Navigator
        drawerStyle={styles.drawerStyles}
        initialRouteName="Home">
        <Drawer.Screen
          options={{
            drawerIcon: MenuIconHome,
            drawerLabel: MenuTextHome,
          }}
          name="Home"
          component={Home}
        />

        <Drawer.Screen
          name="Profile"
          options={{
            drawerIcon: MenuIconProfile,
            drawerLabel: MenuTextProfile,
          }}
          component={ProfilePage}
        />

        <Drawer.Screen
          options={{
            drawerIcon: MenuIconSearch,
            drawerLabel: MenuTextSearch,
          }}
          name="Search"
          component={SearchPage}
        />

        <Drawer.Screen
          options={{
            drawerIcon: MenuIconHistory,
            drawerLabel: MenuTextHistory,
          }}
          name="History"
          component={DineInTakeawayHistory}
        />

        <Drawer.Screen
          options={{
            drawerIcon: MenuIconSettings,
            drawerLabel: MenuTextSettings,
          }}
          name="Settings"
          component={SettingsPage}
        />

        {/* Help goes nowhere so commented out for now */}
        {/* <Drawer.Screen
          options={{
            drawerIcon: MenuIconHelp,
            drawerLabel: MenuTextHelp,
          }}
          name="Help"
          component={Home}
        /> */}

        <Drawer.Screen
          options={{
            drawerIcon: MenuIconLogOut,
            drawerLabel: MenuTextLogOut,
          }}
          name="LogOutPage"
          component={LogOutPage}
        />

      </Drawer.Navigator>
    </View>
  );
};

{
  /*These define the styling and icons for the menu options (May be worth moving to an external file)*/
}
const MenuIconProfile = () => {
  return <Icon style={{ color: '#fff' }} name={'person'} />;
};

const MenuTextProfile = () => {
  return (
    <Text testID="profile-button" style={styles.buttonText}>
      Profile
    </Text>
  );
};

const MenuIconHome = () => {
  return <Icon style={{ color: '#fff' }} name={'home'} />;
};

const MenuTextHome = () => {
  return <Text style={styles.buttonText}>Home</Text>;
};

const MenuIconSearch = () => {
  return <Icon style={{ color: '#fff' }} name={'search'} />;
};

const MenuIconSettings = () => {
  return <Icon style={{ color: '#fff' }} name={'settings'} />;
};

const MenuTextSearch = () => {
  return <Text style={styles.buttonText}>Search</Text>;
};

const MenuTextSettings = () => {
  return <Text style={styles.buttonText}>Settings</Text>;
};

const MenuIconHistory = () => {
  return (
    // Not sure why "list" doesn't work
    <Icon style={{ color: '#fff' }} type="Foundation" name={'list-bullet'} />
  );
};

const MenuTextHistory = () => {
  return <Text style={styles.buttonText}>History</Text>;
};

// const MenuIconHelp = () => {
//   return <Icon style={{ color: '#fff' }} name={'information-circle'} />;
// };

// const MenuTextHelp = () => {
//   return <Text style={styles.buttonText}>Help</Text>;
// };

const MenuIconLogOut = () => {
  return <Icon style={{ color: '#fff' }} name={'log-out'} />;
};

const MenuTextLogOut = () => {
  return <Text style={styles.buttonText}>Log Out</Text>;
};

export const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: p1,
  },
  drawerStyles: {
    backgroundColor: p1,
  },
  buttonText: {
    color: '#fff',
  },
});
