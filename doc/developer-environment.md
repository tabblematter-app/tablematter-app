# Developer Environment Setup

* Download node v12 - https://nodejs.org/en/
* Download androuid studio - https://developer.android.com/studio/
* Pull code from this repo

Note: for android studio ensure you download sdk v29 as well as follow the instruction below to setup emulator
* https://developer.android.com/studio/run/emulator

## Docker containers
Not added yet


## Server 
Not added yet

## table-matter-redux
```bash
 $ cd ./table-matter-redux
 $ npm install
 $ # To build
 $ npm run build
```

## table-matter-app
```bash
 $ cd ./table-matter-app
 $ npm install
 $ npm install --save-dev @haul-bundler/cli
 $ # To build
 $ npx haul start
 $ # In another CLI
 $ react-native run-android
```