
describe('Example', () => {
  beforeEach(async () => {
    await device.reloadReactNative();
  });

  it('should have welcome screen', async () => {
    await expect(element(by.id('title'))).toBeVisible();
  });

  it('should be able to log in', async () => {
    await expect(element(by.id('title'))).toBeVisible();
    await element(by.id('log-in')).tap();
    await element(by.id('email-input')).replaceText('example@mail.com.au');
    await element(by.id('password-input')).replaceText('INotPlainText');
    await element(by.id('login-button')).tap();
    await expect(element(by.id('welcome-title'))).toBeVisible();
  });

  // TODO Change this to create new account
  it('should display new account screen', async () => {
    await element(by.id('new-account')).tap();
    await expect(element(by.id('new-account-title'))).toBeVisible();
  });

  it('Should access the profile page', async () => {
    await expect(element(by.id('title'))).toBeVisible();
    await element(by.id('log-in')).tap();
    await element(by.id('email-input')).replaceText('example@mail.com.au');
    await element(by.id('password-input')).replaceText('INotPlainText');
    await element(by.id('login-button')).tap();
    await expect(element(by.id('welcome-title'))).toBeVisible();
    await element(by.id('nav-button')).tap();
    await element(by.id('profile-button')).tap();
    await expect(element(by.id('profile-title'))).toBeVisible();
  });

  it('Should access the restaurant page', async () => {
    await expect(element(by.id('title'))).toBeVisible();
    await element(by.id('log-in')).tap();
    await element(by.id('email-input')).replaceText('example@mail.com.au');
    await element(by.id('password-input')).replaceText('INotPlainText');
    await element(by.id('login-button')).tap();
    await expect(element(by.id('welcome-title'))).toBeVisible();
    await element(by.id('rest-button')).tap();
    await expect(element(by.id('restaurant-title'))).toBeVisible();
  });

  it('Should access the search results page', async () => {
    await expect(element(by.id('title'))).toBeVisible();
    await element(by.id('log-in')).tap();
    await element(by.id('email-input')).replaceText('example@mail.com.au');
    await element(by.id('password-input')).replaceText('INotPlainText');
    await element(by.id('login-button')).tap();
    await expect(element(by.id('welcome-title'))).toBeVisible();
    await element(by.id('search-bar')).replaceText('chicken');
    await element(by.id('search-bar')).tapReturnKey();
    await expect(element(by.id('search-results-title'))).toBeVisible();
  });

  it('Should access the menu page', async () => {
    await expect(element(by.id('title'))).toBeVisible();
    await element(by.id('log-in')).tap();
    await element(by.id('email-input')).replaceText('example@mail.com.au');
    await element(by.id('password-input')).replaceText('INotPlainText');
    await element(by.id('login-button')).tap();
    await expect(element(by.id('welcome-title'))).toBeVisible();
    await element(by.id('search-bar')).replaceText('chicken');
    await element(by.id('search-bar')).tapReturnKey();
    await expect(element(by.id('search-results-title'))).toBeVisible();
    await element(by.id('result-0')).tap();
    await expect(element(by.id('restaurant-title'))).toBeVisible();
    await element(by.id('menu')).tap();
    await expect(element(by.id('menu-title'))).toBeVisible();

  });

});
