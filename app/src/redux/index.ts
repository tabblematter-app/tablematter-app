import { compose, createStore, applyMiddleware } from "redux";
import tableMatterApp from "./reducers/index";
import { tableMatterMiddleware } from "./middleware";
import { setupCounter } from "./setup-data/counter";

export * from "./actions/index";
export * from "./reducers/index";
export * from "./types/index";

declare global {
    interface Window {
        __REDUX_DEVTOOLS_EXTENSION_COMPOSE__?: typeof compose;
    }
}

const composeEnhancers = compose;
export const store = createStore(tableMatterApp, composeEnhancers(
    applyMiddleware(tableMatterMiddleware),
));


export const setupTableMatter = async () => {
    setupCounter(store);
};