const constants = {
    profilePicturePickerConfig: {
        title: 'Upload New Profile Picture',
        customButtons: [{ name: 'delete', title: "Remove Profile Picture" }],
        storageOptions: {
            skipBackup: true,
            path: 'images',
        }
    },
    uploaderConfig: {
        keyPrefix: '',  //S3 key prefix (if any)
        bucket: 'team46-test',  //S3 bucket name
        region: 'ap-southeast-2',   //S3 region
        accessKey: 'AKIA43J5IEOQQGTW7LSP',  //S3 app access key
        secretKey: 'DmYAfEcRC/s3VHo4sP/Bb43n0kanjofhHDQg0Umv',  //S3 app secret key
        successActionStatus: 201
    },
    payPalConfig: {
        username: "",   //your paypal app client ID
        password: ""    //your paypal app secret ID
    }
};

export default constants;