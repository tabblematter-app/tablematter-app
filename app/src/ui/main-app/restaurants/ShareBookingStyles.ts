import { StyleSheet } from 'react-native';
import { bgColor, p1 } from '../../utils/themes/lightTheme';

export const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: bgColor,
  },
  header: {
    backgroundColor: p1,
  },
  title: {
    color: 'white',
    fontSize: 25,
    width: '100%',
    textAlign: 'center',
    paddingRight: 45,
  },
  shareBooking: {
    color: 'red',
    fontSize: 21,
    textAlign: 'left',
    paddingTop: 25,
    paddingLeft: 45,
    paddingBottom: 25,
  },
  Comments: {
    color: 'red',
    fontSize: 21,
    textAlign: 'left',
    paddingLeft: 45,
  },
  commentsInput: {
    flex: 1,
    textAlignVertical: 'top',
    borderColor: p1,
    borderWidth: 2, // How thick the border is
    paddingLeft: 14, // shift font from the left
    //paddingBottom: 150, // padding of text from the top
    //alignItems: 'stretch',
    // height: 180, // height of text box
    //width: 320, // width of text box
    marginTop: 9, // distance between Comments text and text box
    borderRadius: 10, // how circular the textbox is
    backgroundColor: 'white',
    fontSize: 14,
    marginHorizontal: 45, // centers the textbox
  },
  social: {
    color: '#CA2121',
    fontSize: 20,
    textAlign: 'center',
    paddingLeft: 60,
    paddingBottom: 30,
  },
  smallIcons: {
    color: '#CA2121',
    textAlign: 'left',
    paddingLeft: 40,
    //marginVertical: 4,
    paddingTop: '12.5%',
  },
  smallIconsPartynTable: {
    color: '#CA2121',
    textAlign: 'left',
    paddingLeft: 36,
    //marginVertical: 4,
    paddingTop: '7.5%',
  },
  smallIconsAddress: {
    color: '#CA2121',
    textAlign: 'left',
    paddingLeft: 40,
    //marginVertical: 1,
    paddingTop: '3.5%',
  },
  shareContainer: {
    marginTop: 11,
    justifyContent: 'center',
    flex: 1,
    flexDirection: 'row',
  },

  // -- Styles for Share Button
  button: {
    borderColor: p1,
    borderWidth: 1, // How thick the border is
    height: 60, // height of text box
    width: 280, // width of text box
    borderRadius: 30, // how circular the textbox is
    //marginHorizontal: '15%', // centers the textbox
    //marginVertical: '15%', // centers the textbox
    backgroundColor: p1,
  },
  buttonText: {
    paddingTop: '5%', // centers the text
    textAlign: 'center',
    color: 'white',
    fontSize: 20,
  },
  // --

  desc: {
    width: '20',
  },
  right: {
    flex: 1,
  },
  left: {
    flex: 1,
  },
  body: {
    flex: 5,
  },
  scrollView: {
    //backgroundColor: 'pink',
    //marginHorizontal: 20,
    flex: 1,
  },
});
