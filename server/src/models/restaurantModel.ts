import * as mongoose from "mongoose";
import { tableSchema } from "./tableModel";
import { menuItemSchema } from "./menuItemModel";
import { defaultOpenHours, Restaurant } from "../types/types";

 interface CustomSearchModel extends mongoose.Model<any> {
    search: (searchString: string, callback: any) => void;
}

export const restaurantSchema = new mongoose.Schema({
    name: String,
    desc: String,
    openHours: {
        type: Array,
        default: defaultOpenHours,
    },
    phone: String,
    email: String,
    website: String,
    addressLine1: String,
    addressLine2: String,
    suburb: String,
    state: String,
    images: [{url: String, alt: String}],
    menu: [menuItemSchema], // Embed; always retrieved with restaurant and never used for another restaurant
    tables: [tableSchema], // Embed; always retrieved with restaurant and never used for another restaurant
    staff: [{   // Do not embed; one staff may work at more than one restaurant
        staffId: String,
        privilege: { // Privilege assigned per restaurant
            type: String,
            enum: ['Standard', 'Admin'],
            default: 'Standard',
        },
    }],
    averageRating: Number,
    // Do not store reviews here as the array may become large
    // Do not store the reservations with the restaurant as the array may become very large
});

// This causes all fields to be used in seach (can change if it causes issues)
restaurantSchema.index({ name: "text", desc: "text", addressLine1: "text"},
    { weights: { name: 5, desc: 3, addressLine1: 1, } });

restaurantSchema.statics = {
    searchPartial: function(q: string, callback: any) {
        return this.find({
            $or: [
                { "name": new RegExp(q, "gi") },
                { "desc": new RegExp(q, "gi") },
                { "addressLine1": new RegExp(q, "gi") },
            ]
        }, callback);
    },

    searchFull: function (q: string, callback: any) {
        return this.find({
            $text: { $search: q, $caseSensitive: false }
        }, callback);
    },

    search: function(q: string, callback: any) {
        this.searchFull(q, (err: object, data: Restaurant[]) => {
            if (err) return callback(err, data);
            if (!err && data.length) return callback(err, data);
            if (!err && data.length === 0) return this.searchPartial(q, callback);
        });
    },
};

export default mongoose.model<Restaurant & mongoose.Document>('Restaurant', restaurantSchema) as CustomSearchModel;