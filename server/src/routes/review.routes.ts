import * as express from "express";
import { Response } from "express";
import { findAll } from "../controllers/model.functions";
import reviewModel from "../models/reviewModel";
import { addReview, updateReview, deleteReview, getUserReviews, getReviewById, getReviewsForRestaurant } from "../controllers/review.controller";

export default (app: express.Express) => {
    app.get("/review", (_, res: Response) => findAll(res, reviewModel));

    app.get("/review:reviewId", (req, res: Response) => {
        getReviewById(req.params.reviewId, res);
    });

    app.get("/review/:userId", (req, res: Response) => {
        getUserReviews(req.params.userId, null, res);
    });

    app.get("/review/:userId/:restaurantId", (req, res: Response) => {
        getUserReviews(req.params.userId, req.params.restaurantId, res);
    });

    app.get("/restaurantReviews", (req, res: Response) => {
        getReviewsForRestaurant((req.query.restaurantId as string) || "", res);
    });  

    app.post("/review", (req, res: Response) => {
        addReview(req.body.reservationId, req.body.customerId, req.body.restaurantId, req.body.reviewerName, req.body.rating, req.body.content, res);
    });

    app.patch("/review", (req, res: Response) => {
        updateReview(req.body.reviewId, req.body.reservationId, req.body.customerId, req.body.restaurantId, req.body.reviewerName, req.body.rating, req.body.content, res);
    });

    app.delete("/review/:id", (req, res: Response) => {
        deleteReview(req.params.id, res);
    });
};
