import React, { useEffect } from "react";
import { Icon, View } from "native-base";
import { styles } from "./UserReviewStyles";
import { TouchableWithoutFeedback } from "react-native-gesture-handler";
import { useState } from "react";

const forceUpdate = () => {
  const [value, setValue] = useState(0);
  {/** added null check purely to stop lint complaining about value not being read, 
    as this value var is required for force updating to function correctly. */}
  if (value != null) {   //Should never get here.
    return () => setValue(value => ++value);
  } else {
    return () => setValue(value => ++value);
  }
};


// let rating = 0;

interface UserReviewStarsProps {
  rating?: number;
  updateRatingFunction: Function;
}

interface StarProps {
  selected: boolean;
  half: boolean;
}

const UserReviewStars = (props: UserReviewStarsProps) => {
  const [rating, setRating] = React.useState<number>(0);

  {/* Use Effect to only set the rating on first load */ }
  useEffect(() => {
    setRating(props.rating ? props.rating : 0);
  }, [props.rating]);

  {/*loop through the stars value to correctly present the users rating*/ }
  const stars: Element[] = [];

  const callForceUpdate = forceUpdate();

  for (let x = 1; x <= 5; x++) {
    stars.push(
      <TouchableWithoutFeedback key={x} onPress={() => {
        setRating(x);
        callForceUpdate();
        props.updateRatingFunction(x);
      }}>
        <View>
          <Star half={x - rating === 0.5 ? true : false} selected={x <= rating ? true : false} />
        </View>
      </TouchableWithoutFeedback>
    );
  }

  return (
    <View style={styles.ratingWrapper}>
      {stars}
    </View>
  );
};

{/*Star icon definition*/ }
const Star = (props: StarProps) => {
  return (
    <Icon name={props.half === true ? "star-half" : "star"} style={[styles.userInfoIcon, styles.ratingStar, selectedState(props.selected || props.half)]} />
  );
};

{/*Conditional styling dictating the colour of the stars*/ }
const selectedState = (isSelected: boolean) => {
  if (!isSelected) {
    return styles.ratingStarNotSelected;
  } else {
    return '';
  }
};

export default UserReviewStars;