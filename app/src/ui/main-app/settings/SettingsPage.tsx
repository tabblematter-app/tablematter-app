import React from 'react';
import { Platform } from "react-native";
import { Header, Body, Title, View, Left, Right, Text, List, ListItem, Icon } from "native-base";
import { ScrollView } from 'react-native-gesture-handler';
import { BackButton } from '../../common/BackButton';
import { useNavigation } from '@react-navigation/native';
import { commonStyles } from '../../common/commonStyles';
import { SettingsType } from './SettingsSubMenu';

interface SettingCategory {
  settingCategoryName: string;
  settingSections: SettingSection[];
}

interface SettingSection {
  iconType: string;
  iconName: string;
  settingSectionName: string;
  navigation: string;
  settingsType: SettingsType;
}

const SettingsPage = () => {

  const navigator = useNavigation();

  // These define the setting categoris and sections to render
  const settingCategories: SettingCategory[] = [
    {
      settingCategoryName: 'Account Settings',
      settingSections: [
        { iconType: 'Ionicons', iconName: 'person', settingSectionName: 'Profile Info', navigation: 'EditProfile', settingsType: SettingsType.Account },
        // { iconType:'FontAwesome', iconName:'credit-card-alt', settingSectionName:'Payment Info', navigation: 'SettingsSubMenu', settingsType: SettingsType.Payment },
        // { iconType:'MaterialCommunityIcons', iconName:'truck-delivery', settingSectionName:'Delivery Options', navigation: 'SettingsSubMenu', settingsType: SettingsType.Delivery },
      ]
    },
    {
      settingCategoryName: 'Payment Settings',
      settingSections: [
        { iconType: 'MaterialCommunityIcons', iconName: 'file-document', settingSectionName: 'Legal', navigation: 'SettingsSubMenu', settingsType: SettingsType.Legal }
      ]
    }
  ];

  return (
    <View>
      <Header style={commonStyles.header}>
        <Left style={commonStyles.flex1}>
          <BackButton />
        </Left>
        <Body style={commonStyles.body}>
          <Title testID="menu-title" style={commonStyles.title}>Settings</Title>
        </Body>
        <Right style={commonStyles.flex1} />
      </Header>
      <ScrollView style={{ marginBottom: Platform.OS === "ios" ? 150 : 70 }}>
        {settingCategories.map((category) => {
          return (
            <List key={settingCategories.indexOf(category)} >
              <Text style={[{ paddingTop: 20, paddingLeft: 20, color: 'grey' }]}>{category.settingCategoryName}</Text>
              {category.settingSections.map((section) => {
                return (
                  <ListItem key={category.settingSections.indexOf(section)}
                    onPress={() => {
                      navigator.navigate(section.navigation, {
                        Props: {
                          settingsType: section.settingsType,
                          sectionName: section.settingSectionName,
                        }
                      });
                    }}>
                    {/* Typescript flags section.iconType as an error as it does not explicitly match the pre-defined strings.
                          Supressing as it's a false flag.*/}
                    {/* @ts-ignore */}
                    <Icon type={section.iconType} name={section.iconName} style={{ paddingRight: 10 }} />
                    <Text>{section.settingSectionName}</Text>
                  </ListItem>
                );
              })}
            </List>
          );
        })}
      </ScrollView>
    </View>
  );
};

export default SettingsPage;
