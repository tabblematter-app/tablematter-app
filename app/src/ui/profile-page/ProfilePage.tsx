import React, { useEffect } from "react";
import { connect } from "react-redux";
import { View, Text, Image } from "react-native";
import { Header, Left, Body, Icon, Right } from "native-base";
import { NavButton } from "../main-app/home-screens/NavButton";
import { DrawerNavigationProp } from "@react-navigation/drawer";
import { DrawerParamList } from "../utils/drawer";
import { styles } from "./profilePageStyles";
import { TableMatterState, setReviews, ReviewData } from "../../redux";
import OrderHistoryBlob from "./OrderHistoryBlob";
import { ScrollView } from "react-native-gesture-handler";
import { useNavigation } from "@react-navigation/native";
import { commonStyles } from "../common/commonStyles";
import API from "../utils/api";

type ProfileScreenNavigationProp = DrawerNavigationProp<
  DrawerParamList
>;

interface OwnProps {
  navigation: ProfileScreenNavigationProp;
}

interface StateProps {
  userProfilePictureUrl?: string;
  userFirstName?: string;
  userLastName?: string;
  userEmailAddress?: string;
  userPhoneNumber?: string;
  userId?: string;
  reviews: ReviewData[];
}

interface ReservationData {
  reservationId: string;
  customerId: string;
  restaurantId: string;
  restaurantName: string;
  price: number;
  dineIn: boolean;
}

interface GetOrderHistoryListProps {
  reservations: ReservationData[];
  reviews: ReviewData[];
  userId: string;
}

interface DispatchProps {
  setReviews: (reviews: ReviewData[]) => void;
}

type Props = StateProps & DispatchProps & OwnProps;

const ProfilePage = (props: Props) => {
  const navigation = useNavigation();
  // const [profilePictureUrl, setProfilePictureUrl] = React.useState(props.userProfilePictureUrl ? props.userProfilePictureUrl : "");
  const [orderHistory, setOrderHistory] = React.useState<ReservationData[]>([]);

  //Get order history on page load
  useEffect(() => {
    API.get(`/reservations?userId=${props.userId}`)
      .then((result) => {
        if (result.status === 200) {
          const reservationListData = result.data.data;
          const reservationsList: ReservationData[] = [];
          reservationListData.map((value) => {
            reservationsList.push({
              reservationId: value._id,
              customerId: value.customer.id,
              restaurantId: value.restaurant.id,
              restaurantName: value.restaurant.name,
              dineIn: value.dineIn,
              price: value.price
            });
          });

          setOrderHistory(reservationsList);
        } else {
          console.log('An unexpected error occurred while trying to retrieve reservations for user');
        }
      })
      .catch((error) => {
        console.log('An error occurred while trying to retrieve reservations for user');
        console.log(error);
      });

  }, []);

  //Get reviews for user when order history changes
  useEffect(() => {
    API.get(`/review?userId=${props.userId}`)
      .then((result) => {
        if (result.status === 200) {
          const reviewsListData = result.data;
          const reviewsList: ReviewData[] = [];

          reviewsListData.map((value) => {
            reviewsList.push({
              reviewId: value._id,
              reservationId: value.reservationId,
              customerId: value.customerId,
              restaurantId: value.restaurantId,
              reviewerName: value.reviewerName,
              rating: value.rating,
              content: value.content,
            });
          });

          props.setReviews(reviewsList);
        } else {
          console.log('An unexpected error occurred while trying to retrieve reviews for user');
        }
      })
      .catch((error) => {
        console.log('An error occurred while trying to retrieve reviews for user');
        console.log(error);
      });

  }, []);

  const GetOrderHistoryList = (orderHistoryListProps: GetOrderHistoryListProps) => {
    const orderElements: Element[] = [];

    if (orderHistoryListProps.reservations) {
      orderHistoryListProps.reservations.map((value, index) => {
        const review = orderHistoryListProps.reviews.find(x => x.reservationId === value.reservationId);
        orderElements.push(
          <View key={index}>
            <OrderHistoryBlob orderHistoryID={value.reservationId} restaurantName={value.restaurantName} dineIn={value.dineIn} price={value.price} reviewId={review?.reviewId}
              rating={review?.rating} reviewText={review?.content} customerId={orderHistoryListProps.userId} restaurantId={value.restaurantId} reviewerName={review?.reviewerName}></OrderHistoryBlob>
          </View>
        );
      });
    }

    if (orderElements.length > 0) {
      return (
        <View>
          {orderElements}
        </View>
      );
    } else {
      return (
        <View>
          <Text style={commonStyles.noResultsText}>
            You have no order history. Search for a restaurant to make one now!
          </Text>
        </View>
      );
    }
  };

  return (
    <View style={commonStyles.container}>
      {/*Header section of the page*/}
      <Header style={commonStyles.header}>
        <Left style={commonStyles.flex1}>
          <NavButton navigation={props.navigation} />
        </Left>
        <Body style={commonStyles.body}>
          <Text testID="profile-title" style={commonStyles.title}>Profile</Text>
        </Body>
        <Right style={commonStyles.flex1}>
          <Icon style={{ color: '#fff' }} type="MaterialCommunityIcons" name="dots-vertical" onPress={() => { navigation.navigate("EditProfile"); }} />
        </Right>
      </Header>

      <ScrollView>
        <View style={commonStyles.content}>
          {/*Profile picture and user name*/}
          <View style={styles.profilePic}>
            {
              (props.userProfilePictureUrl && props.userProfilePictureUrl !== "undefined") ?
                <Image style={styles.profilePic} source={{ uri: props.userProfilePictureUrl }} />
                :
                <Icon style={[styles.userInfoIcon, styles.profileIcon]} name="person" />
            }
          </View>

          <Text style={styles.userNames}>{props.userFirstName} {props.userLastName}</Text>
          {/*User information*/}
          <View style={styles.userInfo}>
            <View style={styles.userInfoSection}>
              <Icon style={[styles.userInfoIcon, { marginRight: 10 }]} name="mail" /><Text style={styles.userInfoText}>{props.userEmailAddress}</Text>
            </View>
            <View style={styles.userInfoSection}>
              <Icon style={[styles.userInfoIcon, { marginRight: 10 }]} name="call" /><Text style={styles.userInfoText}>{props.userPhoneNumber}</Text>
            </View>
          </View>
          {/*Order history*/}
          <View style={{ paddingBottom: 20, flex: 1 }}>
            <View>
              <Text style={commonStyles.label}>Order History</Text>
              {props.userId && (
                <GetOrderHistoryList reservations={orderHistory} reviews={props.reviews} userId={props.userId} />
              )}
            </View>
          </View>
        </View>
      </ScrollView>
    </View>
  );
};

const mapStateToProps = (state: TableMatterState): StateProps => {
  return {
    userProfilePictureUrl: state.user.profilePictureUrl,
    userFirstName: state.user.firstName,
    userLastName: state.user.lastName,
    userEmailAddress: state.user.email,
    userPhoneNumber: state.user.phone,
    userId: state.user.id,
    reviews: state.reviews,
  };
};

const mapDispatchToProps: DispatchProps = {
  setReviews,
};

// @ts-ignore 
export default connect<StateProps, DispatchProps>(mapStateToProps, mapDispatchToProps)(ProfilePage);
