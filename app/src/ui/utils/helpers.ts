import { Party, Restaurant } from '../../redux/types/serverTypes';
import moment from 'moment';

export const displayParty = (party: Party) => {
  const partyArray: string[] = [];
  let partyString = "";
  if (party.adultCount > 0) {
    partyArray.push(`${party.adultCount} Adult`);
  };
  if (party.elderCount > 0) {
    partyArray.push(`${party.elderCount} Seniors`);
  };
  if (party.childCount > 0) {
    partyArray.push(`${party.childCount} Child`);
  };
  // if (party.disabledCount > 0) {
  //   partyArray.push(`${party.disabledCount} Disabled`)
  // };
  partyArray.forEach((partyType, index, partyArray) => {
    if (index === partyArray.length - 1)
      partyString += `${partyType}`;
    else
      partyString += `${partyType}, `;
  });
  return partyString === "" ? "Select party" : partyString;
};

// For now, split open hours into 30 minute booking windows
export const openHoursToIntervals = (restaurant: Restaurant, date: string) => {
  // Possible to change open hours to start on Sunday instead?
  const day = new Date(date).getDay();
  // "0800" does not convert nicely
  // restaurant.openHours[day][0] = start time as "0800"
  const startHours = restaurant.openHours[day][0].slice(0, 2);
  const startMinutes = restaurant.openHours[day][0].slice(2, 4);
  const startTime = moment().utcOffset(0).set({ hours: startHours, minutes: startMinutes });
  const endHours = restaurant.openHours[day][1].slice(0, 2);
  const endMinutes = restaurant.openHours[day][1].slice(2, 4);
  const endTime = moment().utcOffset(0).set({ hours: endHours, minutes: endMinutes });
  const bookingIntervals: string[] = [];
  while (!startTime.isSame(endTime)) {
    // Store interval in array as something usable
    bookingIntervals.push(startTime.format('HH:mm'));
    startTime.add(30, 'minute');
  }
  return bookingIntervals;
};

export const displayEndTime = (timeBooked, duration) => {
  return moment(timeBooked).add(duration, 'minutes').format('h:mm a');
};