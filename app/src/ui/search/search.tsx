import React, { useState } from 'react';
import { connect } from "react-redux";
import { styles } from "./searchStyles";
import { commonStyles } from "../common/commonStyles";
import { TextInput } from "react-native";
import { Header, Body, Title, Button, Text, View, Picker, Left, Right } from "native-base";
import { ScrollView } from 'react-native-gesture-handler';
import { BackButton } from '../common/BackButton';
import API from '../utils/api';
import { setSearchResults } from "../../redux";
import { DrawerNavigationProp } from "@react-navigation/drawer";
import { Restaurant } from "../../redux/types/serverTypes";
import { DrawerParamList } from "../utils/drawer";

type NavigationProp = DrawerNavigationProp<DrawerParamList>;

interface OwnProps {
  navigation: NavigationProp;
}

interface DispatchProps {
  setSearchResults: (results: Restaurant[]) => void;
}

type Props = OwnProps & DispatchProps;

const SearchPage = (props: Props) => {
  const [restaurantName, setName] = useState("");
  const [restaurantLocation, setLocation] = useState("");
  // Used for dietary requirements picker
  const [dietaryReqs, setDietaryReqs] = useState<{ selected: string }>({
    selected: "none"
  });
  const [menuItem, setMenuItem] = useState("");

  const handleSearch = () => {
    // API.get(`/restaurant?searchString=${search}`)
    // console.log(restaurantName)
    // console.log(restaurantLocation)
    // console.log(dietaryReqs.selected)
    // console.log(menuItem)
    API.get(`/restaurant?restaurantName=${restaurantName}&locationString=${restaurantLocation}&menuItem=${menuItem}&filters[]=${dietaryReqs.selected}`)
      .then((result) => {
        props.setSearchResults(result.data.data);
        props.navigation.navigate("SearchResults");
      })
      .catch((err) => {
        console.log(err.message);
      });
  };

  return (
    <View style={commonStyles.container}>
      <Header style={commonStyles.header}>
        <Left style={commonStyles.flex1}>
          <BackButton />
        </Left>
        <Body style={commonStyles.body}>
          <Title style={commonStyles.title}>Search</Title>
        </Body>
        <Right style={commonStyles.flex1} />
        {/* {Platform.OS === "ios" && <Right />} */}
      </Header>

      <ScrollView>
        <View style={commonStyles.inputView}>
          <Text style={commonStyles.label}>Name</Text>
          <TextInput
            placeholder="Enter name"
            style={commonStyles.input}
            onChangeText={(event) => setName(event)}
          >
            {restaurantName}
          </TextInput>
          <Text style={commonStyles.label}>Location</Text>
          <TextInput
            placeholder="Enter location"
            style={commonStyles.input}
            onChangeText={(event) => setLocation(event)}
          >
            {restaurantLocation}
          </TextInput>
          <Text style={commonStyles.label}>Dietary Requirements</Text>
          <View style={styles.picker}>
            {/* Need to support multiple options? If so, will need different component */}
            <Picker
              mode="dropdown"
              selectedValue={dietaryReqs.selected}
              onValueChange={(itemValue) =>
                setDietaryReqs({ selected: itemValue })}
            >
              <Picker.Item label="None" value="none" />
              <Picker.Item label="Gluten Free" value="GlutenFree" />
              <Picker.Item label="Vegetarian" value="Vegetarian" />
              <Picker.Item label="Vegan" value="Vegan" />
            </Picker>
          </View>
          <Text style={commonStyles.label}>Menu Item</Text>
          <TextInput
            placeholder="Enter menu item"
            style={commonStyles.input}
            onChangeText={(event) => setMenuItem(event)}
          >
            {menuItem}
          </TextInput>

        </View>
      </ScrollView>

      <View style={commonStyles.buttonView}>
        <Button rounded style={commonStyles.button} onPress={handleSearch}>
          <Text style={commonStyles.buttonText}>SUBMIT</Text>
        </Button>
      </View>

    </View>
  );
};

const mapDispatchToProps: DispatchProps = {
  setSearchResults
};

export default connect<DispatchProps>(null, mapDispatchToProps)(SearchPage);
