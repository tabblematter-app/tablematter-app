import { StyleSheet } from 'react-native';
import { p1 } from '../utils/themes/lightTheme';

export const styles = StyleSheet.create({
  price: {
    paddingTop: 5,
    right: '5%',
    textAlign: 'center',
  },
  total: {
    fontWeight: 'bold',
  },
  category: {
    fontSize: 24,
    fontWeight: 'bold',
    textAlign: 'center',
    width: '100%',
  },
  tick: {
    marginVertical: 40,
    borderWidth: 1,
    borderColor: p1,
    textAlign: 'center',
    alignItems: 'center',
    justifyContent: 'center',
    width: 200,
    height: 200,
    backgroundColor: p1,
    borderRadius: 100,
  },
  checkmark: {
    fontSize: 100,
    color: "white"
  },
  listHeader: {
    backgroundColor: '#FFF'
  },
});
