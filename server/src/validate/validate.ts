import { Document, Model } from "mongoose";

export function validateAll(model: Model<Document>, name: string) {
    model
        .find()
        .then((dbEntries) => {
            let error = false;
            dbEntries.forEach((entry) => {
                const err = entry.validateSync();
                if (err) {
                    console.log(err.message);
                    error = true;
                }
            });
            !error && console.log("Successfully Validated", name);
        })
        .catch((err) => {
            console.log("Some error has occured during validation: ", err);
            process.exit(1);
        });
}
