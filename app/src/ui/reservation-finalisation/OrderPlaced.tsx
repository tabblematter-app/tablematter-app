import React from 'react';
import { Platform, Share } from "react-native";
import { Header, Body, Title, View, Left, Right, Text, List, ListItem, Icon } from "native-base";
import { ScrollView, TouchableOpacity } from 'react-native-gesture-handler';
import { connect } from 'react-redux';
import { styles } from "./summary-styles";
import MenuItemDisplay from '../main-app/restaurants/menu/MenuItem';
import { TableMatterState, NewReservationState, resetReservation } from '../../redux';
import { p1 } from '../utils/themes/lightTheme';
import { commonStyles } from '../common/commonStyles';
import { DrawerNavigationProp } from '@react-navigation/drawer';
import { DrawerParamList } from '../utils/drawer';
import { useRoute, RouteProp } from '@react-navigation/native';
import { Restaurant } from '../../redux/types/serverTypes';
import { defaultRestaurant } from '../main-app/restaurants/RestaurantPage';
import { RootStackParamList } from '../utils/rootStack';
import { BackToHomeButton } from '../common/BackToHome';

type orderPlacedScreenRoute = RouteProp<RootStackParamList, "OrderPlaced">;


interface StateProps {
  newReservation: NewReservationState;
  restaurant: Restaurant;
}

type ProfileScreenNavigationProp = DrawerNavigationProp<
  DrawerParamList
>;

interface OwnProps {
  navigation: ProfileScreenNavigationProp; // Alternatively any
}

interface DispatchProps {
  resetReservation: () => void;
}

type Props = StateProps & OwnProps & DispatchProps;

const OrderPlaced = (props: Props) => {
  const route = useRoute() as orderPlacedScreenRoute;
  const orderId = route.params?.orderId || "";
  const dineIn = route.params?.dineIn || false;

  const [reservation, setReservation] = React.useState<NewReservationState>(props.newReservation);

  React.useEffect(() => {
    setReservation(props.newReservation);
    props.resetReservation();
  }, [orderId]);

  // TODO Add proper share in future (web link with order id etc)
  const shareReservation = async () => {
    try {
      const result = await Share.share({
        message: `Booking for ${reservation.date}`,
        url: `example.com/${orderId}`
      });
      if (result.action === Share.sharedAction) {
        if (result.activityType) {
          // shared with activity type of result.activityType
        } else {
          // shared
        }
      } else if (result.action === Share.dismissedAction) {
        // dismissed
      }
    } catch (error) {
      console.warn(error.message);
    }
  };

  return (
    <View style={commonStyles.container}>
      <Header style={commonStyles.header}>
        <Left style={commonStyles.flex1}>
          <BackToHomeButton />
        </Left>
        <Body style={commonStyles.body}>
          <Title style={commonStyles.title}>Order Placed</Title>
        </Body>
        <Right style={commonStyles.flex1}>
          {dineIn &&
            // <Icon name={"share"} size={30} color="white" onPress={() => shareReservation()} />
            <Icon name="share" style={{ color: "white" }} onPress={() => shareReservation()} />
          }
        </Right>
      </Header>
      <ScrollView style={{ marginBottom: Platform.OS === "ios" ? 150 : 70 }}>
        <Text style={{ width: "100%", textAlign: "center", padding: 10, fontWeight: "bold", marginTop: 30 }}>Order Placed!</Text>
        <Text style={{ width: "100%", textAlign: "center", padding: 10 }}>{orderId}</Text>
        <View style={{ width: "100%", alignItems: "center" }}>
          <TouchableOpacity style={styles.tick}>
            <Icon name={"checkmark"} style={styles.checkmark} />
          </TouchableOpacity>
        </View>
        {/* {dineIn &&
            <>
              <View style={{flexDirection: 'row'}}>
                <View style={{flex: 1}}>
                  <View style={{flexDirection: 'column'}}>
                    <View style={{flex: 1, flexDirection: 'row'}}>
                      <IconB name="calendar" style={styles.smallIcons} size={18} />
                      <View style={{flexDirection: 'column'}}>
                        <Text style={styles.datenPartyText}> Date</Text>
                        <Text style={styles.datenPartyDetails}>
                          Date {props.date}
                        </Text>
                      </View>
                    </View>
                    <View style={{flex: 1}}>
                      <View style={{flexDirection: 'column'}}>
                        <View style={{flex: 1, flexDirection: 'row'}}>
                          <IconC
                            name="people"
                            style={styles.smallIconsPartynTable}
                            size={22}
                          />
                          <View style={{flexDirection: 'column'}}>
                            <Text style={styles.datenPartyText}> Party</Text>
                            <Text style={styles.datenPartyDetails}>
                              X people{' '}
                            </Text>
                          </View>
                        </View>
                        <View style={{flexDirection: 'column'}}>
                          <View style={{flex: 1}}></View>
                        </View>
                      </View>
                    </View>
                  </View>
                </View>
                <View style={{flex: 1}}>
                  <View style={{flexDirection: 'column'}}>
                    <View style={{flex: 1, flexDirection: 'row'}}>
                      <IconA name="clockcircle" style={styles.smallIcons} size={20} />
                      <View style={{flexDirection: 'column'}}>
                        <Text style={styles.datenPartyText}> Time</Text>
                        <Text style={styles.datenPartyDetails}>
                        </Text>
                      </View>
                    </View>
                    <View style={{flex: 1, flexDirection: 'row'}}>
                      <IconD
                        name="table"
                        style={styles.smallIconsPartynTable}
                        size={25}
                      />
                      <View style={{flexDirection: 'column'}}>
                        <Text style={styles.timenSeatText}>Table Number</Text>
                        <Text style={styles.timenSeatDetails}>
                          Table ID
                        </Text>
                      </View>
                    </View>
                  </View>
                </View>
              </View>
              <View style={{flex: 1, flexDirection: 'row'}}>
                <IconC
                  name="location-on"
                  style={styles.smallIconsAddress}
                  size={20}
                />
                <View style={{flexDirection: 'column'}}>
                  <Text style={styles.AddressText}> Address</Text>
                  <Text style={styles.AddressDetails}>
                    {props.restaurant.addressLine1}
                  </Text>
                </View>
              </View>
            </>
            } */}

        <List>
          <ListItem itemDivider itemHeader style={{ backgroundColor: "white" }}>
            <Text style={styles.category}>Order</Text>
          </ListItem>
          {reservation.orders.map((item, key) => <MenuItemDisplay key={key} menuItem={item} orderSummary orderPlaced />)}
          <View style={{ borderBottomColor: p1, borderBottomWidth: 1, }} />
          <ListItem>
            <Body>
              <Text style={styles.total}>Total Cost:</Text>
            </Body>
            <Right style={{ alignContent: "center" }}>
              <Text style={[styles.price, styles.total]}>${reservation.totalPrice.toFixed(2)}</Text>
            </Right>
          </ListItem>
        </List>
      </ScrollView>
    </View>
  );
};

const mapStateToProps = (state: TableMatterState): StateProps => {
  return {
    newReservation: state.newReservation,
    restaurant: state.selectedRestaurant || defaultRestaurant,
  };
};

const mapDispatchToProps: DispatchProps = {
  resetReservation,
};

// IDK why error appears it doesnt affect it from working (Add this on components until it is fixed)
// @ts-ignore 
export default connect<StateProps, DispatchProps>(mapStateToProps, mapDispatchToProps)(OrderPlaced);
