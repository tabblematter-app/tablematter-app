import React from "react";
import Icon from "react-native-vector-icons/MaterialCommunityIcons";
import { View, Text } from "react-native";
import { Button } from "native-base";
import { styles } from "./LandingPageStyles";
import { commonStyles } from "../common/commonStyles";
import { StackNavigationProp } from "@react-navigation/stack";
import { RootStackParamList } from "../utils/rootStack";
import { Col } from "react-native-easy-grid";

type ProfileScreenNavigationProp = StackNavigationProp<
  RootStackParamList,
  'Landing'
>;

interface Props {
  navigation: ProfileScreenNavigationProp; // Alternatively any
}

const LandingPage = (props: Props) => {

  return (
    <View style={styles.page}>
      <Text testID="title" style={styles.title}>TABLE MATTER</Text>
      <View style={styles.logo}>
        <Icon name={"silverware-fork-knife"} size={100} color="#01a699" />
      </View>

      <View style={[commonStyles.buttonRow, { marginTop: 50 }]}>
        <Col style={commonStyles.leftButton}>
          <Button testID="log-in" dark style={styles.button} onPress={() => props.navigation.navigate("Login")}>
            <Text style={styles.text} >LOG IN</Text>
          </Button>
        </Col>
        <Col style={commonStyles.rightButton}>
          <Button testID="new-account" dark style={styles.button} onPress={() => props.navigation.navigate("SignUpPage")}>
            <Text style={styles.text}>SIGN UP</Text>
          </Button>
        </Col>
      </View>
    </View>
  );
};

export default LandingPage;