import { StyleSheet } from "react-native";

export const styles = StyleSheet.create({
  thanks: {
    fontWeight: "bold",
    fontSize: 30,
    marginBottom: 10
  },
  messageContainer: {
    paddingTop: 20,
    flex: 1,
    justifyContent: "center",
    alignItems: "center"
  }
});