import { StyleSheet } from "react-native";
import { bgColor, p1 } from "../../utils/themes/lightTheme";

export const styles = StyleSheet.create({
    container: {
      flex: 1,
      backgroundColor: bgColor,
    },
    header: {
        backgroundColor: p1,
    },
    title: {
        color: "white",
        textAlign: "center",
    },
    navButton: {
        marginLeft: 5,
    }
  });