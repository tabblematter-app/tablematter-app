import React from 'react';
import { Platform } from "react-native";
import { Header, Body, Title, View, Left, Right, Tabs, Tab } from "native-base";
import { BackButton } from '../../../common/BackButton';
import { commonStyles } from '../../../common/commonStyles';
import Visa from './Visa';
import { AmazonPay } from './AmazonPay';
import Paypal from './Paypal';
import { RouteProp, useRoute } from '@react-navigation/native';
import { DrawerParamList } from '../../../utils/drawer';
import { styles } from './PayNowStyles';

type payNowScreenRoute = RouteProp<DrawerParamList, "PayNowPage">;


export const PayNowPage = () => {
  const route = useRoute() as payNowScreenRoute;
  const dineIn = route.params?.dineIn || false;

  return (
    <View>
      <Header style={commonStyles.header}>
        <Left style={commonStyles.flex1}>
          <BackButton />
        </Left>
        <Body style={commonStyles.body}>
          <Title style={commonStyles.title}>Pay Now</Title>
        </Body>
        <Right style={commonStyles.flex1} />
      </Header>
      <View style={{ height: "100%", marginBottom: Platform.OS === "ios" ? 150 : 70 }}>
        <Tabs tabBarUnderlineStyle={styles.tabIndicator}>
          <Tab heading="Visa" tabStyle={styles.tab} activeTextStyle={styles.tabLabel} textStyle={styles.tabLabel} activeTabStyle={styles.tab}>
            <Visa dineIn={dineIn} />
          </Tab>
          <Tab heading="Amazon" tabStyle={styles.tab} activeTextStyle={styles.tabLabel} textStyle={styles.tabLabel} activeTabStyle={styles.tab}>
            <AmazonPay />
          </Tab>
          <Tab heading="Paypal" tabStyle={styles.tab} activeTextStyle={styles.tabLabel} textStyle={styles.tabLabel} activeTabStyle={styles.tab}>
            <Paypal dineIn={dineIn} />
          </Tab>
        </Tabs>
      </View>
    </View >
  );
};
