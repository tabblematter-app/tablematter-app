import {createDrawerNavigator} from '@react-navigation/drawer';

export type DrawerParamList = {
  Home: undefined;
  SignUp: undefined;
  Profile: undefined;
  Search: undefined;
  Orders: undefined;
  Settings: undefined;
  Help: {sort: 'latest' | 'top'} | undefined;
  LogOutPage: undefined;
};

export const Drawer = createDrawerNavigator();
