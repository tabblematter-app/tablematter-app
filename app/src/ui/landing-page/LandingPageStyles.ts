import { StyleSheet } from "react-native";
import { p1, bgColor } from "../utils/themes/lightTheme";

export const styles = StyleSheet.create({
  page: {
    backgroundColor: p1,
    flex: 1,
    alignItems: 'center',
  },
  title: {
    marginTop: 90,
    paddingVertical: 8,
    borderRadius: 6,
    color: "white",
    textAlign: "center",
    fontSize: 30,
    fontWeight: "bold"
  },
  logo: {
    marginTop: 40,
    // borderWidth: 1,
    // borderColor: 'rgba(0,0,0,0.2)',
    textAlign: "center",
    alignItems: 'center',
    justifyContent: 'center',
    width: 300,
    height: 300,
    backgroundColor: '#fff',
    borderRadius: 150,
  },
  text: {
    textAlign: "center",
    color: p1,
    width: "100%",
  },
  button: {
    // width: "80%",
    // marginBottom: 20,
    backgroundColor: bgColor,
    textAlign: "center",
    borderRadius: 30,
  }
});