import React from 'react'; //Removed useState for now until process hookup
import {
  Text,
  TouchableOpacity,
  View,
  ScrollView,
  TextInput,
} from 'react-native';
//Removed DatePickerIOS and Modal from react-native to resolve lint errors
import { styles } from './ShareBookingStyles';
import { Header, Body, Left, Right } from 'native-base';
import { BackButton } from '../../common/BackButton';
import { Restaurant } from '../../../redux/types/serverTypes';
import { TableMatterState, setSelectedRestaurant } from '../../../redux';
import { connect } from 'react-redux';
import { useNavigation } from '@react-navigation/native';
import { defaultRestaurant } from './RestaurantPage';
import { commonStyles } from '../../common/commonStyles';
// import IconA from 'react-native-vector-icons/AntDesign';
// import IconB from 'react-native-vector-icons/FontAwesome5';
// import IconC from 'react-native-vector-icons/MaterialIcons';
// import IconD from 'react-native-vector-icons/MaterialCommunityIcons';
//import Share from 'react-native-share';

interface StateProps {
  restaurant: Restaurant;
  name: string;
}
interface DispatchProps {
  setSelectedRestaurant: (selectedRestaurant: Restaurant | null) => void;
}

type Props = DispatchProps & StateProps;

const ShareBookingPage = (props: Props) => {
  const navigation = useNavigation();

  return (
    <View style={commonStyles.container}>
      <Header style={commonStyles.header}>
        <Left style={commonStyles.flex1}>
          <BackButton onPress={() => props.setSelectedRestaurant(null)} />
        </Left>
        <Body style={commonStyles.body}>
          <Text testID="welcome-title" style={commonStyles.title}>
            Share Booking
          </Text>
        </Body>
        <Right style={commonStyles.flex1}></Right>
      </Header>
      <ScrollView style={styles.scrollView}>
        <View>
          <Text style={styles.shareBooking}>Share your booking</Text>

          <Text style={styles.social}>WhatsApp</Text>

          <Text style={styles.social}>Messenger</Text>
          <Text style={styles.social}>SMS</Text>
          <Text style={styles.social}>Email</Text>
        </View>
        <View style={{ flex: 1 }}>
          <Text style={styles.Comments}>Comments</Text>
          <TextInput
            multiline={true}
            numberOfLines={9}
            style={[styles.commentsInput]}
            placeholder="Please enter your comment"
            placeholderTextColor="red"
          />
        </View>
        <View style={styles.shareContainer}>
          <TouchableOpacity
            style={styles.button}
            onPress={() => navigation.navigate('Menu', { reservation: true })}>
            <Text style={styles.buttonText}>SHARE</Text>
          </TouchableOpacity>
        </View>
      </ScrollView>
    </View>
  );
};

const mapStateToProps = (state: TableMatterState): StateProps => {
  return {
    restaurant: state.selectedRestaurant || defaultRestaurant,
    name: state.user.firstName!,
  };
};
interface DispatchProps {
  setSelectedRestaurant: (selectedRestaurant: Restaurant | null) => void;
}
const mapDispatchToProps: DispatchProps = {
  setSelectedRestaurant,
};

// IDK why error appears it doesnt affect it from working (Add this on components until it is fixed)
// @ts-ignore
export default connect<StateProps, DispatchProps>(
  mapStateToProps,
  mapDispatchToProps,
)(ShareBookingPage);
