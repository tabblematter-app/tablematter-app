import * as mongoose from "mongoose";
import { Staff } from "../types/types";

export const staffSchema = new mongoose.Schema({
    // People should have separate accounts for staff and customer
    firstName: String,
    lastName: String,
    password: String,
    email: String,
    phone: String,
});

module.exports = mongoose.model<Staff & mongoose.Document>('Staff', staffSchema);