import React, { useState } from 'react';
import { Text, TouchableOpacity, View, Image, ScrollView } from 'react-native';
import { styles } from './restaurantsStyles';
import { Header, Body, Left, Right, Button } from 'native-base';
import { BackButton } from '../../common/BackButton';
import { Restaurant, Review } from '../../../redux/types/serverTypes';
import {
  TableMatterState,
  setSelectedRestaurant,
  setDineIn,
  resetReservation,
} from '../../../redux';
import { connect } from 'react-redux';
import Icon from 'react-native-vector-icons/MaterialCommunityIcons';
import { useNavigation } from '@react-navigation/native';
import { commonStyles } from '../../common/commonStyles';
import { StackNavigationProp } from '@react-navigation/stack';
import { DrawerParamList } from '../../utils/drawer';
import { SlideBarChart } from 'react-native-slide-charts';
import RestaurantReviewBlob from './RestaurantReviewBlob';
import API from '../../utils/api';
import { Col } from 'react-native-easy-grid';
//import {color} from 'react-native-reanimated';

// TODO: Replace this in the future

export const defaultRestaurant: Restaurant = {
  _id: '',
  name: 'Error: No Restaurant',
  addressLine1: 'placeholder',
  addressLine2: 'placeholder',
  desc: 'placeholder',
  phone: '1234 567 890',
  openHours: [],
  email: 'placeholder',
  website: 'placeholder',
  suburb: 'placeholder',
  images: [],
  state: 'placeholder',
  menu: [],
  tables: [],
  staff: [],
  averageRating: 0
};

type ProfileScreenNavigationProp = StackNavigationProp<DrawerParamList>;

interface OwnProps {
  navigation: ProfileScreenNavigationProp; // Alternatively any
}

interface StateProps {
  restaurant: Restaurant;
}

interface DispatchProps {
  setSelectedRestaurant: (selectedRestaurant: Restaurant | null) => void;
  setDineIn: (dineIn: boolean) => void;
  resetReservation: () => void;
}
type Props = StateProps & DispatchProps & OwnProps;

const RestaurantPage = (props: Props) => {
  const navigation = useNavigation();

  const handleClose = () => {
    props.resetReservation();
    props.setSelectedRestaurant(null);
  };

  const [currentDay, setCurrentDay] = useState(0);
  const [reviews, setReviews] = useState<Review[]>([]);

  const weekDays = [
    { id: 1, name: 'Mon' },
    { id: 2, name: 'Tue' },
    { id: 3, name: 'Wed' },
    { id: 4, name: 'Thur' },
    { id: 5, name: 'Fri' },
    { id: 6, name: 'Sat' },
    { id: 7, name: 'Sun' },
  ];
  const data = [
    [
      { x: 3, y: 3 },
      { x: 1, y: 1 },
      { x: 5, y: 5 },
      { x: 2, y: 2 },
      { x: 4, y: 4 },
      { x: 6, y: 6 },
      { x: 7, y: 7 },
      { x: 2, y: 2 },
      { x: 3, y: 3 },
      { x: 5, y: 5 },
    ],
    [
      { x: 1, y: 1 },
      { x: 1, y: 1 },
      { x: 3, y: 3 },
      { x: 2, y: 2 },
      { x: 4, y: 4 },
      { x: 6, y: 4 },
      { x: 6, y: 6 },
      { x: 1, y: 1 },
      { x: 3, y: 3 },
      { x: 2, y: 2 },
    ],
    [
      { x: 3, y: 3 },
      { x: 4, y: 4 },
      { x: 5, y: 1 },
      { x: 2, y: 2 },
      { x: 2, y: 2 },
      { x: 6, y: 6 },
      { x: 7, y: 7 },
      { x: 2, y: 7 },
      { x: 6, y: 6 },
      { x: 5, y: 5 },
    ],
    [
      { x: 2, y: 2 },
      { x: 1, y: 1 },
      { x: 3, y: 3 },
      { x: 3, y: 3 },
      { x: 4, y: 4 },
      { x: 5, y: 5 },
      { x: 4, y: 4 },
      { x: 4, y: 4 },
      { x: 3, y: 3 },
      { x: 7, y: 7 },
    ],
    [
      { x: 3, y: 3 },
      { x: 4, y: 4 },
      { x: 7, y: 7 },
      { x: 2, y: 2 },
      { x: 4, y: 4 },
      { x: 6, y: 6 },
      { x: 7, y: 7 },
      { x: 2, y: 2 },
      { x: 3, y: 3 },
      { x: 5, y: 5 },
    ],
    [
      { x: 2, y: 2 },
      { x: 2, y: 2 },
      { x: 3, y: 3 },
      { x: 2, y: 2 },
      { x: 4, y: 4 },
      { x: 5, y: 5 },
      { x: 5, y: 5 },
      { x: 7, y: 7 },
      { x: 7, y: 7 },
      { x: 7, y: 7 },
    ],
    [
      { x: 2, y: 2 },
      { x: 3, y: 3 },
      { x: 4, y: 4 },
      { x: 7, y: 7 },
      { x: 6, y: 6 },
      { x: 6, y: 6 },
      { x: 7, y: 7 },
      { x: 2, y: 2 },
      { x: 1, y: 1 },
      { x: 1, y: 1 },
    ],
  ];

  React.useEffect(() => {
    API.get(`/restaurantReviews?restaurantId=${props.restaurant._id}`)
      .then((result) => {
        if (result.data.status) {
          setReviews(result.data.data);
        } else {
          setReviews([]);
        }
      })
      .catch((err) => {
        console.log(err);
      });
  }, []);

  const returnCrowd = (people) => {
    if (people < 4) return 'Not busy: Up to 5 minutes wait';
    if (people >= 4 && people < 6) return 'Usually Busy: Up to 10 minutes wait';
    if (people >= 6) return 'Very busy: Up to 30 minutes wait';
    return 'Not busy';
  };

  return (
    <View style={commonStyles.container}>
      <Header style={commonStyles.header}>
        <Left style={commonStyles.flex1}>
          <BackButton onPress={() => handleClose()} />
        </Left>
        <Body style={commonStyles.body}>
          <Text testID="restaurant-title" style={commonStyles.title}>
            {props.restaurant.name}
          </Text>
        </Body>
        <Right style={commonStyles.flex1}>
          <Icon
            name={'silverware-fork-knife'}
            size={30}
            color="white"
            onPress={() => navigation.navigate('Menu', { reservation: false })}
          />
        </Right>
      </Header>

      {/* styles.scrollView */}
      <ScrollView style={commonStyles.flex1}>
        {/* Restaurant Image */}
        <View style={styles.restImage}>
          {/* TODO: Add images to server from restaurant perspective */}
          <Image
            style={styles.image}
            source={require('../../../../Assets/PlaceholderGraphic.jpg')}
          />
        </View>

        <View style={commonStyles.content}>
          <View>
            <Text style={styles.titletext}>{props.restaurant.name}</Text>
            <View
              style={{
                flex: 1,
                flexDirection: 'row',
                justifyContent: 'space-between',
              }}>
              <Text style={styles.addrtext}>{props.restaurant.addressLine1}</Text>
              {/* <Text style={styles.cashmoney}>$$$</Text> */}
            </View>
            <Text style={styles.textDescription}>{props.restaurant.desc}</Text>
            <View style={{ flex: 1 }}>
              <Text style={commonStyles.label}>Popular Times</Text>
              <View
                style={{ flex: 1, flexDirection: 'row', justifyContent: 'center' }}>
                {weekDays.map((item, index) => {
                  return (
                    <TouchableOpacity
                      key={index}
                      style={{
                        ...styles.daysButton,
                        backgroundColor:
                          currentDay === index ? "#D93636" : 'transparent',
                        // borderColor: currentDay === index ? '#500707' : '#F71818',
                      }}
                      onPress={() => {
                        setCurrentDay(index);
                      }}>
                      <Text style={{
                        ...styles.daysText,
                        color: currentDay === index ? "#FFF" : "#D93636"
                      }}>{item.name}</Text>
                    </TouchableOpacity>
                  );
                })}
              </View>
              <View
                style={{
                  marginTop: 30,
                  flex: 1,
                  alignSelf: 'center'
                }}>
                <SlideBarChart
                  // scrollable
                  shouldCancelWhenOutside={false}
                  alwaysShowIndicator={true}
                  data={data[currentDay]}
                  axisWidth={0}
                  axisHeight={16}
                  barSelectedColor="#500707"
                  fillColor="#BD1D1D"
                  yAxisProps={{
                    hideMarkers: true,
                    showAverageLine: true,
                  }}
                  xAxisProps={{
                    axisMarkerLabels: [
                      '11am',
                      '12pm',
                      '1pm',
                      '2pm',
                      '3pm',
                      '4pm',
                      '5pm',
                      '6pm',
                      '7pm',
                      '8pm',
                    ],
                  }}
                  toolTipProps={{
                    toolTipTextRenderers: [
                      ({ scaleY, y }) => ({
                        text: returnCrowd(scaleY.invert(y).toFixed(1).toString()),
                      }),
                    ],
                  }}
                />
              </View>
            </View>
          </View>
        </View>

        <View style={[commonStyles.buttonRow, { marginVertical: 20 }]}>
          <Col style={commonStyles.leftButton}>
            <Button
              rounded
              style={commonStyles.button}
              onPress={() => {
                props.setDineIn(false);
                navigation.navigate('Menu', {
                  reservation: true,
                  dineIn: false,
                });
              }}>
              <Text style={commonStyles.buttonText}>TAKEAWAY</Text>
            </Button>
          </Col>
          <Col style={commonStyles.rightButton}>
            <Button
              rounded
              style={commonStyles.button}
              onPress={() => {
                props.setDineIn(true);
                navigation.navigate('DineInPage');
              }}>
              <Text style={commonStyles.buttonText}>DINE IN</Text>
            </Button>
          </Col>
        </View>

        {reviews.length > 0 && (
          <View>
            <Header style={commonStyles.header}>
              <Body>
                <Text testID="reviews-title" style={styles.reviewsTitle}>
                  Reviews
                    </Text>
              </Body>
            </Header>
            <View>
              {reviews.map((review, key) => <RestaurantReviewBlob key={`review-${key}`} reviewerName={review.reviewerName} rating={review.rating} content={review.content} modified={review.modified} />)}
            </View>
          </View>
        )}
      </ScrollView>
    </View>
  );
};

const mapStateToProps = (state: TableMatterState): StateProps => {
  return {
    restaurant: state.selectedRestaurant || defaultRestaurant,
  };
};

const mapDispatchToProps: DispatchProps = {
  setSelectedRestaurant,
  setDineIn,
  resetReservation,
};

// IDK why error appears it doesnt affect it from working (Add this on components until it is fixed)
// @ts-ignore
export default connect<StateProps, DispatchProps>(mapStateToProps, mapDispatchToProps)(RestaurantPage);
