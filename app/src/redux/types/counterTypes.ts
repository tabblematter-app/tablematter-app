
export interface CounterState {
    value: number;
}


export const SET_VALUE_ACTION = "SET_VALUE_ACTION";
export interface AddValueAction {
    type: typeof SET_VALUE_ACTION;
    value: number;
}

export const RESET_VALUE_ACTION = "RESET_VALUE_ACTION";
export interface ResetValueAction {
    type: typeof RESET_VALUE_ACTION;
}


export type CounterActions =
    AddValueAction |
    ResetValueAction;

    // add | between action types