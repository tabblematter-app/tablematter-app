# TableMatter - REST API & Mobile Application
A Mobile application + REST API that runs on both Android and ios platforms.

Developed using React Native.

## Requirements
* Node.js - v12 - https://nodejs.org/en/
* Docker - https://www.docker.com/
* MongoDB - https://www.mongodb.com/
* Visual Studio Code - https://code.visualstudio.com/
   - PlantUML - https://marketplace.visualstudio.com/items?itemName=jebbs.plantuml
   - ESLint - https://marketplace.visualstudio.com/items?itemName=dbaeumer.vscode-eslint
   - JSHint - https://marketplace.visualstudio.com/items?itemName=dbaeumer.jshint

## Primary Dependencies
* Node.js - https://nodejs.org/en/
* React Native - https://reactnative.dev/
* Redux - https://redux.js.org/
* React Redux - https://react-redux.js.org/ (Maintained by Redux)
* Lerna - https://lerna.js.org/
* TODO (select Unit test framework, select e2e framework)

## Development Environment
* Download node v12 - https://nodejs.org/en/
* Download androuid studio - https://developer.android.com/studio/
* Download the most recent version of mongodb - https://docs.mongodb.com/manual/tutorial/install-mongodb-on-windows/
* Pull code from this repo

Ensure that you have JDK 8 installed (find zip in google drive)

Note: for android studio ensure you download sdk v29 as well as follow the instruction below to setup emulator
Also install (Android 9.0) (Pie)
* https://developer.android.com/studio/run/emulator

Add the following to your environment variables 
* path: C:\Users\{user-name}\AppData\Local\Android\Sdk\platform-tools\adb.exe (if on PC)
* path: C:\Program Files\MongoDB\Server\4.2\bin (allows for mongo in CLI)
* ANDROID_STUDIO:  C:\Users\{user-name}\AppData\Local\Android\Sdk

## MongoDb
NOTE: ONLY RUN THIS ONCE UNLESS YOU DELETE THE Drop the DB inbetween
To drop the DB
```bash
$ mongo
> use tablematter
> db.dropDatabase()
```
Create Database with dummy data
```bash
$ mongo
> use tablematter
> exit
$  mongoimport --db=tablematter --collection=customer --file=dummy-server-info/customer.json --drop
$  mongoimport --db=tablematter --collection=menuItems --file=dummy-server-info/menuItem.json --drop
$  mongoimport --db=tablematter --collection=restaurants --file=dummy-server-info/restaurant.json --drop
$  mongoimport --db=tablematter --collection=reservations --file=dummy-server-info/reservation.json --drop
$  mongoimport --db=tablematter --collection=counter --file=dummy-server-info/counter.json --drop
```

## Docker containers
Not added yet


## Server 
```bash
$ cd ./server
$ npm install
$ npm run build
$ npm run start
```

## table-matter-app
```bash
 $ cd ./app
 $ npm install
 $ react-native run-android
```