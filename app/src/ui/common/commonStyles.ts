import { StyleSheet, Platform } from 'react-native';
import { bgColor, p1 } from '../utils/themes/lightTheme';

export const commonStyles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: bgColor,
  },
  content: {
    width: "90%",
    alignSelf: 'center',
  },
  header: {
    backgroundColor: p1,
  },
  flex1: {
    flex: 1,
  },
  scrollViewFlex: {
    flex: 1,
  },
  body: {
    flex: 5,
    // flex: Platform.OS === "ios" ? 2 : 1
    alignItems: 'center',
  },
  // Text in header
  title: {
    color: 'white',
    fontSize: 20,
    textAlign: 'center',
  },
  // Containers for action button
  buttonView: {
    width: '80%',
    alignSelf: 'center',
    justifyContent: 'center',
    height: 130,
  },
  adjustedHeight: {
    height: 80,
  },
  stackedButtonsView: {
    width: '80%',
    marginBottom: 35,
    height: 200,
    justifyContent: 'space-between',
  },
  buttonRow: {
    flexDirection: 'row',
    width: '90%',
    alignSelf: 'center',
    justifyContent: 'center',
    height: 130,
  },
  leftButton: {
    marginRight: 10,
    justifyContent: 'center',
  },
  rightButton: {
    marginLeft: 10,
    justifyContent: 'center',
  },
  button: {
    backgroundColor: p1,
    height: 60,
  },
  lightButton: {
    backgroundColor: bgColor,
    height: 60,
  },
  buttonText: {
    fontSize: 16,
    textAlign: 'center',
    color: bgColor,
    width: '100%',
    textTransform: "uppercase"
  },
  lightButtonText: {
    fontSize: 20,
    textAlign: 'center',
    color: p1,
    width: '100%',
  },
  // Container for search inputs
  inputView: {
    width: '80%',
    alignSelf: 'center',
    flex: 1,
  },
  // Title for input
  label: {
    marginVertical: 15,
    fontSize: 16,
    fontWeight: 'bold',
    color: p1,
  },
  input: {
    borderWidth: 1,
    height: 48,
    borderColor: p1,
    borderRadius: 30,
    paddingHorizontal: 20,
    width: Platform.OS === 'ios' ? '95%' : '100%',
    fontSize: 14,
  },
  alignVertical: {
    alignItems: 'center',
  },
  cardButton: {
    flex: 1,
    justifyContent: 'center',
  },
  cardButtonText: {
    color: p1,
  },
  amountView: {
    flex: 1,
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
  },
  amountIcon: {
    fontSize: 20,
    color: p1,
  },
  amountText: {
    // marginTop: '2.5%',
    fontSize: 18,
    color: 'black',
    justifyContent: 'center',
    textAlign: 'center',
  },
  noResultsText: {
    textAlign: "center",
    marginTop: 40,
    color: "#AAA"
  },
});
