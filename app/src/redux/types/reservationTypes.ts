import { Party, MenuItem, RestaurantShort } from "./serverTypes";

// TODO: Determine whether more of Reservation type needed in the state
// atm user info gathered at 
export interface NewReservationState {
    restaurant: RestaurantShort;
    tableNumbers: number[];
    orders: MenuItem[];
    dineIn: boolean;
    totalPrice: number;
    party: Party;
    date: string;
    duration: number;
    paid: boolean;
}

export const RESET_RESERVATION_ACTION = "RESET_RESERVATION_ACTION";
export interface ResetReservationAction {
    type: typeof RESET_RESERVATION_ACTION;
}

export const SET_RESERVATION_RESTAURANT_ACTION = "SET_RESERVATION_RESTAURANT_ACTION";
export interface SetReservationRestaurantAction {
    type: typeof SET_RESERVATION_RESTAURANT_ACTION;
    restaurant: RestaurantShort;
}

export const SET_TABLE_NUMBERS_ACTION = "SET_TABLE_NUMBERS_ACTION";
export interface SetTableNumbersAction {
    type: typeof SET_TABLE_NUMBERS_ACTION;
    tableNumbers: number[];
}

export const ADD_MENU_ITEM_ACTION = "ADD_MENU_ITEM_ACTION";
export interface AddMenuItemAction {
    type: typeof ADD_MENU_ITEM_ACTION;
    menuItem: MenuItem;
}

export const SET_DINE_IN_ACTION = "SET_DINE_IN_ACTION";
export interface SetDineInAction {
    type: typeof SET_DINE_IN_ACTION;
    dineIn: boolean;
}

export const SET_PAID_ACTION = "SET_PAID_ACTION";
export interface SetPaidAction {
    type: typeof SET_PAID_ACTION;
    paid: boolean;
}

export const REMOVE_MENU_ITEM_ACTION = "REMOVE_MENU_ITEM_ACTION";
export interface RemoveMenuItemAction {
    type: typeof REMOVE_MENU_ITEM_ACTION;
    menuItem: MenuItem;
    numberToRemove: number;
}

export const SET_PARTY_ACTION = "SET_PARTY_ACTION";
export interface SetPartyAction {
    type: typeof SET_PARTY_ACTION;
    party: Party;
}

export const SET_DATE_ACTION = "SET_DATE_ACTION";
export interface SetDateAction {
    type: typeof SET_DATE_ACTION;
    date: string;
}

export const SET_DURATION_ACTION = "SET_DURATION_ACTION";
export interface SetDurationAction {
    type: typeof SET_DURATION_ACTION;
    duration: number;
}

export type ReservationActionTypes = 
    ResetReservationAction |
    SetReservationRestaurantAction |
    SetTableNumbersAction |
    AddMenuItemAction |
    RemoveMenuItemAction |
    SetPartyAction |
    SetDateAction |
    SetDurationAction |
    SetDineInAction |
    SetPaidAction;
