import * as chai from "chai";
const server=require("../index");
import chaiHttp = require('chai-http');
import { isMainThread } from "worker_threads";
chai.use(chaiHttp);
const should = chai.should();


describe("review", function(){
    describe ("get review", function(){
        it("get all reviews", (done) => {
            chai.request(server)
                .get("/review")
                .send({})
                .end(({},res: any)=>{
                    res && res.should.have.status(200);
                    done();
                });
        });
    });

    describe ("create review", () => {
        it("should create review", (done) => {
            chai.request(server)
                .post("/review", )
                .send({
                    customerId: "5df06124548aa152b5ea6918",
                    restaurantId: "5df06124548aa152b5ea691e",
                    reviewName: "OK",
                    rating: 4,
                    content: ""
                })
                .end(({},res: any)=>{
                    res && res.should.have.status(200);
                    done();
                });
        });
    });

    describe ("edit review", () => {
        it("should edit review information", (done) => {
            chai.request(server)
                .patch("/review", )
                .send({
                    messageId: "noIdCurrently",
                    customerId: "5df06124548aa152b5ea6918",
                    restaurantId: "5df06124548aa152b5ea691e",
                    reviewName: "OK",
                    rating: 4,
                    content: ""
                })
                .end(({},res: any)=>{
                    res && res.should.have.status(200);
                    done();
                });
        });
    });

    describe ("delete review", () => {
        it("delete review", (done) => {
            chai.request(server)
                .delete("/review/noIdCurrently", )
                .send({})
                .end(({},res: any)=>{
                    res && res.should.have.status(200);
                    done();
                });
        });
    });

    describe ("get user reviews", () => {
        it("get all reviews by user", (done) => {
            chai.request(server)
                .get("/review/noIdCurrently", )
                .send({})
                .end(({},res: any)=>{
                    res && res.should.have.status(200);
                    done();
                });
        });

        it("get specific review by user given restaurant", (done) => {
            chai.request(server)
                .get("/review/noIdCurrently/restaurant", )
                .send({})
                .end(({},res: any)=>{
                    res && res.should.have.status(200);
                    done();
                });
        });
    });
});