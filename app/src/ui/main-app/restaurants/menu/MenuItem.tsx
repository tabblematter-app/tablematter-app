import React from 'react';
import { Text, ListItem, Body, Right, Icon } from "native-base";
import { MenuItem } from '../../../../redux/types/serverTypes';
import { styles } from "../restaurantsStyles";
import AddItemModal from './AddItemModal';
import { removeMenuItem } from '../../../../redux';
import { connect } from 'react-redux';
import { TouchableOpacity } from 'react-native';


interface OwnProps {
  menuItem: MenuItem;
  isReservation?: boolean;
  orderSummary?: boolean;
  orderPlaced?: boolean;
}

interface DispatchProps {
  removeMenuItem: (menuItem: MenuItem, numberToRemove: number) => void;
}

type Props = OwnProps & DispatchProps;

const MenuItemDisplay = (props: Props) => {
  const [orderModal, setOrderModal] = React.useState(false);

  return (
    <ListItem noBorder={props.orderSummary} style={{ paddingTop: props.orderSummary ? 0 : 13, paddingBottom: props.orderSummary ? 8 : 13 }}>
      <Body>
        <Text>{props.menuItem.name}</Text>
        {!props.orderSummary && <Text note >{"\n"}{props.menuItem.desc}</Text>}
      </Body>
      <Right style={{ alignContent: "center" }}>
        {props.isReservation &&
          <TouchableOpacity style={styles.addButton} onPress={() => setOrderModal(true)}>
            <Icon name="add" style={styles.addButtonIcon} />
          </TouchableOpacity>
        }
        <Text style={styles.price}>${props.menuItem.price.toFixed(2)}</Text>
      </Right>
      {props.orderSummary && !props.orderPlaced &&
        <Right>
          {/* <Text style={styles.remove} onPress={() => props.removeMenuItem(props.menuItem, 1)}>Remove</Text> */}
          {/* <Button style={styles.iconButton} onPress={() => props.removeMenuItem(props.menuItem, 1)}> */}
          <Icon name="close" style={styles.icon} onPress={() => props.removeMenuItem(props.menuItem, 1)}></Icon>
          {/* </Button> */}
        </Right>
      }
      {orderModal && <AddItemModal isOpen={orderModal} hide={() => setOrderModal(false)} menuItem={props.menuItem} />}
    </ListItem>
  );
};

const mapDispatchToProps: DispatchProps = {
  removeMenuItem,
};

// IDK why error appears it doesnt affect it from working (Add this on components until it is fixed)
// @ts-ignore 
export default connect<{}, DispatchProps>(undefined, mapDispatchToProps)(MenuItemDisplay);
