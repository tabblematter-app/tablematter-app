# Process

## Agile - GitLab mapping
| Agile      | GitLab     | Example                                          |
| ---------- | ---------- | ------------------------------------------------ |
| Epic       | Epics      | Release names                                    |
| Feature    | Issue      | Feature                                          |
| User story | Task lists | As a customer, I am required to                  |
| Sprint     | Milestones |                                                  |

## Sprint planning
 * Required: Developers, Team leader etc
 * Select issues for sprint
 * Estimate size of issues

## Task process
 1. Select features (issues)
 2. Assign it
 3. Write user story (if appropriate)
 4. Write tests (unit, and e2e)
 5. Code until tests pass
 6. Merge request
 7. Demo
 8. Review, merge into develop and close

## End of sprint
 * Demo all features to customer (if they want to frequent reviews)

## Bug Fix process
 1. Create issue outlining bug and impacted component
 2. Assign issue to developer
 3. Open branch for issue (along with merge request)
 4. Code until bug is fixed
 5. Add/Modify tests to ensure working (if required)
 6. Review and merge into develop

## Release process
 * Create list of new fetures
 * Create list of bug fixes
 * Create any additional notes
 * Merge documentation into develop
 * Merge develop into master

## Definition of Done
 * End 2 End test written
 * Code reviewed
 * Pipeline passing
 * Update documentation (If appropriate)
 * Make sure issue is closed
 