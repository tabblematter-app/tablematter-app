import React from 'react';
import { Platform } from "react-native";
import { Header, Body, Title, View, Left, Right, Text, List, ListItem, Button } from "native-base";
import { ScrollView } from 'react-native-gesture-handler';
import { connect } from 'react-redux';
import { styles } from "./summary-styles";
import { Reservation, Restaurant } from '../../redux/types/serverTypes';
import MenuItemDisplay from '../main-app/restaurants/menu/MenuItem';
import { TableMatterState, NewReservationState, UserState, setPaid } from '../../redux';
import { BackButton } from '../common/BackButton';
import { p1 } from '../utils/themes/lightTheme';
import { commonStyles } from '../common/commonStyles';
import { useNavigation } from '@react-navigation/native';
import API from '../utils/api';
import { defaultRestaurant } from '../main-app/restaurants/RestaurantPage';
import { restToRestShort, userToCustShort } from '../utils/type-conversions';
import { Col } from 'react-native-easy-grid';

interface StateProps {
  restaurant: Restaurant;
  customer: UserState;
  newReservation: NewReservationState;
  totalPrice: number;
}

interface DispatchProps {
  setPaid: (paid: boolean) => void;
}

type Props = StateProps & DispatchProps;

const OrderSummary = (props: Props) => {
  const navigation = useNavigation();

  const handleMakeReservation = () => {
    props.setPaid(false);
    const reservation: Reservation = {
      restaurant: restToRestShort(props.restaurant),
      customer: userToCustShort(props.customer),
      orders: props.newReservation.orders,
      dineIn: props.newReservation.dineIn,
      duration: props.newReservation.duration,
      totalPrice: props.newReservation.totalPrice,
      finished: false,
      cancelled: false,
      paid: false,
      tableNumbers: [],
      timeBooked: (new Date()).toString(),
    };

    API.post("/reservation", { reservation })
      .then((data) => {
        console.log(data.data.data._id);
        data.data ? navigation.navigate("OrderPlaced", { orderId: data.data.data._id, dineIn: false }) : navigation.navigate("ErrorPage");
      })
      .catch(() => navigation.navigate("ErrorPage"));
  };

  return (
    <View style={commonStyles.container}>
      <Header style={commonStyles.header}>
        <Left style={commonStyles.flex1}>
          <BackButton />
        </Left>
        <Body style={commonStyles.body}>
          <Title style={commonStyles.title}>Order Summary</Title>
        </Body>
        <Right style={commonStyles.flex1} />
      </Header>
      <ScrollView style={{ marginBottom: Platform.OS === "ios" ? 150 : 70 }}>
        <List>
          <ListItem itemDivider itemHeader style={styles.listHeader}>
            <Text style={styles.category}>Order</Text>
          </ListItem>
          {props.newReservation.orders.map((item, key) => <MenuItemDisplay key={key} menuItem={item} orderSummary />)}
          <View style={{ borderBottomColor: p1, borderBottomWidth: 1, }} />
          <ListItem>
            <Body>
              <Text style={styles.total}>Total Cost:</Text>
            </Body>
            <Right style={{ alignContent: "center" }}>
              <Text style={[styles.price, styles.total]}>${props.totalPrice.toFixed(2)}</Text>
            </Right>
          </ListItem>
        </List>

        <View style={commonStyles.buttonRow}>
          <Col style={commonStyles.leftButton}>
            <Button rounded style={commonStyles.button} onPress={() => handleMakeReservation()}>
              <Text style={commonStyles.buttonText}>PAY IN STORE</Text>
            </Button>
          </Col>
          <Col style={commonStyles.rightButton}>
            <Button rounded style={commonStyles.button} onPress={() => navigation.navigate("PayNowPage", { dineIn: false })}>
              <Text style={commonStyles.buttonText}>PAY NOW</Text>
            </Button>
          </Col>
        </View>


      </ScrollView>
    </View>
  );
};

const mapStateToProps = (state: TableMatterState): StateProps => {
  return {
    customer: state.user!,
    restaurant: state.selectedRestaurant || defaultRestaurant,
    newReservation: state.newReservation,
    totalPrice: state.newReservation.totalPrice,
  };
};

const mapDispatchToProps: DispatchProps = {
  setPaid,
};

// IDK why error appears it doesnt affect it from working (Add this on components until it is fixed)
// @ts-ignore 
export default connect<StateProps, DispatchProps>(mapStateToProps, mapDispatchToProps)(OrderSummary);
