import React from "react";
// import Icon from "react-native-vector-icons/AntDesign";
import { Icon } from "native-base";
import { DrawerNavigationProp } from "@react-navigation/drawer";
import { DrawerParamList } from "../../utils/drawer";

type ProfileScreenNavigationProp = DrawerNavigationProp<
  DrawerParamList
>;

interface Props {
  navigation: ProfileScreenNavigationProp; // Alternatively any
}

export const NavButton = (props: Props) => {
  return (
    <Icon name="menu" testID="nav-button" style={{ color: '#FFF' }} onPress={() => props.navigation.openDrawer()} />
  );
};
