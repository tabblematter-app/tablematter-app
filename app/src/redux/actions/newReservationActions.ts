import { RestaurantShort, MenuItem, Party } from "../types/serverTypes";
import { TableMatterActionTypes, SET_RESERVATION_RESTAURANT_ACTION, SET_TABLE_NUMBERS_ACTION, ADD_MENU_ITEM_ACTION, REMOVE_MENU_ITEM_ACTION, SET_PARTY_ACTION, SET_DURATION_ACTION, SET_DINE_IN_ACTION, SET_PAID_ACTION, RESET_RESERVATION_ACTION, SET_DATE_ACTION } from "../types";

export const resetReservation = (): TableMatterActionTypes => {
    return {
        type: RESET_RESERVATION_ACTION,
    };
};

export const addReservationRestaurant = (restaurant: RestaurantShort): TableMatterActionTypes => {
    return {
        type: SET_RESERVATION_RESTAURANT_ACTION,
        restaurant,
    };
};

export const setTableNumbers = (tableNumbers: number[]): TableMatterActionTypes => {
    return {
        type: SET_TABLE_NUMBERS_ACTION,
        tableNumbers,
    };
};

export const addMenuItem = (menuItem: MenuItem): TableMatterActionTypes => {
    return {
        type: ADD_MENU_ITEM_ACTION,
        menuItem,
    };
};

export const setDineIn = (dineIn: boolean): TableMatterActionTypes => {
    return {
        type: SET_DINE_IN_ACTION,
        dineIn,
    };
};

export const setPaid = (paid: boolean): TableMatterActionTypes => {
    return {
        type: SET_PAID_ACTION,
        paid
    };
};

export const removeMenuItem = (menuItem: MenuItem, numberToRemove: number): TableMatterActionTypes => {
    return {
        type: REMOVE_MENU_ITEM_ACTION,
        menuItem,
        numberToRemove,
    };
};

export const setParty = (party: Party): TableMatterActionTypes => {
    return {
        type: SET_PARTY_ACTION,
        party,
    };
};

export const setDate = (date: string): TableMatterActionTypes => {
    return {
        type: SET_DATE_ACTION,
        date,
    };
};

export const setDuration = (duration: number): TableMatterActionTypes => {
    return {
        type: SET_DURATION_ACTION,
        duration,
    };
};