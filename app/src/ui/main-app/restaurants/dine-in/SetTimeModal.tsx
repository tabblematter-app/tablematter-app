import React from 'react';
import { Card, CardItem, View, Button, Text } from "native-base";
import Modal from "react-native-modal";
import { modalStyles } from "../addModalStyles";

interface OwnProps {
  children: any;
  handleSave: (save: boolean) => void;
}

type Props = OwnProps;

export const SetTimeModal = (props: Props) => {
  return (
    //  style={modalStyles.mainView}
    // <View>
    <Modal isVisible={true}>
      <Card style={{ alignContent: "center" }}>
        <CardItem style={{ alignContent: "center", justifyContent: "center" }}>
          <View style={{ flexDirection: "column" }}>
            <View style={modalStyles.view}>
              {props.children}
            </View>
            <View style={modalStyles.view}>
              <Button style={[modalStyles.cancelButton, modalStyles.submitButton]} onPress={() => props.handleSave(false)}>
                <Text style={modalStyles.submitButtonText}>Cancel</Text>
              </Button>
              <Button style={modalStyles.submitButton} onPress={() => props.handleSave(true)}>
                <Text style={modalStyles.submitButtonText}>Update</Text>
              </Button>
            </View>
          </View>
        </CardItem>
      </Card>
    </Modal>
  );
};