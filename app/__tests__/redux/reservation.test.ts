import reducer from "../../src/redux/reducers/index";
import { SET_RESERVATION_RESTAURANT_ACTION, NewReservationState, SET_TABLE_NUMBERS_ACTION, ADD_MENU_ITEM_ACTION, SET_PARTY_ACTION, SET_DURATION_ACTION, SET_DINE_IN_ACTION, SET_PAID_ACTION, SET_SELECTED_RESERVATION_ACTION } from "../../src/redux";
import { MenuItem, CategoryEnum, Party, Reservation } from "../../src/redux/types/serverTypes";

const defaultNewReservation: NewReservationState = {
    restaurant: {
        id: "",
        name: "",
        addressLine1: "",
        addressLine2: "",
        suburb: "",
        state: "",
        phone: "",
    },
    tableNumbers: [],
    orders: [],
    totalPrice: 0,
    dineIn: true,
    date: "",
    party: {
        size: 0,
        elderCount: 0,
        adultCount: 0,
        childCount: 0,
        disabledCount: 0,
    },
    duration: 0,
    paid: false,
};

describe("TableMatter - New Reservation Reducer", () => {
  it("should add a restaurant", () => {
    expect(reducer(undefined, {
      type: SET_RESERVATION_RESTAURANT_ACTION,
      restaurant: {
        id: "1",
        name: "string",
        addressLine1: "string",
        addressLine2: "string",
        suburb: "string",
        state: "string",
        phone: "string",
      }
    }))
      .toMatchObject({
        newReservation: {
          ...defaultNewReservation,
          restaurant: {
            id: "1",
            name: "string",
            addressLine1: "string",
            addressLine2: "string",
            suburb: "string",
            state: "string",
            phone: "string",
          }
        }
      });
  });

  it("Should add tables", () => {
    expect(reducer(undefined, {
      type: SET_TABLE_NUMBERS_ACTION,
      tableNumbers: [1, 2],
    }))
      .toMatchObject({
        newReservation: {
          ...defaultNewReservation,
          tableNumbers: [1, 2]
        }
      });
  });

  it("Should Set the party", () => {
    const party: Party = {
      size: 1,
      childCount: 1,
      adultCount: 1,
      elderCount: 1,
      disabledCount: 1,
    };

    expect(reducer(undefined, {
      type: SET_PARTY_ACTION,
      party,
    }))
      .toMatchObject({
        newReservation: {
          ...defaultNewReservation,
          party,
        }
      });
  });

  it("Should Set the duration", () => {
    expect(reducer(undefined, {
      type: SET_DURATION_ACTION,
      duration: 2,
    }))
      .toMatchObject({
        newReservation: {
          ...defaultNewReservation,
          duration: 2,
        }
      });
  });

  it("Should Set the dineIn var", () => {
    expect(reducer(undefined, {
      type: SET_DINE_IN_ACTION,
      dineIn: false,
    }))
      .toMatchObject({
        newReservation: {
          ...defaultNewReservation,
          dineIn: false,
        }
      });
  });

  it("Should Set the paid var", () => {
    expect(reducer(undefined, {
      type: SET_PAID_ACTION,
      paid: true,
    }))
      .toMatchObject({
        newReservation: {
          ...defaultNewReservation,
          paid: true,
        }
      });
  });

it("Should add New MenuItem", () => {
    const menuItem: MenuItem = {
        _id: "string", // This will likely need to be debugged after we add front end
        name: "string",
        desc: "string",
        price: 0,
        discount: 0,
        category: CategoryEnum.APPETIZER,
        images: [],
        tags: [],
        allergens: [],
    };

    expect(reducer(undefined, {
      type: ADD_MENU_ITEM_ACTION,
      menuItem,
    }))
      .toMatchObject({
        newReservation: {
          ...defaultNewReservation,
          orders: [menuItem]
        }
      });
  });

  it("Should set the reservation", () => {
    const selectedReservation: Reservation = {
      customer: {
        id: "string",
        firstName: "string",
        lastName: "string",
        phone: "string"
      },
      restaurant: {
        id: "string",
        name: "string",
        addressLine1: "string",
        addressLine2: "string",
        suburb: "string",
        state: "string",
        phone: "string"
      },
      orders: [],
      totalPrice: 0,
      timeBooked: "string",
      finished: false,
      cancelled: false,
      dineIn: true,
      tableNumbers: [],
      party: {
        size: 1,
        adultCount: 1,
        childCount: 0,
        elderCount: 0,
        disabledCount: 0
      },
      duration: 30,
      paid: false
    };

    expect(reducer(undefined, {
      type: SET_SELECTED_RESERVATION_ACTION,
      selectedReservation,
    }))
      .toMatchObject({
        selectedReservation: selectedReservation
      });
  });

  it("Should set the reservation to null", () => {
    const selectedReservation = null;

    expect(reducer(undefined, {
      type: SET_SELECTED_RESERVATION_ACTION,
      selectedReservation,
    }))
      .toMatchObject({
        selectedReservation: null
      });
  });
});