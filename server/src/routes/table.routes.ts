import * as express from "express";
import { Response } from "express";
import { findAll } from "../controllers/model.functions";
import tableModel from "../models/tableModel";

export default (app: express.Express) => {
    app.get("/table", (_, res: Response) => findAll(res, tableModel));
};
