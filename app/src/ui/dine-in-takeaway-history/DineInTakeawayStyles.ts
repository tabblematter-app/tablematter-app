import { StyleSheet } from "react-native";
import { bgColor, p1 } from "../utils/themes/lightTheme";

export const styles = StyleSheet.create({
  // Tab styles
  tabIndicator: {
    backgroundColor: p1
  },
  tab: {
    backgroundColor: bgColor,
  },
  tabLabel: {
    color: p1,
  },
  container: {
    width: "90%",
    alignSelf: "center",
    marginBottom: 20,
  },
  // Card styles
  cardImageContainer: {
    height: 125,
    width: "100%",
    overflow: "hidden",
    flex: 1,
  },
  cardImage: {
    width: "100%",
    height: undefined,
    flex: 1,
  },
  cardImageInner: {
    backgroundColor: "rgba(0, 0, 0, 0.5)",
    flex: 1,
    justifyContent: "center",
    alignItems: "center"
  },
  borderTop: {
    borderTopWidth: 1,
    borderTopColor: '#CCC',
  },
  borderBottom: {
    borderBottomWidth: 1,
    borderBottomColor: '#CCC',
  },
  borderRight: {
    borderRightWidth: 1,
    borderRightColor: p1,
  },
  noHistoryText: {
    textAlign: "center",
    marginTop: 40,
    color: "#AAA"
  },
  orderTotal: {
    justifyContent: "center",
  },
  orderTotalText: {
    fontWeight: "bold",
  },
  icon: {
    color: "#555",
    marginRight: 10,
  },
  col: {
    alignItems: "center",
    flexDirection: "row",
    marginTop: 10,
  }
});