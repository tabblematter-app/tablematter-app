# Code Style (For TableMatter application)

## Naming convention
* All folders will be named using kebab-case
* Files should also be kebab-case with the exception of React component files which will be named using CamelCase

## General rules
* Indentation will be 4 spaces (Standard tab)
* TSlint shall be used for TS linting rules
* ESLint shall be used in the situation that TypeScript cannot be used

### Database Interfaces
Since React-Native does not support symlinks (which is how npm handles local dependencies) we will need to do the following when adding db schemas:
* Add interface type in the types folder within server
* After making the changes copy them to the types folder in the redux section of App
* (I know this is not preferable, but it should only be a temp fix)


## React Native
* As stated above files will be named with CamelCase

### Components utilising Redux
All components using redux will follow the pattern outlines below:

``` typescript
interface OwnProps {
    // Props inherited from parent
}

interface StateProps {
    // Props inherited from redux store
}

interface DispatchProps {
    // Props to edit redux store
}

type Props = OwnPros & StateProps & DispatchProps;


const ExampleComponent = (props: Props) => {
    return <button />;
};

const mapStateToProps = (state: ReduxStoreType): StateProps => {
    return {
        statePropOne: state.CorrespondingState,
    };
};

const mapDispatchToProps: DispatchProps = {
    updateFunction,
};

export default connect<StateProps, DispatchProps, OwnProps>(mapStateToProps, mapDispatchToProps)(ExampleComponent);
```

### Basic component Not using Redux
``` typescript
interface Props {
    // Props inherited from parent component
}

const ExampleComponent = (props: Props) => {
    return <button />;
}
```

## Unit Testing
``` typescript
describe("Reducer Name", () => {
    describe("ADD_ENTRY", () => {
        it("should add a entry", () => {
            const action = addFunction("PAGE");

            const expectedState = {
                stateName: {
                    entry: ["PAGE"],
                },
            };

            expect(reducer(State, action)).toMatchObject(expectedState);
        });
    });
});

```


## E2E Testing
``` typescript
// TODO: Add E2E testing structure when an example exists
```


