const constants = {
    success:{
        login: "Successful Login",
        restaurantsFound: "Found Restaurants",
        userReservations: "Successfully got reservations",
        addReservation: "Successfully made reservation Reservation",
        updateReservation: "Successfully updated Reservation",
        deleteReservation: "Successfully deleted Reservation",
        updatePicture: "Successfully updated Profile Picture",
        addReview: "Successfully added Review",
        updateReview: "Successfully updated Review",
        deleteReview: "Successfully deleted Review",
        getReviewById: "Successfully retrieved Review",
        getUserReviews: "Successfully found user Reviews",
        getReviewsForRestaurant: "Successfully restrieved reviews for Restaurant",
    },
    fail: {
        internalError: "500 Internal Server Error",
        notFoundError: "404 Resource Not Found",
        badRequestError: "400 Bad Request",
    
    
        login: "Invalid Email and/or Password",

        restaurantsNotFound: "No restaurants found with those search parameters",
        userReservations: "Sorry, you have not past reservations",
        addReservation: "Failed to add Reservation",
        updateReservation: "Failed to update Reservation",
        deleteReservation: "Failt to cancel Reservation",

        updatePicture: "Failed to update Profile Picture",
        userExists: "User already exists",

        addReview: "Failed to add Review",
        updateReview: "Failed to update Review",
        deleteReview: "Failed to delete Review",
        getReviewById: "Failed to find Review",
        getUserReviews: "Failed to find user Reviews",
        getReviewsForRestaurant: "Failed to get reviews for Restaurant",
    },
};

export default constants;