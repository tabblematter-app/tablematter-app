import constants from "../utils/constants";
import { Response } from "express";
import helper from "../utils/responseHelper";
import restaurantModel from "../models/restaurantModel";
import { Restaurant, DietaryStatusEnum, Reservation } from "../types/types";
import reservationModel from "../models/reservationModel";


export function findRestaurants(searchString: string | null, restaurantName: string | null, locationString: string | null, menuItem: string | null, filters: string[] | null, res: Response) {
  if (searchString) {
    findWithFuzzySearch(searchString, res);
  } else if (restaurantName || locationString || menuItem || filters) {
    findWithExplicitSearch(restaurantName || "", locationString || "", menuItem || "", filters, res);
  } else {
    findNoSearch(res);
  }
}

function findNoSearch(res: Response) {
  restaurantModel.find().sort({ _id: 1 }).limit(20)
    .then((restaurants) => {
      res.send(helper.successResponse(restaurants, constants.success.restaurantsFound));
    })
    .catch((err) => {
      res.send(helper.failResponse(err, constants.fail.restaurantsNotFound));
    });
}

function findWithFuzzySearch(searchString: string, res: Response) {
  restaurantModel.search(searchString, (err: object, restaurants: Restaurant[]) => {
    if (err) {
      res.send(helper.failResponse(err, constants.fail.restaurantsNotFound));
    } else {
      res.send(helper.successResponse(restaurants, constants.success.restaurantsFound));
    }

  });
}

export function findWithExplicitSearch(restaurantName: string, locationString: string, menuItem: string, filters: string[] | null, res: Response) {
  restaurantModel.find(
    {
      "name": restaurantName === "" ? new RegExp('.*') : new RegExp(`${restaurantName}+`, "i"),
      "addressLine1": locationString === "" ? new RegExp('.*') : new RegExp(`${locationString}+`, "i"),
      "menu.name": menuItem === "" ? new RegExp('.*') : new RegExp(`${menuItem}+`, "i"),
    }).sort({ _id: 1 }).limit(20)
    .then((restaurants) => {
      if (filters && !filters.includes("none")) {
        res.send(helper.successResponse(matchesFoodFilter(restaurants, filters), constants.success.restaurantsFound));
      } else {
        res.send(helper.successResponse(restaurants, constants.success.restaurantsFound));
      }
    }).catch((err) => {
      res.send(helper.failResponse(err, constants.fail.restaurantsNotFound));
    });

  // TODO Add code to get restaurants that match other filters
}

// TODO come up with more efficient function
function matchesFoodFilter(restaurants: Restaurant[], filters: string[]): Restaurant[] {
  const restaurantsToReturn: Restaurant[] = [];

  restaurants.forEach((restaurant) => {
    let included = false;
    filters.forEach((filter) => {
      const hasFilter = restaurant.menu.some((item) => item.tags.includes(filter as DietaryStatusEnum));
      hasFilter && (included = true);
    });
    included && restaurantsToReturn.push({ ...restaurant });
  });

  return restaurantsToReturn;
}

//Gets a list of restaurants that have been previously booked by the user
export const findResturantsBookedByUser = (userId: string, res: Response) => {
  //First check if the userId is valid
  if (!userId || userId.length === 0) {
    return res.send(helper.failResponse(null, constants.fail.badRequestError));
  }

  //Now attempt to get the previous reservations made by the user
  let reservationList: Reservation[] = [];

  reservationModel.find({ 'customer.id': userId })
    .then((reservations: Reservation[]) => {
      if (!reservations || reservations.length === 0) {
        return res.send(helper.failResponse(Error("NoReservations"), constants.fail.userReservations));
      }

      reservationList = reservations;

      //Get the unique reservation ids
      const restaurantIdList: string[] = [];
      reservationList.map((value) => {
        if (!(restaurantIdList.indexOf(value.restaurant.id) >= 0)) {
          restaurantIdList.push(value.restaurant.id);
        }
      });
      
        // //Get the restaurants that match the ids
        restaurantModel.find({'_id': { $in: restaurantIdList }})
          .then((restaurants: Restaurant[]) => {
            if (!restaurants || restaurants.length === 0) {
              return res.send(helper.failResponse(null, "No restaurants found for user reservations"));
            }

            return res.send(helper.successResponse(restaurants, constants.success.restaurantsFound));
          })
          .catch((err) => {
              res.send(helper.failResponse(err, constants.fail.internalError));
          });
    })
    .catch((err) => {
      res.send(helper.failResponse(err, constants.fail.internalError));
    });
};


export const findRestaurantById = async (id: string, res: Response) => {
  restaurantModel.findById(id)
    .then((foundRestaurant: Restaurant) => {
      return res.send(helper.successResponse(foundRestaurant, constants.success.restaurantsFound));
    })
    .catch((err) => {
      return res.send(helper.failResponse(err, "No restaurant found with that id"));
    });
};