import React from 'react';
import { View, Form, Button, Text, Item, Label, Input } from "native-base";
import { commonStyles } from '../../../common/commonStyles';
import { styles } from "./visa-styles";
import { useNavigation } from '@react-navigation/native';
import { TableMatterState, setPaid, UserState, NewReservationState } from '../../../../redux';
import { defaultRestaurant } from '../RestaurantPage';
import { Restaurant, Reservation } from '../../../../redux/types/serverTypes';
import { restToRestShort, userToCustShort } from '../../../utils/type-conversions';
import API from '../../../utils/api';
import { connect } from 'react-redux';

interface OwnProps {
  dineIn: boolean;
}

interface StateProps {
  restaurant: Restaurant;
  customer: UserState;
  newReservation: NewReservationState;
  totalPrice: number;
}

interface DispatchProps {
  setPaid: (paid: boolean) => void;
}

type Props = OwnProps & StateProps & DispatchProps;

const Visa = (props: Props) => {
  const navigation = useNavigation();

  const handleMakeReservation = () => {
    props.setPaid(true);
    const reservation: Reservation = {
      restaurant: restToRestShort(props.restaurant),
      customer: userToCustShort(props.customer),
      orders: props.newReservation.orders,
      dineIn: props.newReservation.dineIn,
      duration: props.newReservation.duration,
      totalPrice: props.newReservation.totalPrice,
      finished: false,
      cancelled: false,
      paid: true,
      tableNumbers: [],
      timeBooked: (new Date()).toString(),
    };

    API.post("/reservation", { reservation })
      .then((data) => {
        console.log(data.data.data._id);
        data.data ? navigation.navigate("OrderPlaced", { orderId: data.data.data._id, dineIn: props.dineIn }) : navigation.navigate("ErrorPage");
      })
      .catch(() => navigation.navigate("ErrorPage"));
  };

  return (
    <View style={commonStyles.container} >
      <Form>
        <Item style={styles.input} floatingLabel>
          <Label>Card Number</Label>
          <Input textContentType="creditCardNumber" />
        </Item>
        <Item style={styles.input} floatingLabel last>
          <Label>Expiry Date</Label>
          <Input />
        </Item>
        <Item style={styles.input} floatingLabel last>
          <Label>CVV</Label>
          <Input />
        </Item>
        <Item style={styles.cardHolder} floatingLabel last>
          <Label>Card Holder Name</Label>
          <Input />
        </Item>
      </Form>

      <View style={commonStyles.buttonView}>
        <Button rounded style={commonStyles.button} onPress={() => handleMakeReservation()}>
          <Text style={commonStyles.buttonText}>Submit</Text>
        </Button>
      </View>
    </View>
  );
};

const mapStateToProps = (state: TableMatterState): StateProps => {
  return {
    customer: state.user!,
    restaurant: state.selectedRestaurant || defaultRestaurant,
    newReservation: state.newReservation,
    totalPrice: state.newReservation.totalPrice,
  };
};

const mapDispatchToProps: DispatchProps = {
  setPaid,
};

// IDK why error appears it doesnt affect it from working (Add this on components until it is fixed)
// @ts-ignore 
export default connect<StateProps, DispatchProps, OwnProps>(mapStateToProps, mapDispatchToProps)(Visa);