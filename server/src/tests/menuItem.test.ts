import * as chai from "chai";
const server=require("../index");
import chaiHttp = require('chai-http');
import { isMainThread } from "worker_threads";
chai.use(chaiHttp);
const should = chai.should();


describe("menu Item", function(){
    describe ("get menuItem", function(){
        it("get all menu Items", (done) => {
            chai.request(server)
                .get("/menu-item")
                .send({})
                .end(({},res: any)=>{
                    res && res.should.have.status(200);
                    done();
                });
        });
    });
});