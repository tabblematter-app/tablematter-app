import { TableMatterState, SET_VALUE_ACTION, SET_USER_RESERVATIONS_ACTION, SET_SEARCH_RESULTS_ACTION } from "../../src/redux/types";
import reducer from "../../src/redux/reducers/index";

// TODO: Change to statr
export const defaultState: TableMatterState = {
  counter: {
    value: 0,
  },
  reviews: [],
  selectedReservation: null,
  user: {
    id: undefined,
    profilePictureUrl: undefined,
    firstName: undefined,
    lastName: undefined,
    email: undefined,
    phone: undefined,
  },
  reservations: [],
  searchResults: [],
  selectedRestaurant: null,
  newReservation: {
    restaurant: {
      id: "",
      name: "",
      addressLine1: "",
      addressLine2: "",
      suburb: "",
      state: "",
      phone: "",
    },
    dineIn: true,
    tableNumbers: [],
    orders: [],
    totalPrice: 0,
    party: {
      size: 0,
      elderCount: 0,
      adultCount: 0,
      childCount: 0,
      disabledCount: 0,
    },
    duration: 0,
    paid: false,
  }
};

describe("TableMatter - Reducer", () => {
  it("should return the initial state", () => {
    //eslint-disable
    expect(reducer(undefined, { type: undefined }))
      .toMatchObject(defaultState);

  });

  it("should set counter value to 1", () => {
    expect(reducer(undefined, {
      type: SET_VALUE_ACTION,
      value: 1,
    }))
      .toMatchObject({
        counter: {
          value: 1,
        },
      });
  });

  it("should set the search results to an empty array", () => {
    expect(reducer(undefined, {
      type: SET_SEARCH_RESULTS_ACTION,
      searchResults: [],
    })).toMatchObject({
      searchResults: [],
    });;
  });

  it("should set user reservation to array with object below", () => {
    const reservations = [{
      "_id": {
        "$oid": "5df06124548aa152b5ea6922"
      },
      "customer": {
        "id": "5df06124548aa152b5ea6918",
        "firstName": "John",
        "lastName": "Smith",
        "password": "d160e618a9b616c038a65f925f2498ff609741e00714f775fa01ca2488d901e0e089e11c1163f0912eda2d2cdf1a0a58fd82ba52ee6bd1fc569892545a5def17",
        "email": "example@mail.com.au",
        "phone": "0412 345 678"
      },
      "restaurant": {
        "id": "5df06124548aa152b5ea691e",
        "name": "Gregs Fried Chicken",
        "addressLine1": "1 Bryce avenue Mooroolbark",
        "addressLine2": "",
        "phone": "0412 345 678",
        "suburb": "Mooroolbark",
        "state": "Victoria"
      },
      "dineIn": true,
      "tableNumbers": [1],
      "orders": [],
      "totalPrice": 0,
      "party": {
        "size": 2,
        "childCount": 0,
        "adultCount": 2,
        "elderCount": 0,
        "disabledCount": 0
      },
      "timeBooked": "2014-02-01T09:28:56.321-10:00",
      "duration": 2,
      "finished": true,
      "cancelled": false,
      "paid": false,
    }];
    expect(reducer(undefined, {
      type: SET_USER_RESERVATIONS_ACTION,
      reservations,
    })).toMatchObject({
      reservations: reservations,
    });;
  });

});