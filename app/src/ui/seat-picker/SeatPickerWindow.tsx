import React, { useState } from "react";
import { View, FlatList } from "react-native";
import { styles } from "./seatPickerStyles";
import SeatPickerTable, { Floor, Table, TableType, } from "../seat-picker/SeatPickerTable";
import { Wall } from "../seat-picker/SeatPickerWall";
import PinchZoomView from 'react-native-pinch-zoom-view';
import WallRender from "./WallRender";


interface RenderTableItemObject {
    item: Table;
}

interface RenderWallItemObject {
    item: Wall;
}

interface GridProps {
    selectForMe: boolean;
    floors: Floor[];
    floor: number;
}

const SeatPickerWindow = (props: GridProps) => {

    
    const Props = props;

    if(Props.selectForMe) {
        console.log("selectForMe");
    }

    const [numColumns, setNumColumns] = useState(4);

    let modifyTables = false;

    // Sort the array into ascending x and y values in order to render them correctly.
    Props.floors[Props.floor].tables.sort((a, b) => {
        
        const valid = (a.y - b.y);

        // If the initial columns for the grid are not enough for x values,
        // increase the maximum
        if(a.x > numColumns) {
            setNumColumns(a.x);
        }

        if(b.x > numColumns)
        {
            setNumColumns(b.x);
        }

        if(valid == 0) {
            const xValid = (a.x - b.x);
            
            if(xValid < 0)
            {
                return -1;
            }
        }

        return valid;
    });

    // Include gaps for empty spaces. This generates positioning in the flatview based on x and y values
    do
    {
        Props.floors[Props.floor].tables.forEach((item: Table) => {

            const index = Props.floors[Props.floor].tables.indexOf(item);
            let indexRow = 0;
    
            if(item.y > 0) 
            {
                indexRow = (numColumns * (item.y - 1));
            }

            // if(item.y == 1){
            //     indexRow = numColumns;
            // }
    
            if((index + 1) < (item.x + indexRow)) {

                modifyTables = true;

                for(let i = (index + 1); i < item.x + indexRow; i++){
                    Props.floors[Props.floor].tables.splice(i - 1, 0, {shape: TableType.Blank, x: i - indexRow, y: item.y, width: 1, length: 1, number: 0, capacity: 4, available: true});
                }    
            } 
            else {
                if(Props.floors[Props.floor].tables.indexOf(item) + 1 == Props.floors[Props.floor].tables.length)
                {
                    //tables.push(item);
                    modifyTables = false;
                }
            }
        });
    } while (modifyTables);


    const tableRenderItem = ({ item }: RenderTableItemObject) => {
        return (
            <SeatPickerTable key={Props.floors[Props.floor].tables.indexOf(item)} shape={item.shape} available={item.available} capacity={item.capacity} width={item.width} length={item.length} number={item.number} x={item.x} y={item.y} />
        );
    };

    return (
        <View style={[{ minHeight: '90%', minWidth: '200%' }]}>
            <PinchZoomView minScale={0.5} maxScale={2}>  
                <WallRender floor={Props.floor} floors={Props.floors} numColumns={numColumns} />
                <FlatList 
                    style={styles.flatList}
                    data={Props.floors[Props.floor].tables}
                    renderItem={tableRenderItem}
                    numColumns={numColumns}
                    key={numColumns}
                />
            </PinchZoomView>
        </View>
    );
};

export default(SeatPickerWindow);
