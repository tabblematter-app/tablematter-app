import React from "react";
import Icon from "react-native-vector-icons/AntDesign";
import { useNavigation } from "@react-navigation/native";

export const BackToHomeButton = () => {

    //self-created navigation object. Should work Universally.
    const navigation = useNavigation();

    return (
        <Icon name="arrowleft" size={30} color="white" onPress={() => navigation.navigate("Home")}/>
    );
};
