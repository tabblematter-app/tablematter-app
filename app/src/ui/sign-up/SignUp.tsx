import React, { useState } from 'react';
import { Text, TextInput } from "react-native";
import { styles } from "./signUpStyles";
import { commonStyles } from "../common/commonStyles";
import { Header, Body, Title, Right, Left, View, Button } from "native-base";
import { StackNavigationProp } from '@react-navigation/stack';
import { RootStackParamList } from '../utils/rootStack';
import { ScrollView } from 'react-native-gesture-handler';
import { setUser } from '../../redux';
import { connect } from 'react-redux';
import API from '../utils/api';
import { BackButton } from '../common/BackButton';
import { sha512 } from 'js-sha512';

type ProfileScreenNavigationProp = StackNavigationProp<
  RootStackParamList
>;

interface OwnProps {
  navigation: ProfileScreenNavigationProp; // Alternatively any
}

interface DispatchProps {
  setUser: (id: string, profilePictureUrl: string, firstName: string, lastName: string, email: string, phone: string) => void;
}

type Props = DispatchProps & OwnProps;

const SignUp = (props: Props) => {
  const [firstName, setFirstName] = useState('');
  const [lastName, setLastName] = useState('');
  const [email, setEmail] = useState('');
  const [phone, setPhone] = useState('');
  const [password, setPassword] = useState('');
  const [passwordCheck, setPasswordCheck] = useState('');
  const [failed, setFailed] = useState("");

  const [firstNameError, setFirstNameError] = useState(false);
  const [lastNameError, setLastNameError] = useState(false);
  const [emailError, setEmailError] = useState(false);
  const [phoneError, setPhoneError] = useState(false);
  const [passwordError, setPasswordError] = useState(false);
  const [passwordCheckError, setPasswordCheckError] = useState(false);

  const handleSignUp = () => {
    firstName === "" ? setFirstNameError(true) : setFirstNameError(false);
    lastName === "" ? setLastNameError(true) : setLastNameError(false);
    email === "" ? setEmailError(true) : setEmailError(false);
    phone === "" ? setPhoneError(true) : setPhoneError(false);
    password === "" ? setPasswordError(true) : setPasswordError(false);
    passwordCheck !== password ? setPasswordCheckError(true) : setPasswordCheckError(false);
    console.log("Sign up check");
    if (!firstNameError && !lastNameError && !emailError && !phoneError && !passwordError && !passwordCheckError) {
      console.log('Beginning sign-up...');
      const encodedPassword = sha512(password);
      API.post('/createUserAccount', { firstName, lastName, email, password: encodedPassword, phone })
        .then((result) => {
          if (result.data.status) {
            const user = result.data.data;

            // Put Details into Redux
            props.setUser(user.id, user.profilePictureUrl, user.firstName, user.lastName, user.email, user.phone);
            props.navigation.navigate("SignUpSuccess");
          } else {
            // Give response saying sign-up was unsuccessful
          }
        })
        .catch((error) => {
          // Give response saying sign-up was unsuccessful
          setFailed(error.message);
        });
    }
  };

  return (
    <View style={commonStyles.container}>
      <Header style={commonStyles.header}>
        <Left style={commonStyles.flex1}>
          <BackButton />
        </Left>
        <Body style={commonStyles.body}>
          <Title testID="new-account-title" style={commonStyles.title}>Sign Up</Title>
        </Body>
        <Right style={commonStyles.flex1} />
      </Header>

      {/*  style={{ marginBottom: 50 }} */}
      <ScrollView>
        <View style={commonStyles.inputView}>
          <Text style={commonStyles.label}>First Name</Text>
          <Text style={firstNameError ? styles.error : styles.hidden}>Need to Enter First Name</Text>
          <TextInput
            style={commonStyles.input}
            placeholder="Enter first name"
            value={firstName}
            onChangeText={(text: React.SetStateAction<string>) => setFirstName(text)}
          />

          <Text style={commonStyles.label}>Last Name</Text>
          <Text style={lastNameError ? styles.error : styles.hidden}>Need to Enter Last Name</Text>
          <TextInput
            style={commonStyles.input}
            placeholder="Enter last name"
            value={lastName}
            onChangeText={(text: React.SetStateAction<string>) => setLastName(text)}
          />

          <Text style={commonStyles.label}>Email</Text>
          <Text style={emailError ? styles.error : styles.hidden}>Please enter valid email</Text>
          <TextInput
            style={commonStyles.input}
            placeholder="Enter email address"
            value={email}
            onChangeText={(text: React.SetStateAction<string>) => setEmail(text)}
          />

          <Text style={commonStyles.label}>Phone Number</Text>
          <Text style={phoneError ? styles.error : styles.hidden}>Please enter phone number</Text>
          <TextInput
            style={commonStyles.input}
            placeholder="Enter mobile phone number"
            value={phone}
            onChangeText={(text: React.SetStateAction<string>) => setPhone(text)}
          />

          <Text style={commonStyles.label}>Password</Text>
          <Text style={passwordError ? styles.error : styles.hidden}>Please enter password</Text>
          <TextInput
            style={commonStyles.input}
            secureTextEntry={true}
            placeholder="Enter password"
            value={password}
            onChangeText={(text: React.SetStateAction<string>) => setPassword(text)}
          />

          <Text style={commonStyles.label}>Confirm Password</Text>
          <Text style={passwordCheckError ? styles.error : styles.hidden}>Passwords must match</Text>
          <TextInput
            style={commonStyles.input}
            secureTextEntry={true}
            placeholder="Re-enter password"
            value={passwordCheck}
            onChangeText={(text: React.SetStateAction<string>) => setPasswordCheck(text)}
          />

          <Text style={[commonStyles.label, (!failed && styles.hidden), styles.alert]}>
            {failed}
          </Text>

          <View style={[commonStyles.buttonView, { width: "100%" }]}>
            <Button rounded style={commonStyles.button} onPress={() => handleSignUp()}>
              <Text style={commonStyles.buttonText}>SUBMIT</Text>
            </Button>
          </View>
        </View>
      </ScrollView>
    </View>
  );
};

const mapDispatchToProps: DispatchProps = {
  setUser,
};

// IDK why error appears it doesnt affect it from working (Add this on components until it is fixed)
// eslint-disable-next-line
export default connect<{}, DispatchProps>(undefined, mapDispatchToProps)(SignUp);