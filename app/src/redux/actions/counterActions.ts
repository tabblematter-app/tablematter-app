import { TableMatterActionTypes, SET_VALUE_ACTION } from "../types";


export const setCounter = (value: number): TableMatterActionTypes => {
    return {
        type: SET_VALUE_ACTION,
        value,
    };
};
