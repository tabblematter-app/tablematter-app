import { StyleSheet, Dimensions } from "react-native";
import { p1 } from "../../utils/themes/lightTheme";

const windowWidth = Dimensions.get('window').width;

export const styles = StyleSheet.create({
  searchBar: {
    width: "auto",
    flex: 1,
  },
  searchSettingsButton: {
    color: p1
  },
  searchBarWrapper: {
    flexDirection: "row",
    paddingVertical: 20,
    paddingHorizontal: windowWidth * 0.05,
    borderBottomWidth: 1,
    borderBottomColor: p1,
  },
  orderAgainTitle: {
    color: p1,
    fontWeight: "bold",
    marginTop: 20
  },
  filterIcon: {
    alignItems: "flex-end",
    justifyContent: "center",
    marginLeft: windowWidth * 0.05
  }
});