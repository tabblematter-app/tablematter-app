import React from 'react';
import { Text, View } from "react-native";
import { styles } from "./signUpSuccessStyles";
import { commonStyles } from '../common/commonStyles';
import { Header, Body, Title, Button } from "native-base";
import { StackNavigationProp } from '@react-navigation/stack';
import { RootStackParamList } from '../utils/rootStack';

type ProfileScreenNavigationProp = StackNavigationProp<
  RootStackParamList
>;

interface OwnProps {
  navigation: ProfileScreenNavigationProp; // Alternatively any
}

type Props = OwnProps;

export const SignUpSuccess = (props: Props) => {

  return (
    <View style={commonStyles.container}>
      <Header style={commonStyles.header}>
        <Body style={commonStyles.body}>
          <Title testID="new-account-title" style={commonStyles.title}>TABLE MATTER</Title>
        </Body>
      </Header>
      <View style={[commonStyles.content, styles.messageContainer]}>
        <Text style={styles.thanks}>THANK YOU!</Text>
        <Text>Your account was successfully created</Text>
      </View>
      <View style={commonStyles.buttonView}>
        <Button
          rounded
          onPress={() => props.navigation.navigate("Tutorial")}
          style={commonStyles.button}
        >
          <Text style={commonStyles.buttonText}>Continue</Text>
        </Button>
      </View>
    </View>
  );
};