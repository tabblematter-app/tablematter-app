import { StyleSheet } from "react-native";
import { p1, p2 } from "../utils/themes/lightTheme";

export const styles = StyleSheet.create({
  header: {
    backgroundColor: p1,
    },
    title: {
        color: "white",
        fontSize: 25,
    },
    buttonWrapper: {
      paddingLeft: 10,
      paddingRight: 10,
      flexDirection: 'row',
      flexWrap: 'wrap',
      alignContent: 'center',
      justifyContent: 'center',
      position: 'absolute',
      bottom: 0,
      width: '100%'
    },
    button: {
      borderColor: p1,
      borderWidth: 1, // How thick the border is
      height: 50, // height of text box
      width: '45%', // width of text box
      borderRadius: 30, // how circular the textbox is
      backgroundColor: p1,
      margin: 5,
    },
    buttonText: {
        paddingTop: '8%', // centers the text
        textAlign: "center",
        color: "white",
        fontSize: 14,
        fontWeight: 'bold',
    },
    scrollView: {
      //backgroundColor: 'pink',
      //marginHorizontal: 20,
      flex: 1,
    },
    tableWrapper: {
      padding: 30,
      flexDirection: 'row',
      flexWrap: 'wrap',
      alignItems: 'center',
      justifyContent: 'center',
    },
    tableSelected : {
      backgroundColor: p1,
    },
    tableUnselected: {
      backgroundColor: p2,
    },
    tableUnavailable: {
      backgroundColor: '#b7b7b7',
    },
    squareTable: {
      margin: 10,
      marginTop: 20,
      width: 50,
      height: 50,
      borderRightColor: '#000',
      borderRightWidth: 1,
      borderLeftColor: '#000',
      borderLeftWidth: 1,
      borderTopColor: '#000',
      borderTopWidth: 1,
      borderBottomColor: '#000',
      borderBottomWidth: 1,
    },
    circleTabel: {
      margin: 10,
      marginTop: 20,
      width: 50,
      height: 50,
      borderRightColor: '#000',
      borderRightWidth: 1,
      borderLeftColor: '#000',
      borderLeftWidth: 1,
      borderTopColor: '#000',
      borderTopWidth: 1,
      borderBottomColor: '#000',
      borderBottomWidth: 1,
      borderRadius: 50,
    },
    rectangleWideTabel: {
      margin: 10,
      marginTop: 20,
      width: 120,
      height: 50,
      borderRightColor: '#000',
      borderRightWidth: 1,
      borderLeftColor: '#000',
      borderLeftWidth: 1,
      borderTopColor: '#000',
      borderTopWidth: 1,
      borderBottomColor: '#000',
      borderBottomWidth: 1,
    },
    rectangleLongTabel: {
      margin: 10,
      marginTop: 20,
      width: 50,
      height: 100,
      borderRightColor: '#000',
      borderRightWidth: 1,
      borderLeftColor: '#000',
      borderLeftWidth: 1,
      borderTopColor: '#000',
      borderTopWidth: 1,
      borderBottomColor: '#000',
      borderBottomWidth: 1,
    },
    blankTable: {
      margin: 10,
      marginTop: 20,
      width: 50,
      height: 50,
    },
    flatList: {
      overflow: "visible",
      position: "absolute",
    },
    wallHorizontal: {
      backgroundColor: '#000',
      borderLeftColor: '#000',
      borderTopLeftRadius: 2,
      alignSelf: 'flex-end',
      //height: 2,
      //width: 95,
    },
    wallVertical: {
      backgroundColor: '#000',
      borderTopColor: '#000',
      borderTopEndRadius: 2,
      alignSelf: 'flex-end',
      //height: 95,
      //width: 2,
    },
    blankSpace: {
      // backgroundColor: 'red',
      // borderRightColor: 'blue',
      // borderRightWidth: 2,
      // borderTopColor: 'blue',
      // borderTopWidth: 2,
    },
    floorPickerWrapper: {
      paddingLeft: 10,
      paddingRight: 10,
      flexDirection: 'row',
      flexWrap: 'wrap',
      alignContent: 'center',
      justifyContent: 'center',
      position: 'absolute',
      bottom: 0,
      width: '100%'
      
    },
    floorPickerOption: {
      height: 100,
      width: 100
    }
  });
