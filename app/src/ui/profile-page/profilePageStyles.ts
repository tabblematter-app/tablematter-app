import { StyleSheet } from "react-native";
import { p1, p2 } from "../utils/themes/lightTheme";

export const styles = StyleSheet.create({
  userInfo: {
    flexWrap: "wrap",
    flexDirection: 'row',
    alignItems: "center",
    justifyContent: "space-between",
    paddingHorizontal: 10,
  },
  userInfoSection: {
    flexWrap: "wrap",
    flexDirection: 'row',
    alignItems: "center",
    justifyContent: "center"
  },
  userInfoText: {
    paddingHorizontal: 5,
  },
  userInfoIcon: {
    color: p1,
  },
  profilePic: {
    width: 100,
    height: 100,
    alignSelf: "center",
    justifyContent: "center",
    borderRadius: 100,
    marginVertical: 30,
    backgroundColor: p2
  },
  userNames: {
    fontSize: 20,
    fontWeight: "bold",
    color: p1,
    alignSelf: "center",
    paddingBottom: 10,
  },
  profileIcon: {
    alignSelf: "center",
  },
  hidden: {
    display: "none"
  },
  alert: {
    marginBottom: 15,
  },
  altButton: {
    borderWidth: 1,
    borderColor: p1,
    padding: 5,
    alignSelf: 'center',
    minWidth: 200,
    marginBottom: 20,
  },
  altButtonText: {
    color: p1,
    fontSize: 16,
    alignSelf: 'center'
  }
});
