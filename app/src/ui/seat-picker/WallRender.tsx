import React from "react";
import { View, FlatList } from "react-native";
import { styles } from "./seatPickerStyles";
import { Floor, Table } from "./SeatPickerTable";
import SeatPickerWall, { Wall } from "./SeatPickerWall";


interface RenderTableItemObject {
    item: Table;
}

interface RenderWallItemObject {
    item: Wall;
}

interface GridProps {
    floors: Floor[];
    floor: number;
    numColumns: number;
}

const WallRender = (props: GridProps) => {
    
    const Props = props;
    //const [floor, setFloor] = useState(Props.floor);
    const horizontalWalls: Wall[] = [];
    const verticalWalls: Wall[] = [];

    let modifyWalls = false;

    // Sorts the walls into correct order for rendering
    Props.floors[Props.floor].walls.sort((a, b) => {
        
        const valid = (a.y1 - b.y1);

        if(valid == 0) {
            const xValid = (a.x1 - b.x1);
            
            if(xValid < 0)
            {
                return -1;
            }
        }

        return valid;
    });

    Props.floors[Props.floor].walls.map((item) => {

        if(item.x1 == 0 && item.x2 == 0 && item.y1 == 0 && item.y2 == 0)
        {
            horizontalWalls.push(item);
            verticalWalls.push(item);
        }

        if(item.x1 !== item.x2)
        {
            horizontalWalls.push(item);
        } else {
            verticalWalls.push(item);
        }
    });

    const finalVert: Wall[][] = [];
    const finalHoriz: Wall[][] = [];

    horizontalWalls.forEach((wall) => {
        const result: Wall[] = [];

        for(let i = wall.x1; i < wall.x2; i++)
        {
            result.push({ x1: i - 1, x2: i, y1: wall.y1, y2: wall.y2 });
        }

        finalHoriz.push(result);
    });

    verticalWalls.forEach((wall) => {
        const result: Wall[] = [];

        for(let i = wall.y1; i < wall.y2; i++)
        {
            result.push({ x1: wall.x1, x2: wall.x2, y1: i+1, y2: i+2 });
        }

        finalVert.push(result);
    });

    modifyWalls = true;

    // Include gaps for empty spaces. This generates positioning in the flatview based on x and y values
    finalVert.forEach((array: Wall[]) => {
        do
        {
            array.forEach((item: Wall) => {

                const index = array.indexOf(item);
                let indexRow = 0;
        
                if(index != 0)
                {
                    if(item.y1 > 0) 
                    {
                        indexRow = (props.numColumns * (item.y1 - 1));
                    }

                    for(let i = (index); i < item.x1 + indexRow; i++){
                        array.splice(i, 0, { x1: 0, x2: 0, y1: 0, y2: 0 });
                    }
                }

                if(array.indexOf(item) + 1 == array.length)
                {
                    modifyWalls = false;
                }
            });
        } while (modifyWalls);
    });

    modifyWalls = true;

    // Include gaps for empty spaces. This generates positioning in the flatview based on x and y values
    finalHoriz.forEach((array: Wall[]) => {
        do
        {
            array.forEach((item: Wall) => {

                const index = array.indexOf(item);
                let indexRow = 0;
        
                if(index != 0)
                {
                    if(item.y1 > 0) 
                    {
                        indexRow = (props.numColumns * (item.y1 - 1));
                    }

                    for(let i = (index); i < item.x1 + indexRow; i++){
                        array.splice(i, 0, { x1: 0, x2: 0, y1: 0, y2: 0 });
                    }
                }

                if(array.indexOf(item) + 1 == array.length)
                {
                    modifyWalls = false;
                }
            });
        } while (modifyWalls);
    });

    const horizontalWallRenderItem = ({ item }: RenderWallItemObject) => {
        return (
            <SeatPickerWall walls={horizontalWalls} key={Props.floors[Props.floor].walls.indexOf(item)} dimensions={item} showHorizontal={true} showVertical={false}/>
        );
    };

    const verticalWallRenderItem = ({ item }: RenderWallItemObject) => {
        return (
            <SeatPickerWall walls={verticalWalls} key={Props.floors[Props.floor].walls.indexOf(item)} dimensions={item} showHorizontal={false} showVertical={true}/>
        );
    };

    return (
        <View>
            {/* <FlatList 
                    style={styles.flatList}
                    data={finalHoriz}
                    renderItem={horizontalWallRenderItem}
                    numColumns={props.numColumns}
                    key={(props.numColumns * 2)}
                /> */}
                {finalVert.map((vert) => {
                    return (
                        <FlatList 
                        style={styles.flatList}
                        data={vert}
                        renderItem={verticalWallRenderItem}
                        numColumns={props.numColumns}
                        key={(props.numColumns * 3)}
                        />
                    );
                })}
                {finalHoriz.map((horiz) => {
                    return (
                        <FlatList 
                        style={styles.flatList}
                        data={horiz}
                        renderItem={horizontalWallRenderItem}
                        numColumns={props.numColumns}
                        key={(props.numColumns * 3)}
                        />
                    );
                })}
        </View>
    );
};

export default(WallRender);
