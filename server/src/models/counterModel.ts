import * as mongoose from "mongoose";

interface Counter {
    value: number;
}

export const CounterSchema = new mongoose.Schema({
    value: Number,
});

export default mongoose.model<Counter & mongoose.Document>("counter", CounterSchema, "counter");
