import * as React from 'react';
import { TextInput } from 'react-native';
import { styles } from './loginStyles';
import { commonStyles } from '../common/commonStyles';
import {
  Header,
  Title,
  Body,
  Text,
  View,
  Left,
  Right,
  Button,
} from 'native-base';
import { StackNavigationProp } from '@react-navigation/stack';
import { RootStackParamList } from '../utils/rootStack';
import { ScrollView } from 'react-native-gesture-handler';
import { setUser } from '../../redux';
import { connect } from 'react-redux';
import API from '../utils/api';
import { BackButton } from '../common/BackButton';
import { sha512 } from 'js-sha512';
import { showError } from '../utils/alert';

type ProfileScreenNavigationProp = StackNavigationProp<RootStackParamList>;

interface OwnProps {
  navigation: ProfileScreenNavigationProp; // Alternatively any
}

interface DispatchProps {
  setUser: (
    id: string,
    profilePictureUrl: string,
    firstName: string,
    lastName: string,
    email: string,
    phone: string,
  ) => void;
}

type Props = DispatchProps & OwnProps;

const Login = (props: Props) => {
  const [email, setEmail] = React.useState('');
  const [password, setPassword] = React.useState('');
  //  17/07/2020 - Passwords are now hashed in sha512 before sending to server
  const [failed, setFailed] = React.useState('');

  const validate = () => {
    const pwd = password;
    if (email === '' || pwd === '') {
      showError('Error', 'Email or password cannot be empty');
      return false;
    }
    return true;
  };

  const handleLogin = () => {
    if (!validate()) {
      return;
    }
    const encodedPassword = sha512(password);
    API.get('/login/' + email + '/' + encodedPassword)
      .then((result) => {
        if (result.data.status) {
          const user = result.data.data;
          console.log('User ID after sign in: ' + user.id);
          if (user.firstName) {
            props.setUser(
              user.id,
              user.profilePictureUrl,
              user.firstName,
              user.lastName,
              user.email,
              user.phone,
            );
            props.navigation.navigate('Main');
          } else {
            setFailed(user.message);
          }
        } else {
          setFailed(result.data.message);
        }
      })
      .catch();
  };

  return (
    <View style={commonStyles.container}>
      <Header style={commonStyles.header}>
        <Left style={commonStyles.flex1}>
          <BackButton />
        </Left>
        <Body style={commonStyles.body}>
          <Title style={commonStyles.title}>Log In</Title>
        </Body>
        <Right style={commonStyles.flex1} />
      </Header>
      <ScrollView>
        <View style={commonStyles.inputView}>
          <Text style={commonStyles.label}>Email</Text>
          <TextInput
            testID="email-input"
            style={commonStyles.input}
            placeholder="Enter email address"
            value={email}
            onChangeText={(text: React.SetStateAction<string>) => setEmail(text)}
          />
          <Text style={commonStyles.label}>Password</Text>
          <TextInput
            testID="password-input"
            style={[commonStyles.input, styles.password]}
            secureTextEntry={true}
            placeholder="Enter password"
            value={password}
            onChangeText={(text: React.SetStateAction<string>) => setPassword(text)}
          />
          <Text
            style={[
              commonStyles.label,
              !failed && styles.hidden,
              styles.alert,
            ]}>
            {failed}
          </Text>
        </View>
      </ScrollView>

      <View style={commonStyles.buttonView}>
        <Button
          rounded
          testID="login-button"
          style={commonStyles.button}
          onPress={() => handleLogin()}>
          <Text style={commonStyles.buttonText}>SUBMIT</Text>
        </Button>
      </View>
    </View>
  );
};

const mapDispatchToProps: DispatchProps = {
  setUser,
};

// IDK why error appears it doesnt affect it from working (Add this on components until it is fixed)

export default connect<{}, DispatchProps>(undefined, mapDispatchToProps)(Login);
