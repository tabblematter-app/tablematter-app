# Architecture
This document outlines the system acrtchitecture including different scenarios of how the server, client, database and any other external entities communicate within the context of the TableMatter application.

## Overall 
The overall architecture of this project will be a simple client server based model where the user sends queries to the server in the form of REST API calls and the server then updates and gets information from the database to complete the query.

```plantuml
cloud AppServer
cloud DashboardServer
node Client1
node Client2
node ExternalClient
database Restaurants
Client1 <--> AppServer : WebSocket
Client2 <--> AppServer : WebSocket
ExternalClient <--> DashboardServer : WebRTC
AppServer -> Restaurants
DashboardServer -> Restaurants
```

## Entity-relationship model

Relationships within the Table matter database

```plantuml

hide circle
' avoid problems with angled crows feet
skinparam linetype ortho

entity "User" as user {
  * ID
  * name
  * bookings
  * password
}
entity "Booking" as booking {
  * ID
  * date
  * meals
  * totalCost
}
entity "Restaurant" as restaurant {
  * ID
  * Address
  * name
  * bookings
  * menu
}
entity "Menu Item" as menuItem{
  * ID
  * mealName
  * cost
  * ingredients
  * dietryStatus
}

booking }o--|| user
booking }o--|| restaurant
restaurant ||--|| menuItem
menuItem }o--|| booking
```

## Scenarios

### Booking Seat
When the user enters the select seat/booking page, the client queries the server based off of the selected date and time the available tables which are then displayed to the user to select.

```plantuml
actor User
User-> Client1 : Time and Date of booking
Client1 -> Server : Get available tables
Server -> Client1 : Available tables
User -> Client1 : Table selection
User -> Client1 : Finalise booking
Client1 -> Server: Book Restauraun started
```

### Payment Gate
When the client books a restaurant the server is alerted to reserve the table and time for 10 minutes in order to give the client time to pay. following this the client is asked to input their payment information. Once the payment is accepted the reservation is finalised and added to the database.

```plantuml
actor User
User-> Client1 : Book Resauraunt
Client1 -> Server : Book Restauraunt started
User -> Client1 : Payment Information
Client1 -> PaymentService: Payment information
PaymentService -> Client1: Payment method valid
Client1 -> Server: Booking confirmation
```

### Login
When the client logs into their account they give the server their email and password. From this the server determines whether the user exists and has the correct credentials
```plantuml
actor User
User-> Client1 : Log into account
Client1 -> Server : logIn(email, password)
Server -> Server: email and password check
Server -> Client1: Valid user + information
```

### Create Account
When the client creates a new account, the server first checks whether they have entered all the required information. Following this it checks whether the email is allready in use. Assuming the email is not in use a new account is created and the user is automatically logged in with it.
```plantuml
actor User
User-> Client1 : Create new Account
Client1 -> Server : createAccount(accountInformation)
Server -> Server: isEmailUsed
Server -> Client1: Valid user + new account confirmation
```

### Edit Account
When the client wants to update details on their account they send a message to the server along with their account id (which can only be retrieved from logging on) once the server validates their id the account entry is updated with the new information (be it password, email etc) in the event of email update, uniqueness will be ensured.
NOTE: Before this project can be considered consumer safe, changing the password will require more security.
```plantuml
actor User
User-> Client1 : Update account information
Client1 -> Server : updateAccount(accountInformation)
Server -> Server : doesIdExist()
Server -> Server: isEmailChanged()
Server -> Server : isNewEmailUnique()
Server -> Client1: Valid user + new account confirmation
```


### Search Restaurants
When the client searches for restaurants multiple search terms/types can be used. Depending on the details given the server then either conducts a fuzzy or complex search on the restaurants table, returning the top 20 results.
```plantyuml
actor User
User-> Client1 : Search for restaurant
Client1 -> Server : searchForRestaurants(query)
Server -> Server : fuzzySearch()
Server -> Client1: Top 20 most matching restaurants
```