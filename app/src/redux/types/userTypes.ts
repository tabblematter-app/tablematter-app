
export interface UserState {
    id?: string;
    profilePictureUrl? : string;
    firstName?: string;
    lastName?: string;
    email?: string;
    phone?: string;
}

export const SET_USER_ACTION = "SET_USER_ACTION";
export interface SetUserAction {
    type: typeof SET_USER_ACTION;
    id: string;
    profilePictureUrl: string;
    firstName: string;
    lastName: string;
    email: string;
    phone: string;
}

export const LOG_OUT_ACTION = "LOG_OUT_ACTION";
export interface LogOutAction {
    type: typeof LOG_OUT_ACTION;
}

export type UserActions =
    SetUserAction |
    LogOutAction;