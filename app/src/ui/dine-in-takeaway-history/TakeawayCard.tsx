import React from 'react';
import { ImageBackground } from "react-native";
import { Body, Text, View, Card, CardItem, Button } from "native-base";
import { commonStyles } from "../common/commonStyles";
import { styles } from "./DineInTakeawayStyles";
import { Reservation } from "../../redux/types/serverTypes";
import { connect } from 'react-redux';
import { setSelectedReservation } from "../../redux";
import { useNavigation } from '@react-navigation/native';

interface StateProps {
  reservation: Reservation | null;
}

interface DispatchProps {
  setSelectedReservation: (selectedReservation: Reservation | null) => void;
}

type Props = StateProps & DispatchProps;

const TakeawayCard = (props: Props) => {
  const navigation = useNavigation();
  const viewReservation = () => {
    props.setSelectedReservation(props.reservation);
    navigation.navigate('ReservationSummaryPage');
  };

  if (props.reservation) {
    return (
      <Card style={styles.container}>
        <CardItem cardBody>
          <View style={styles.cardImageContainer}>
            <ImageBackground
              source={require('../../../Assets/PlaceholderGraphic.jpg')}
              style={styles.cardImage}
            >
              <View style={styles.cardImageInner}>
                <Text style={{ color: '#FFF' }}>{props.reservation.restaurant.name}</Text>
                <Text style={{ color: '#FFF' }}>{props.reservation.restaurant.addressLine1}</Text>
              </View>
            </ImageBackground>
          </View>
        </CardItem>
        {/* If linked to a booking */}
        {/* {props.reservation.party &&
          <CardItem style={styles.borderBottom}>
            <Text>Pre-order for Booking (ID here)</Text>
          </CardItem>
        } */}
        <CardItem>
          <Body style={{ width: "100%" }}>
            {[...new Set(props.reservation.orders.map(menuItem => menuItem.name))].map((menuItemName, key) =>
              <Text key={key}>
                {props.reservation?.orders.filter(menuItem => menuItem.name).length + " x " + menuItemName}
              </Text>
            )}
          </Body>
        </CardItem>
        <CardItem style={[styles.orderTotal, styles.borderTop]}>
          <Text style={styles.orderTotalText}>Total: ${props.reservation.totalPrice}</Text>
        </CardItem>
        <CardItem cardBody style={[commonStyles.flex1, styles.borderTop]}>
          {/* Receipt if paid for */}
          {props.reservation.paid &&
            <Button transparent style={commonStyles.cardButton}>
              <Text style={commonStyles.cardButtonText}>RECEIPT</Text>
            </Button>
          }
          {/* Enable editing if linked to upcoming reservation & not paid for */}
          {props.reservation.party && !props.reservation.finished && !props.reservation.paid &&
            <Button transparent style={commonStyles.cardButton} onPress={viewReservation}>
              <Text style={commonStyles.cardButtonText}>VIEW</Text>
            </Button>
          }
        </CardItem>
      </Card>
    );
  } else {
    return null;
  }
};

const mapDispatchToProps: DispatchProps = {
  setSelectedReservation,
  // setSelectedRestaurant
};

// IDK why error appears it doesnt affect it from working (Add this on components until it is fixed)
// @ts-ignore 
export default connect<{}, DispatchProps>(undefined, mapDispatchToProps)(TakeawayCard);