import { TableMatterActionTypes, SET_USER_ACTION, LOG_OUT_ACTION } from "../types";


export const setUser = (id: string, profilePictureUrl: string, firstName: string, lastName: string, email: string, phone: string): TableMatterActionTypes => {
    return {
        type: SET_USER_ACTION,
        profilePictureUrl,
        id,
        firstName,
        lastName,
        email,
        phone,
    };
};

export const logOut = (): TableMatterActionTypes => {
    return {
        type: LOG_OUT_ACTION,
    };
};
