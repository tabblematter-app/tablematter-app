import { StyleSheet } from "react-native";
import { p1 } from "../utils/themes/lightTheme";

export const styles = StyleSheet.create({
  hidden: {
    display: "none",
  },
  error: {
    color: p1,
    paddingLeft: 30,
  },
  alert: {
    marginBottom: 15,
  },
});