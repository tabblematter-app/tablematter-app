import * as chai from "chai";
const server=require("../index");
import chaiHttp = require('chai-http');
import { isMainThread } from "worker_threads";
chai.use(chaiHttp);
const should = chai.should();


describe("customer", function(){
    describe ("get user", function(){
        it("get specific user", (done) => {
            chai.request(server)
                .get("/login/101458985@student.swin.edu.au/7fcf4ba391c48784edde599889d6e3f1e47a27db36ecc050cc92f259bfac38afad2c68a1ae804d77075e8fb722503f3eca2b2c1006ee6f6c7b7628cb45fffd1d")
                .send({})
                .end(({},res: any)=>{
                    res && res.should.have.status(200);
                    done();
                });
        }).timeout(5000);

        it("get all users", (done) => {
            chai.request(server)
                .get("/user")
                .send({})
                .end(({},res: any)=>{
                    res && res.should.have.status(200);
                    done();
                });
        });

    });

    describe ("create account", () => {
        it("should create account", (done) => {
            chai.request(server)
                .post("/createUserAccount", )
                .send({
                    firstName: "John",
                    lastName: "Smith",
                    password: "d160e618a9b616c038a65f925f2498ff609741e00714f775fa01ca2488d901e0e089e11c1163f0912eda2d2cdf1a0a58fd82ba52ee6bd1fc569892545a5def17",
                    email: "example@mail.com.au",
                    phone: "0412 345 678"
                })
                .end(({},res: any)=>{
                    res && res.should.have.status(200);
                    done();
                });
        });
    });

    describe ("edit account", () => {
        it("should edit account information", (done) => {
            chai.request(server)
                .post("/editUserAccount", )
                .send({
                    id: "5df06124548aa152b5ea6918",
                    firstName: "John",
                    lastName: "Smith",
                    //password: "d160e618a9b616c038a65f925f2498ff609741e00714f775fa01ca2488d901e0e089e11c1163f0912eda2d2cdf1a0a58fd82ba52ee6bd1fc569892545a5def17",
                    email: "example@mail.com.au",
                    phone: "0412 345 678"
                })
                .end(({},res: any)=>{
                    res && res.should.have.status(200);
                    done();
                });
        });
    });
});