import { StyleSheet } from "react-native";
import { p1, p2, p3 } from "../utils/themes/lightTheme";

export const styles = StyleSheet.create({
  userInfoIcon: {
    color: p1,
  },
  reviewFeedbackBox: {
    borderRadius: 20,
    borderColor: p1,
    marginVertical: 10,
    padding: 5,
  },
  reviewImageContainer: {
    height: 170,
    marginBottom: 20
  },
  image: {
    width: "100%",
    height: undefined,
    flex: 1,
  },
  ratingWrapper: {
    alignSelf: "center",
    flexWrap: "wrap",
    alignItems: "flex-start",
    flexDirection: "row",
  },
  ratingStar: {
    margin: 10,
    fontSize: 55,
  },
  ratingStarNotSelected: {
    color: p3,
  },
  reviewerNameCheckboxText: {
    marginLeft: 10,
  },
  placeholder: {
    color: p1,
    marginTop: 3,
    fontSize: 16,
    paddingLeft: 20,
  },
  hidden: {
    display: "none",
  },
  alert: {
    marginBottom: 15,
  },
  editProfilePic: {
    width: 100,
    height: 100,
    alignSelf: "center",
    justifyContent: "center",
    borderRadius: 100,
    marginTop: 10,
    marginBottom: 10,
    backgroundColor: p2
  },
});