import React from 'react'; //Removed useState for now until process hookup
import {
  Text,
  View,
  ScrollView,
  //  Share,
  //  Platform,
  //  Image,
} from 'react-native';
//Removed DatePickerIOS and Modal from react-native to resolve lint errors
import { BackButton } from '../../common/BackButton';
import { connect } from 'react-redux';
import { useNavigation, RouteProp, useRoute } from '@react-navigation/native';
import MapView, { Marker, Callout } from 'react-native-maps';
import { Header, Body, Left, Right, List, ListItem, Button, Icon } from 'native-base';
import { Restaurant, Party, Reservation } from '../../../redux/types/serverTypes';
import {
  TableMatterState,
  setParty,
  setSelectedRestaurant,
  setTableNumbers,
  setDate,
  setDuration,
  NewReservationState,
  UserState,
  setPaid,
} from '../../../redux';
import API from '../../utils/api';
import { defaultRestaurant } from './RestaurantPage';
import { p1 } from '../../utils/themes/lightTheme';
import MenuItemDisplay from '../../main-app/restaurants/menu/MenuItem';
import { styles } from './SummaryStyles';
import { commonStyles } from '../../common/commonStyles';
import moment from 'moment';
import { restToRestShort, userToCustShort } from '../../utils/type-conversions';
import { Grid, Row, Col } from 'react-native-easy-grid';
import { displayParty } from '../../utils/helpers';
import { RootStackParamList } from '../../utils/rootStack';

type menuScreenRoute = RouteProp<RootStackParamList, 'BookingDetailsPage'>;

interface StateProps {
  restaurant: Restaurant;
  party: Party;
  date: string;
  name: string;
  tableNumbers: number[];
  duration: number;
  newReservation: NewReservationState;
  totalPrice: number;
  customer: UserState;
}
interface DispatchProps {
  setSelectedRestaurant: (selectedRestaurant: Restaurant | null) => void;
  setParty: (party: Party) => void;
  setTableNumbers: (tableNumbers: number[]) => void;
  setDate: (date: string) => void;
  setDuration: (duration: number) => void;
  setPaid: (paid: boolean) => void;
}

type Props = DispatchProps & StateProps;

const BookingDetailsPage = (props: Props) => {
  const navigation = useNavigation();

  const route = useRoute() as menuScreenRoute;
  const hasMenu = route.params?.hasMenu || false;

  const handleMakeReservation = () => {
    props.setPaid(false);
    const reservation: Reservation = {
      restaurant: restToRestShort(props.restaurant),
      customer: userToCustShort(props.customer),
      orders: props.newReservation.orders,
      dineIn: props.newReservation.dineIn,
      duration: props.duration,
      totalPrice: props.newReservation.totalPrice,
      finished: false,
      cancelled: false,
      paid: false,
      party: props.party,
      tableNumbers: props.tableNumbers,
      timeBooked: props.date,
    };

    API.post('/reservation', { reservation })
      .then((data) => {
        console.log(data.data.data._id);
        data.data
          ? navigation.navigate('OrderPlaced', {
            orderId: data.data.data._id,
            dineIn: true,
          })
          : navigation.navigate('ErrorPage');
      })
      .catch(() => navigation.navigate('ErrorPage'));
  };

  const displayEndTime = () => {
    return moment(props.date).add(props.duration, 'minutes').format('h:mm a');
  };

  return (
    <View style={commonStyles.container}>
      <Header style={commonStyles.header}>
        <Left style={commonStyles.flex1}>
          <BackButton
            onPress={() => {
              setParty({
                size: 1,
                childCount: 0,
                adultCount: 1,
                elderCount: 0,
                disabledCount: 0,
              });
              setTableNumbers([]);
              setDate('');
              setDuration(30);
            }}
          />
        </Left>
        <Body style={commonStyles.body}>
          <Text testID="welcome-title" style={commonStyles.title}>
            Reservation Summary
				</Text>
        </Body>
        <Right style={commonStyles.flex1}>
        </Right>
      </Header >
      <ScrollView style={commonStyles.flex1}>
        <View style={styles.body}>
          <View style={{ alignItems: 'center', marginVertical: 15 }}>
            <Text style={styles.subtitle}>Your reservation for</Text>
            <Text style={styles.title}>{props.restaurant.name}</Text>
          </View>

          <Grid style={{ marginBottom: 20 }}>
            <Row style={styles.row}>
              <Col style={styles.leftCol}>
                <Row style={{ alignItems: 'center', justifyContent: 'center' }}>
                  <Icon name="calendar" style={styles.icon}></Icon>
                  <Text style={styles.detailTitle}>Date</Text>
                </Row>
                <Text style={styles.detailText}>
                  {moment(props.date).format('MMMM Do YYYY')}
                </Text>
              </Col>
              <Col style={styles.rightCol}>
                <Row style={{ alignItems: 'center', justifyContent: 'center' }}>
                  <Icon name="time" style={styles.icon}></Icon>
                  <Text style={styles.detailTitle}>Time</Text>
                </Row>
                <Text>
                  {moment(props.date).format('h:mm a') + ' - ' + displayEndTime()}
                </Text>
              </Col>
            </Row>

            <Row style={styles.row}>
              <Col>
                <Row style={{ alignItems: 'center', justifyContent: 'center' }}>
                  <Icon name="people" style={styles.icon}></Icon>
                  <Text style={styles.detailTitle}>Party</Text>
                </Row>
                <Text style={styles.detailText}>
                  {props.party &&
                    <Text>
                      {displayParty(props.party) + ' at Table(s) ' + props.tableNumbers.join(", ")}
                    </Text>
                  }
                </Text>
              </Col>
            </Row>
            <Row style={styles.row}>
              <Col>
                <Row style={{ alignItems: 'center', justifyContent: 'center' }}>
                  <Icon name="location" type="Entypo" style={styles.icon}></Icon>
                  <Text style={styles.detailTitle}>Address</Text>
                </Row>
                <Text style={styles.detailText}>
                  {props.restaurant.addressLine1} {/* Restaurant's address */}
                </Text>
              </Col>
            </Row>
          </Grid>

          {hasMenu &&
            <List style={{ marginBottom: 20 }}>
              <Text style={[styles.title, { marginBottom: 10 }]}>Your Order</Text>
              {props.newReservation.orders.map((item, key) => <MenuItemDisplay key={key} menuItem={item} orderSummary />)}
              <View style={{ borderBottomColor: p1, borderBottomWidth: 1, }} />
              <ListItem style={{ marginHorizontal: 0 }}>
                <View style={styles.totalRow}>
                  <Text style={styles.total}>Total Cost:</Text>
                  <Text style={styles.total}>${props.totalPrice.toFixed(2)}</Text>
                </View>
                {/* <Body>
                    <Text style={styles.total}>Total Cost:</Text>
                  </Body>
                  <Right style={{ alignContent: "center" }}>
                    <Text style={[styles.price, styles.total]}>${props.reservation.totalPrice.toFixed(2)}</Text>
                  </Right> */}
              </ListItem>
            </List>
          }

          <Text style={styles.title}>Restaurant Location</Text>
          <View style={styles.mapContainer}>
            <MapView
              style={styles.map}
              region={{
                latitude: 37.8025268,
                longitude: -122.4351449,
                latitudeDelta: 0.0011111,
                longitudeDelta: 0.011111,
              }}>
              <Marker
                coordinate={{ latitude: 37.8025259, longitude: -122.4351431 }}>
                <Callout>
                  <Text style={{ color: 'red', fontWeight: 'bold' }}>
                    Chicken Frenzy
                				</Text>
                </Callout>
              </Marker>
            </MapView>
          </View>

          {hasMenu ?
            <View style={{ marginTop: 30 }}>
              <Text style={styles.title}>Place Reservation</Text>
              <View style={[commonStyles.buttonRow, { width: "100%" }]}>
                <Col style={commonStyles.leftButton}>
                  <Button
                    rounded
                    style={commonStyles.button}
                    onPress={() => handleMakeReservation()}>
                    <Text style={commonStyles.buttonText}>PAY IN STORE</Text>
                  </Button>
                </Col>
                <Col style={commonStyles.rightButton}>
                  <Button
                    rounded
                    style={commonStyles.button}
                    onPress={() => navigation.navigate('PayNowPage', { dineIn: true })}>
                    <Text style={commonStyles.buttonText}>PAY NOW</Text>
                  </Button>
                </Col>
              </View>
            </View> :
            <View style={{ marginTop: 30 }}>
              <Text style={styles.title}>Place Reservation</Text>
              <View style={[commonStyles.buttonView, { width: "100%" }]}>
                <Button
                  rounded
                  style={commonStyles.button}
                  onPress={() =>
                    navigation.navigate('BookingConfirmationPage', {
                      reservation: true,
                      dineIn: true,
                      navigateToConfirmation: true,
                    })
                  }>
                  <Text style={commonStyles.buttonText}>Confirm Booking</Text>
                </Button>
              </View>
            </View>
          }
        </View>
      </ScrollView>
    </View >
  );
};

const mapStateToProps = (state: TableMatterState): StateProps => {
  return {
    restaurant: state.selectedRestaurant || defaultRestaurant,
    party: state.newReservation.party,
    name: state.user.firstName! || 'unknown',
    tableNumbers: state.newReservation.tableNumbers || [],
    date: state.newReservation.date,
    newReservation: state.newReservation,
    totalPrice: state.newReservation.totalPrice,
    duration: state.newReservation.duration,
    customer: state.user!,
  };
};

interface DispatchProps {
  setSelectedRestaurant: (selectedRestaurant: Restaurant | null) => void;
};

const mapDispatchToProps: DispatchProps = {
  setSelectedRestaurant,
  setParty,
  setTableNumbers,
  setDate,
  setDuration,
  setPaid,
};

// IDK why error appears it doesnt affect it from working (Add this on components until it is fixed)
// @ts-ignore
export default connect<StateProps, DispatchProps>(mapStateToProps, mapDispatchToProps)(BookingDetailsPage);
