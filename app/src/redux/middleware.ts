import { Dispatch } from "redux";
import { TableMatterActionTypes } from "./types";

// This file adds redux listeners (will help alot with debugging)


// logs to console (to help debug)
export const tableMatterMiddleware = () => {
    return (next: Dispatch) => (action: TableMatterActionTypes) => {
        console.info("Dispatching", action);

        const result = next(action);
        return result;
    };
};
